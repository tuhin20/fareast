

(function( $ ) {

	'use strict';

	var datatableInit = function() {

		$('#datatable-default').dataTable(
			{
				destroy: true, //use this to reinitiate the table, other wise problem will occur
				processing: true,
				serverSide: true,
				ajax: {
					url: "<?php echo site_url('click2call/process_paging');?>"
					,type: 'POST'
					,data:{query_id: '0'}
				}
			}
		);

	};

	$(function() {
		datatableInit();
	});

}).apply( this, [ jQuery ]);