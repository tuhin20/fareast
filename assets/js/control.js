$(document).on('change','#selectCategory',changeSubSelect);
$(document).on('change','#selectItem',changeSubSelect);
$(document).on('change','#selectCompany',changeSubSelect);
$(document).on('change','#selectDistrict',changeSubSelect);

function changeSubSelect(e){
    //alert('control changed');
    var type=e.target.getAttribute("type");
    var selectedValue=$(e.target).val();//this.value//$(e.target.getAttribute("id")).val();
    //alert(type+'  '+selectedValue);
    var formID=$(e.target).closest("form").attr('id');
    //alert(formID);
    postToController(type,selectedValue,formID);

}

function postToController(type,selectedValue,formID){
    var formID=formID;
    var formIDForJquery='#'+formID;
    var controllerUrl=$('#siteUrl').val()+'/'+'question/getCtrlSelect';
    //alert('type'+type);
    var item_id=$( "select#selectItem" ).val(); //getting value of <select control> which id is selectItem for company change
    //alert('item id'+item_id);
    $.post(
        controllerUrl,
        { type: type, selectedValue:selectedValue,item_id:item_id}, //queryWithoutLimit
        function(data) {
            //alert(data);
            var obj = $.parseJSON(data);
            console.log(obj);
            var divID='';
            if(type=='category'){
                // ex.divID= div id of the control to get changed;
                divID = $(formIDForJquery+ " #selectItem").closest("div").attr("id"); //selecting input controller inside of specific form
                //postToController('item',-1,formID);
            }
            else if(type=='item'){
                $('select#selectCompany').val('-1');
                $('select#selectProduct').val('-1');
                // ex.divID= div id of the control to get changed;
                divID = $(formIDForJquery+ " #selectQueryType").closest("div").attr("id"); //selecting input controller inside of specific form
                //postToController('item',-1,formID);
            }
            else if((type=='company') ){
                divID = $(formIDForJquery+" #selectProduct").closest("div").attr("id");
            }
            else if((type=='district') ){
                divID = $(formIDForJquery+" #selectThana").closest("div").attr("id");
            }
            else divID='';
            //alert('div id '+divID);
            if(divID!='')
                $('#'+divID).html(obj.content);
        }
    );
}