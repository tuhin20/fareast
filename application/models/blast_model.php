<?php
class blast_model extends CI_Model
{
    var $table = 'blast';
	public $queryblast="select * from naafco_predictive ";
    

	
	//public $orderPart=" order by client.name,client.id";
    public $orderPart=" order by product.id desc";


    public function __construct()
    {
        parent::__construct();
        //$this->load->model('utility_model');
        $this->load->helper('date');
        $this->load->database();
		$this->load->library('excel');

    }
	public function do_campaign_already_exist($id,$campaign){        
		$sql="select * from voice_blast where campaign='$campaign' and id<>$id ";
		//echo $sql;
		$results=  $this->prime_model->getByQuery($sql);
        if(sizeof($results)>0)
            return true;
        else return false;
    }
	
	public function upload_file($input_name,$target_folder,$allowed_types='*'){
		//picture upload
		$config['upload_path'] = "./$target_folder"; //make sure the folder exist
		$config['allowed_types'] = $allowed_types;
		//$new_name = $_FILES["voice_file"]['name']) ; //microtime(true);
		$config['file_name'] = $new_name;
		$config['max_size']	= '30000';
		$config['max_width']  = '50000';
		$config['max_height']  = '50000';
		$config['max_filename'] = '200';
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		if($this->upload->do_upload($input_name)){
			//$path = $config['upload_path'].'/'.$new_name;
			print_r($this->upload->data());    //'file_name'
			//$file=$this->upload->data('file_path')."/".$this->upload->data('raw_name');
			return $this->upload->data();
		}
		
		else{
			return array();
			//$data['return_value'] = array('success'=>false,'msg'=>$this->upload->display_errors());
		}
		//end of picture upload
	}
	public function save_dst($blast_id,$file_path){
		$object = PHPExcel_IOFactory::load($file_path);
		foreach($object->getWorksheetIterator() as $worksheet)
		{
			
			$highestRow = $worksheet->getHighestRow();
			//echo $highestRow;
			$highestColumn = $worksheet->getHighestColumn();
			
			for($row=2; $row<=$highestRow; $row++){
				
				$i=0;
				$dst = $worksheet->getCellByColumnAndRow($i, $row)->getCalculatedValue();//getValue();
				if($dst=='')
					continue;
				//$counselor = $worksheet->getCellByColumnAndRow(++$i, $row)->getValue();
				
				
			
				$result[] = array(
						'blast_id' =>$blast_id
						,'dst'=>$dst

				);
				
			}
		}
		//print_r($result);		
		$this->prime_model->bulkInsert('blast_dst',$result);
	}
	
	public function prepare_single_row($blast,$dst){
		return array('blast_id'=>$blast['id']
							,'client_id'=>$blast['created_by']
							,'callerid'=>$blast['callerid']
							,'dst'=>$dst
							,'voice_file'=>$blast['voice_file']
							,'retry'=>$blast['retry']
							,'created_date'=>mdate("%Y-%m-%d  %H:%i:%s", time())
							,'call_status'=>'Queue'
						);
	}
	
	public function push_to_blast_log($id){
		
        $blast=$this->prime_model->getByID('voice_blast','id',$id);
		
		$blast_dst=$this->prime_model->getByQuery("select * from blast_dst where blast_id=$id");
		
		$this->load->helper('date');  
		$data=array();
		foreach($blast_dst as $item){
			$data[]=$this->prepare_single_row($blast,$item['dst']);
		}
		
		if(sizeof($data)>0){
			$this->db->insert_batch('blast_log', $data);			
		}
	}
	public function get_clients(){
		$condition="";
		/*if(!$this->user_model->is_admin()){
			$current_user=$this->user_model->get_current_user();
			$condition=" and client_id=$current_user[id] ";
			
		}*/
		$callerids=$this->prime_model->getByQuery("select id,name from client where is_active=1 $condition");
		return $callerids;
       
	}
	public function get_callerids(){
		$condition="";
		if(!$this->user_model->is_admin()){
			$current_user=$this->user_model->get_current_user();
			$condition=" and client_id=$current_user[id] ";
			
		}
		$callerids=$this->prime_model->getByQuery("select id from number where 1 $condition order by id");
		return $callerids;
       
	}
	public function get_channel(){
		$current_user=$this->user_model->get_current_user();
		
		$result=$this->prime_model->getByQuery("select sum(channel) as total from channel_info where client_id=$current_user[id]");
		return $result[0]['total'];
       
	}
	public function get_codes(){
		$condition="";
		if(!$this->user_model->is_admin()){
			$current_user=$this->user_model->get_current_user();
			$condition=" and created_by=$current_user[id] ";
			
		}
		$codes=$this->prime_model->getByQuery("select code from voice_blast where 1 $condition");
		return $codes;
       
	}
	public function get_campaigns(){
		$condition="";
		if(!$this->user_model->is_admin()){
			$current_user=$this->user_model->get_current_user();
			$condition=" and created_by=$current_user[id] ";
			
		}
		$result=$this->prime_model->getByQuery("select campaign from voice_blast where 1 $condition");
		return $result;
       
	}
	public function approve($id){

        $this->prime_model->executeQuery("LOCK TABLE voice_blast WRITE");
		
		//sleep(10);
		
		$blast=$this->prime_model->getByID('voice_blast','id',$id);
		if($blast['status']=='Approved'){
			return array('success'=>false,'msg'=>'Already approved');
		}
		
		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

		
		if($blast['is_scheduled']=='N'){
			$this->push_to_blast_log($id);
		}
		
		$current_user=$this->user_model->get_current_user();		
		$this->prime_model->executeQuery("update voice_blast set status='Approved',approved_by=$current_user[id],approved_date=now() where id=$blast[id]");
        
		
		$this->db->trans_complete(); # Completing transaction

		/*Optional*/

		if ($this->db->trans_status() === FALSE) {
			# Something went wrong.
			$this->db->trans_rollback();
			$success=false;
		} 
		else {
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
			$success=true;
		}
		
		if($success){
            return array('success'=>true,'msg'=>'Approved successfully');
        }
        else{
            return array('success'=>false,'msg'=>'Unable to approve');
        }
	}
	public function demo($id,$dst){

    	
		$blast=$this->prime_model->getByID('voice_blast','id',$id);
		/*if($blast['status']=='Approved'){
			return array('success'=>false,'msg'=>'Already approved');
		}*/
		
		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

		$single_row=$this->prepare_single_row($blast,$dst);
		$this->prime_model->insert("blast_log",$single_row);
		
		
		$this->db->trans_complete(); # Completing transaction

		/*Optional*/

		if ($this->db->trans_status() === FALSE) {
			# Something went wrong.
			$this->db->trans_rollback();
			$success=false;
		} 
		else {
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
			$success=true;
		}
		
		if($success){
            return array('success'=>true,'msg'=>'Demo prepared successfully');
        }
        else{
            return array('success'=>false,'msg'=>'Unable to prepare demo');
        }
	}
	
	public function get_blast($id){
		$blast=$this->prime_model->getByID('voice_blast','id',$id);
		if($blast['voice_debug']!=''){
			$arr=explode('/',$blast['voice_debug']);
			//echo end(explode('/',$blast['voice_debug']));
			$blast['voice_file_name']= end($arr);
		}
		if($blast['dst_file']!=''){
			$arr=explode('/',$blast['dst_file']);
			$blast['dst_file_name']= end($arr);
		}
		$blast['dsts']=$this->prime_model->getByQuery("select * from blast_dst where blast_id=$id");
		return $blast;
	}
	
	public function save($params){
		if($params['id']!=-1){ //if edit
            $blast=$this->prime_model->getByID('voice_blast','id',$params['id']);
			if($blast['status']=='Approved'){
				return array('success'=>false,'msg'=>'You can\'t edit data after approval.');
			}			
		}
		
		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

		
		
		//print_r($params);
        
		$blast_data=array('id'=>$params['id']
							,'campaign' => $params['campaign']
							,'callerid' => $params['callerid']
							,'max_channel' => $params['max_channel']
							,'retry' => $params['retry']
							,'is_scheduled'=>'N' 
							);
		if(isset($params['is_scheduled'])){
			$blast_data['is_scheduled']='Y';
			$blast_data['schedule_date']=$params['schedule_date'];
		}
		
		$upload_data=$this->upload_file('voice_file','voice_files','*');
		if(sizeof($upload_data)>0){
			$voice_file=$upload_data['file_path'].$upload_data['raw_name'];
			$blast_data['voice_file']=$voice_file;
			if(isset($params['convert'])){
				$output = shell_exec("sudo /etc/asterisk/voice_convert.sh $upload_data[full_path]  $voice_file.sln"); //file conversion
			}
			$blast_data['voice_debug']=$upload_data['full_path'];
		}
		$upload_data=$this->upload_file('number_file','csv_files','xlsx|xls');
		
		$save_dst=false;
		if(sizeof($upload_data)>0){
			
			$blast_data['dst_file']=$upload_data['full_path'];
			$save_dst=true;
			
		}
		
		if($params['id']==-1){
            unset($blast_data['id']);
			$blast_id=$this->prime_model->insert_details('voice_blast', $blast_data);
			
			$code="b".str_pad("$blast_id",3,"0",STR_PAD_LEFT);
			$this->prime_model->update('voice_blast',array('id'=>$blast_id,'code'=>$code));
			
			
		}
		else{
			$blast_id=$params['id'];
			print_r($blast_data);
			$this->prime_model->update_details('voice_blast', $blast_data);
		}
		//save dst from excel
		if($save_dst){
			//delete old dst regarding this blast, this is done for edit
			$this->delete_dst($blast_id);
			$this->save_dst($blast_id,$blast_data['dst_file']);
		}
		
		//save price
		
        
        $this->db->trans_complete(); # Completing transaction

		/*Optional*/

		if ($this->db->trans_status() === FALSE) {
			# Something went wrong.
			$this->db->trans_rollback();
			$success=false;
		} 
		else {
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
			$success=true;
		}
		
		if($success){
            return array('success'=>true,'msg'=>'blast Data saved successfully','test'=>$data);
        }
        else{
            return array('success'=>false,'msg'=>'Unable to save blast Data. Please try after sometime','test'=>$data);
        }
    }
	
	
    public function delete_dst($blast_id){
		$this->prime_model->executeQuery("delete from blast_dst where blast_id=$blast_id");
		
	}
	public function delete_by_id($id){
		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

		$blast=$this->prime_model->getByID('voice_blast','id',$id);
		if($blast['status']=='Approved'){
			return false;
		}
		
		if(!$this->user_model->is_admin()){
			$current_user=$this->user_model->get_current_user();
			if($blast['created_by']!=$current_user['id']){
				return false;
			}
		}
		
		$current_user=$this->user_model->get_current_user();
		$blast['deleted_by']=$current_user['id'];
        $blast['deleted_at']=mdate("%Y-%m-%d  %H:%i:%s", time());
		$this->prime_model->insert("deleted_voice_blast",$blast);
		$this->prime_model->executeQuery("delete from voice_blast where id=$id");
		
		//$this->prime_model->executeQuery("delete from distribution where id=$distribution_id");
		
		
		
        
        
        $this->db->trans_complete(); # Completing transaction

		/*Optional*/

		if ($this->db->trans_status() === FALSE) {
			# Something went wrong.
			$this->db->trans_rollback();
			$success=false;
		} 
		else {
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
			$success=true;
		}
		
		return $success;
    }
    
	
	/*public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}*/
	
	
	public function download_report(){
		ini_set('memory_limit', '-1');
		
		$sql="select v.campaign,b.tried_at,b.callerid,b.dst,b.call_status,c.duration,c.billsec , 
			CASE WHEN b.call_status ='Call_ended' THEN 'Success' ELSE b.call_status END as report_status
			from blast_log b left join cdr c on b.uniqueid=c.uniqueid inner join voice_blast v on b.blast_id=v.id   where 1  "; //where v.code <>""
		
		if(!$this->user_model->is_admin()){
			$current_user=$this->user_model->get_current_user();
			$sql .=" and v.created_by=$current_user[id] ";
		}
	
		$query_id =$this->session->userdata('report_query_id');
		$temp=$this->prime_model->getByID('query','id',$query_id);
		$sql .=$temp['value'];
		
		
		$cdr_data=  $this->prime_model->getByQuery($sql);

		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=report.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		fprintf($output, chr(0xEF).chr(0xBB).chr(0xBF));
		fputcsv($output, array('Campaign','Calldate','Source','Destination','Status','Duration','Billsec'));

		foreach($cdr_data as $item)
		{
		
			fputcsv($output,array($item['campaign'],$item['tried_at'],$item['callerid'],$item['dst'],$item['report_status']
							,$item['duration'],$item['billsec']
		
			));
		}

		fclose($output);                 

	}
	public function get_summary($params){
		$sql="select v.campaign,b.tried_at,b.callerid,b.dst,b.call_status,c.duration,ifnull(c.billsec,0)as billsec,ifnull(bl.deduction,0)as deduction 
		from blast_log b left join cdr c on b.uniqueid=c.uniqueid inner join voice_blast v on b.blast_id=v.id left join balance_log bl on b.uniqueid=bl.uniqueid  where 1  "; //where v.code <>""
	
		if(!$this->user_model->is_admin()){
			$current_user=$this->user_model->get_current_user();
			$sql .=" and v.created_by=$current_user[id] ";
		}
		
		
		$from_date=addslashes(trim($params['from_date']));
		$to_date=addslashes(trim($params['to_date']));
	

		if($from_date!=''){
			//$sql .=" and (vicidial_log.call_date >='$from_date:00') ";
			$sql .=" and (b.created_date >='$from_date:00') ";
		}

		if($to_date!=''){
			//$sql .=" and (vicidial_log.call_date <='$to_date:59') ";
			$sql .=" and (b.created_date <='$to_date:59') ";
		}
		
		if($params['campaign']!=''){
			//$sql .=" and (vicidial_log.call_date <='$to_date:59') ";
			$sql .=" and v.campaign ='$params[campaign]' ";
		}
		
		$result=$this->prime_model->getByQuery($sql);
		
		$total=0;
		$total_connected=0;
		$total_not_connected=0;
		$total_billsec=0;
		$total_expenses=0;
		
		
		foreach($result as $item){
			$total++;
			$total_billsec +=$item['billsec'];
			$total_expenses +=$item['deduction'];
			if($item['call_status']=='Call_ended'){
				$total_connected++;
			}
			else if($item['call_status']=='Not_connected'){
				$total_not_connected++;
			}
		}
		$output=array('from_date'=>$params['from_date']
							,'to_date'=>$params['to_date']
							,'campaign'=>$params['campaign']
							,'total_calls'=>$total
							,'total_connected_calls'=>$total_connected
							,'total_not_connected_calls'=>$total_not_connected
							,'total_billsec'=>floor($total_billsec/60).' minutes '.fmod($total_billsec,60). 'sec'
							,'total_expenses'=>round($total_expenses,2). ' Taka'
							
							);
		return $output;					
	}
	
	public function download_summary($output_id){
		ini_set('memory_limit', '-1');
		
		$temp=$this->prime_model->getByID('query','id',$output_id);
		$json=$temp['value'];
		
		
		$result=  json_decode($json,true);

		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=report.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		fprintf($output, chr(0xEF).chr(0xBB).chr(0xBF));
		fputcsv($output, array('From date',$result['from_date']));
		fputcsv($output, array('To date',$result['to_date']));
		fputcsv($output, array('Campaign',$result['campaign']));
		fputcsv($output, array('',''));
		fputcsv($output, array('',''));
		fputcsv($output, array('Total Connected calls',$result['total_connected_calls']));
		fputcsv($output, array('Total Not Connected calls',$result['total_not_connected_calls']));
		fputcsv($output, array('Total  calls',$result['total_calls']));
		fputcsv($output, array('Total  billsec',$result['total_billsec']));
		fputcsv($output, array('Total  expenses',$result['total_expenses']));
		/*foreach($result as $item)
		{
		
			fputcsv($output,array($item['campaign'],$item['tried_at'],$item['callerid'],$item['dst'],$item['report_status']
							,$item['duration'],$item['billsec']
		
			));
		}*/

		fclose($output);                 

	}

  
}
