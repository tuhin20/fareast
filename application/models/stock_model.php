<?php
class stock_model extends CI_Model
{
    var $table = 'stock';
	public $querystock="select * from naafco_predictive ";
    

	
	//public $orderPart=" order by client.name,client.id";
    public $orderPart=" order by product.id desc";


    public function __construct()
    {
        parent::__construct();
        //$this->load->model('utility_model');
        $this->load->helper('date');
        $this->load->database();

    }
	
	public function is_mobile_already_exist($task,$id,$mobile) {
		if(substr($mobile,0,2)=='88')
			$mobile=substr($mobile,2);
		else if(substr($mobile,0,3)=='+88')
			$mobile=substr($mobile,3); 
        //$mobile=$this->db->escape(trim($mobile)); //It also automatically adds single quotes around the data so you don’t have to do that as well.
		if($task=='add')
			$sql="select * from contact where  mobile like '%$mobile%'";
		else 
			$sql="select * from contact where id<> $id and mobile like '%$mobile%'";
        $contacts=$this->prime_model->getByQuery($sql);
        if(sizeof($contacts)>0)
            return true;
        else return false;
    }
	
	public function get_dashboard_info($params){
		//print_r($params);
		//$date="2019-12-10";
		$from_date=trim($params['from_date']);
		$to_date=trim($params['to_date']);
		
		$file_total=$this->prime_model->getByQuery("select file_name,count(*) as total from stock where direction='outgoing_c2c' and created_date between '$from_date 00:00:00' and '$to_date 23:59:59' group by file_name");
		$file_completed=$this->prime_model->getByQuery("select file_name, count(*) as completed from stock where direction='outgoing_c2c' and uniqueid !='' and created_date between '$from_date 00:00:00' and '$to_date 23:59:59'group by file_name");

		
		$agent_total=$this->prime_model->getByQuery("select user,count(*) as total from stock where direction='outgoing_c2c' and created_date between '$from_date 00:00:00' and '$to_date 23:59:59' group by user");
		$agent_completed=$this->prime_model->getByQuery("select user, count(*) as completed from stock where direction='outgoing_c2c' and uniqueid !='' and created_date between '$from_date 00:00:00' and '$to_date 23:59:59'group by user");
		
		//print_r($file_total);
		//print_r($file_completed);
		
		$output_file_wise=array();
		foreach($file_total as $item){
			$completed=0;
			foreach($file_completed as $temp){
				if($item['file_name']==$temp['file_name']){
					$completed=$temp['completed'];
					break;
				}
			}
			$output_file_wise[]=array('file_name'=>$item['file_name'],'total'=>$item['total'],'completed'=>$completed);
		}
		
		$output_agent_wise=array();
		foreach($agent_total as $item){
			$completed=0;
			foreach($agent_completed as $temp){
				if($item['user']==$temp['user']){
					$completed=$temp['completed'];
					break;
				}
			}
			$output_agent_wise[]=array('agent_name'=>$item['user'],'total'=>$item['total'],'completed'=>$completed);
		}
		
		//print_r($output_agent_wise);
		return array('output_file_wise'=>$output_file_wise,'output_agent_wise'=>$output_agent_wise);
		
	}
	public function save_collection($params){
		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

		
		
		//print_r($params);
        
		$collection_data=array('description' => $params['description']);
								
		if($params['id']==-1){
            unset($collection_data['id']);
			$collection_id=$this->prime_model->insert_details('collection', $collection_data);
			$code="s".str_pad("$collection_id",3,"0",STR_PAD_LEFT);
			$this->prime_model->update('collection',array('id'=>$collection_id,'code'=>$code));
			
		}
		else{
			$collection_id=$params['id'];
			$collection_data['id']=$params['id'];
			$this->prime_model->update_details('collection', $collection_data);
			//deleting existing stock related to collection_id during update
			$this->prime_model->executeQuery("delete from stock where table_name='collection' and table_link_id=$collection_id");
		}
		
		
		//save stock
		$arr_stock_id=$params['stock_id'];
		$arr_medicine_id=$params['medicine_id'];
		$arr_qty=$params['qty'];
		foreach($arr_stock_id as $key=>$value){
			if(isset($arr_medicine_id[$key])&&$arr_medicine_id[$key]>0){ //for new stock stock id=-1, we need to check if medicine selected or not
				$this->save_stock("collection",$collection_id,$arr_medicine_id[$key],$arr_qty[$key],'in','');
			}
		}
		
		
        
        
        $this->db->trans_complete(); # Completing transaction

		/*Optional*/

		if ($this->db->trans_status() === FALSE) {
			# Something went wrong.
			$this->db->trans_rollback();
			$success=false;
		} 
		else {
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
			$success=true;
		}
		
		if($success){
            return array('success'=>true,'collection_id'=>$collection_id,'msg'=>'collection Data saved successfully','test'=>$data);
        }
        else{
            return array('success'=>false,'msg'=>'Unable to save collection Data. Please try after sometime','test'=>$data);
        }
    }
	
	public function save_distribution($params){
		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

		
		
		//print_r($params);
        
		$distribution_data=array('name' => $params['name']
								,'type'=>$params['type']
								,'member_bp' => $params['member_bp']
								,'police_unit' => $params['police_unit']
								,'member_nid' => $params['member_nid']
								,'mobile'=>$params['mobile']
								,'present_address'=>$params['present_address']);
		if($params['id']==-1){
            unset($distribution_data['id']);
			$distribution_id=$this->prime_model->insert_details('distribution', $distribution_data);
			$code="p".str_pad("$distribution_id",3,"0",STR_PAD_LEFT);
			$this->prime_model->update('distribution',array('id'=>$distribution_id,'code'=>$code));
			
		}
		else{
			$distribution_id=$params['id'];
			$distribution_data['id']=$params['id'];
			$this->prime_model->update_details('distribution', $distribution_data);
			//deleting existing stock related to distribution_id during update
			$this->prime_model->executeQuery("delete from stock where table_name='distribution' and table_link_id=$distribution_id");
		}
		
		
		//save stock
		$arr_stock_id=$params['stock_id'];
		$arr_medicine_id=$params['medicine_id'];
		$arr_qty=$params['qty'];
		foreach($arr_stock_id as $key=>$value){
			if(isset($arr_medicine_id[$key])&&$arr_medicine_id[$key]>0){ //for new stock stock id=-1, we need to check if medicine selected or not
				$this->save_stock("distribution",$distribution_id,$arr_medicine_id[$key],$arr_qty[$key],'out','');
			}
		}
		
		
        
        
        $this->db->trans_complete(); # Completing transaction

		/*Optional*/

		if ($this->db->trans_status() === FALSE) {
			# Something went wrong.
			$this->db->trans_rollback();
			$success=false;
		} 
		else {
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
			$success=true;
		}
		
		if($success){
            return array('success'=>true,'distribution_id'=>$distribution_id,'msg'=>'Distribution Data saved successfully','test'=>$data);
        }
        else{
            return array('success'=>false,'msg'=>'Unable to save Distribution Data. Please try after sometime','test'=>$data);
        }
    }
	
	public function get_latest_price($medicine_id){		
		
		$latest_price=0;
		if($medicine_id>0){
			$result=$this->prime_model->getByQuery("select price from price_history where medicine_id=$medicine_id order by created_date desc limit 1");
			if(sizeof($result)>0)
				$latest_price=$result[0]['price'];
		}
		return $latest_price;
    }
	
    public function save_stock($table_name,$table_link_id,$medicine_id,$qty,$type,$description){		
		
		//$user=$this->user_model->get_current_user();
		//$params['user']=$user['name'];
		$medicine_name="";
		$price_at_that_time=0;
		if($medicine_id>0){
			$medicine=$this->prime_model->getByID('medicine','id',$medicine_id);
			$medicine_name=$medicine['name'];
			$latest_price=$this->get_latest_price($medicine_id);
		}
		$stock=array('type'=>$type,'table_name'=>$table_name,'table_link_id'=>$table_link_id
					,'medicine_id'=>$medicine_id,'medicine_name'=>$medicine_name
					,'qty'=>$qty,'price_at_that_time'=>$latest_price,'description'=>$description);
		
		return $this->prime_model->insert_details('stock', $stock);
    }
	
	   public function get_all_product_details()
    {
        $sql = "select * from product";

        $query = $this->db->query($sql);
        return $query->result();
    }
	
	public function delete_distribution($distribution_id){
		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

		$this->prime_model->executeQuery("delete from stock where table_name='distribution' and table_link_id=$distribution_id");
		$this->prime_model->executeQuery("delete from distribution where id=$distribution_id");
		
		
		
        
        
        $this->db->trans_complete(); # Completing transaction

		/*Optional*/

		if ($this->db->trans_status() === FALSE) {
			# Something went wrong.
			$this->db->trans_rollback();
			$success=false;
		} 
		else {
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
			$success=true;
		}
		
		return $success;
    }
	public function delete_collection($collection_id){
		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

		$this->prime_model->executeQuery("delete from stock where table_name='collection' and table_link_id=$collection_id");
		$this->prime_model->executeQuery("delete from collection where id=$collection_id");
		
		
		
        
        
        $this->db->trans_complete(); # Completing transaction

		/*Optional*/

		if ($this->db->trans_status() === FALSE) {
			# Something went wrong.
			$this->db->trans_rollback();
			$success=false;
		} 
		else {
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
			$success=true;
		}
		
		return $success;
    }
	
	public function get_qty_value($distributions,$medicine_id,$d){
		$value="";
		foreach($distributions as $item){
			if($item['medicine_id']==$medicine_id && $item['date']==$d){
				$value= $item['total'];
				break;
			}			
		}
		return $value;
	}
	public function get_stock_info($date){
		
		$sql="select m.name as medicine_name,m.id as medicine_id,ifnull(a.previous_in,0) as previous_in,ifnull(b.previous_out,0) as previous_out,ifnull(c.new_in,0)as new_in  from medicine m
			left join (select medicine_id,sum(qty) as previous_in from stock where type='in' and   created_date<'$date' group by medicine_id)a on m.id=a.medicine_id
			left join (select medicine_id,sum(qty) as previous_out from stock where type='out' and created_date<'$date' group by medicine_id)b on m.id=b.medicine_id
			left join (select medicine_id,sum(qty) as new_in from stock where type='in' and   created_date>='$date' group by medicine_id)c on m.id=c.medicine_id
			order by m.name

			";
		return $this->prime_model->getByQuery($sql);
	}
	
	public function prepare_data($from_date,$to_date){
		$dynamic_columns=array();
		//$rows=array();
		$report=array();
		
		$stock_info=$this->get_stock_info($from_date); //this contains all medicine regardless of in stock or not
		
		
		$sql="select medicine_id,DATE_FORMAT(created_date, '%d.%m.%Y')as 'date','out' as type,sum(qty) as total from stock 
				where type='out'  and created_date between '$from_date' and '$to_date'
				group by medicine_id,DATE_FORMAT(created_date, '%d.%m.%Y') order by created_date";
		//echo $sql;
		$distributions=$this->prime_model->getByQuery($sql);
		
		foreach($distributions as $item){
			if(!in_array($item['date'], $dynamic_columns)){
				$dynamic_columns[]=$item['date']; //dynamic columns and sorte
			}
		}
		
		//asort($rows);	
		$sl=1;
		foreach($stock_info as $s){
			$body=array(); //each body row
			$body[]=$sl;
			$body[]=$s['medicine_name'];
			$body[]=$s['previous_in']-$s['previous_out'];
			$body[]=$s['new_in'];
			$body[]=$s['previous_in']-$s['previous_out']+$s['new_in'];
			$value="";
			$total_distribution=0; // row each row total_distribution is initialized to 0
			if(sizeof($dynamic_columns)==0){
				$body[]="";
			}
			else{
				foreach($dynamic_columns as $d){
					$value=$this->get_qty_value($distributions,$s['medicine_id'],$d); //foreach date get  sum qty of that medicine
					$body[]=$value;
					if (is_numeric($value)) {
						$total_distribution +=$value;
					}
				}
			}
			$body[]=$total_distribution;
			$body[]=$s['previous_in']-$s['previous_out']+$s['new_in']-$total_distribution;
			$rows[]=$body; //at last adding to report
			$sl++;
		}
		
		return array('dynamic_columns'=>$dynamic_columns, 'rows'=>$rows);;
	}
	
	public function get_report($params){
		
		/*$this->load->helper('date');
        $datestring = "%Y-%m-%d  %H:%i:%s";
        $time = time();
        $data['created_date']=mdate($datestring, $time);*/
		if(!isset($params['from_date'])|| !isset($params['to_date'])){
			return array('dynamic_columns'=>array(), 'rows'=>array());;
		}
		else{
			return $this->prepare_data($params['from_date'],$params['to_date']);
		}
		
	}
	
	public function get_report2($params){
		if(!isset($params['from_date'])|| !isset($params['to_date'])){
			return array();;
		}
		
		$from_date=$params['from_date'].':00';
		$to_date=$params['to_date'].':59';
		
		$sql="select d.id,d.type,d.created_date,
			case
				WHEN d.type = 'bp' THEN ( select concat(COALESCE(name,''),'->',COALESCE(designation,''),'->',COALESCE(mobile,'') ,'->',COALESCE(signature,'') ) FROM member  where bp=d.member_bp )
				WHEN d.type = 'nid' THEN ( select concat(COALESCE(name,''),'->',COALESCE(designation,''),'->',COALESCE(mobile,'') ,'->',COALESCE(signature,'') ) FROM member  where nid=d.member_nid)
				WHEN d.type= 'unit' THEN d.police_unit 
			end as info_data,
			r.* from distribution d left join (select s.table_link_id,m.name as medicine_name,s.qty,s.price_at_that_time,s.qty*price_at_that_time as total_price from stock s inner join medicine m on s.medicine_id=m.id where s.table_name='distribution' order by m.name)r 
			on d.id=r.table_link_id where d.created_date
			between '$from_date' and '$to_date' order by d.created_date asc ";
			
		$result=$this->prime_model->getByQuery($sql);
		//echo $sql;
		//print_r($result);
		
		$dataset=array();
		
		$sl=0;
		$prev_id=-1;
		foreach($result as $item){
			if($item['id']!=$prev_id){
				$sl++;
				$info=explode("->",$item['info_data']);	
				$signature="";
				if(strlen($info[3])!=""){
					$src=base_url('sg/'.$info[3]);
					$signature="<img width='100px' height='50px' src='$src' >";
				}
				
				//$dataset[]=array('','','','','','','','','');
				$dataset[]=array($sl,$info[0],$info[1],$item['created_date'],$item['medicine_name'],$item['qty'],$item['total_price'],$info[2],$signature);
			}
			else{
				$dataset[]=array('','','','',$item['medicine_name'],$item['qty'],$item['total_price'],'','');			
			}
			
			$prev_id=$item['id'];
		}
		
		return $dataset;
		
		
	}
	public function download_report($from_date,$to_date){
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('report');
        //$query=$this->session->userdata ( 'query_smsc_report');
        

        $i=2;
        //Invoice(Mushok-11)
        $this->excel->getActiveSheet()->mergeCells("D$i:D3");
        $this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("D$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->getStyle("D$i")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue("D$i", $this->prime_model->getSystemName());

        $i=$i+2;
        //Report to
        $this->excel->getActiveSheet()->mergeCells("A$i:D$i");
        $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("A$i", $operator_details['report_to']);

        //invoice no
        $this->excel->getActiveSheet()->mergeCells("F$i:G$i");
        //$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setSize(12);
        //$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("F$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("F$i", 'Invoice no');

        $this->excel->getActiveSheet()->mergeCells("H$i:I$i");
        //$this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setSize(12);
        //$this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("H$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("H$i", ": ");

        $i=$i+1;
        //Company name
        $this->excel->getActiveSheet()->mergeCells("A$i:D$i");
        $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("A$i", $operator_details['company_name']);

        //Date of issue
        $this->excel->getActiveSheet()->mergeCells("F$i:G$i");
        //$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setSize(12);
        //$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("F$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("F$i", 'Date of issue');

        $this->excel->getActiveSheet()->mergeCells("H$i:I$i");
        //$this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setSize(12);
        //$this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("H$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("H$i", ": ". mdate('%d-%M-%Y',  now()));
        //end of first row

        $i=$i+1;
        //Company address
        $this->excel->getActiveSheet()->mergeCells("A$i:D$i");
        //$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setSize(12);
        //$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("A$i", $operator_details['company_address']);

        //period
        $this->excel->getActiveSheet()->mergeCells("F$i:G$i");
        //$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setSize(12);
        //$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("F$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("F$i", 'Period');

        $this->excel->getActiveSheet()->mergeCells("H$i:I$i");
        $this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("H$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        $from = new DateTime($from_date);//str_replace("-"," ",$from_date)
        $to=new DateTime($to_date);//str_replace("-"," ",$to_date)
        //echo $date->format('Y-m-d H:i:s');
        $this->excel->getActiveSheet()->setCellValue("H$i",": ". $from->format('d-M-Y ').' to '.$to->format('d-M-Y '));

        //end of second row

        $i=$i+1;

        //Service Provider Name
        $this->excel->getActiveSheet()->mergeCells("F$i:G$i");
        //$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setSize(12);
        //$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("F$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("F$i", 'Service Provider Name');

        $this->excel->getActiveSheet()->mergeCells("H$i:I$i");
        $this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("H$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("H$i",": Metronet Bangladesh Limited");

        $i=$i+1;

        //VAT Reg No
        $this->excel->getActiveSheet()->mergeCells("F$i:G$i");
        //$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setSize(12);
        //$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("F$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("F$i", 'VAT Reg No');

        $this->excel->getActiveSheet()->mergeCells("H$i:I$i");
        $this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("H$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("H$i",":");

        $i=$i+1;
        //attention person name
        //$this->excel->getActiveSheet()->mergeCells("A$i:D$i");
        $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("A$i",'Attention:');

        $this->excel->getActiveSheet()->mergeCells("B$i:D$i");
        //$this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setSize(12);
        //$this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("B$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("B$i", $operator_details['attention_person_name']);

        //Mushok-11 No
        $this->excel->getActiveSheet()->mergeCells("F$i:G$i");
        //$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setSize(12);
        //$this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("F$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("F$i", 'Mushok-11 No');

        $this->excel->getActiveSheet()->mergeCells("H$i:I$i");
        $this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("H$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("H$i",":");



        $i=$i+1;
        //attention person details
        $this->excel->getActiveSheet()->mergeCells("A$i:D$i");
        //$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setSize(12);
        //$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("A$i",$operator_details['attention_person_details']);

        $i=$i+1;
        //Customer VAT Reg no:
        $this->excel->getActiveSheet()->mergeCells("A$i:D$i");
        //$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setSize(12);
        //$this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("A$i","Customer VAT Reg No:");


        $i=$i+3;
        $this->excel->getActiveSheet()->setCellValue("A$i", "SL");
        //change the font size
        $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setSize(12);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);

        $this->excel->getActiveSheet()->setCellValue("B$i", 'Description');
        //change the font size
        $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setSize(12);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);

        //c
        $this->excel->getActiveSheet()->setCellValue("C$i", 'Total Calls');
        //change the font size
        $this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setSize(12);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);

        //D
        $this->excel->getActiveSheet()->setCellValue("D$i", 'Duration (second)');
        //change the font size
        $this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setSize(12);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle("D$i")->getFont()->setBold(true);

        //E
        $this->excel->getActiveSheet()->setCellValue("E$i", 'Duration (MM:SS)');
        //change the font size
        $this->excel->getActiveSheet()->getStyle("E$i")->getFont()->setSize(12);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle("E$i")->getFont()->setBold(true);

        //F
        $this->excel->getActiveSheet()->setCellValue("F$i", 'Rate');
        //change the font size
        $this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setSize(12);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle("F$i")->getFont()->setBold(true);

        //G
        $this->excel->getActiveSheet()->setCellValue("G$i", 'Total Amount');
        //change the font size
        $this->excel->getActiveSheet()->getStyle("G$i")->getFont()->setSize(12);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle("G$i")->getFont()->setBold(true);

        //H
        $str_vat="VAT (".$this->vat_operator."%)";
        $this->excel->getActiveSheet()->setCellValue("H$i", $str_vat);
        //change the font size
        $this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setSize(12);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle("H$i")->getFont()->setBold(true);

        //I
        $this->excel->getActiveSheet()->setCellValue("I$i", 'Total Amount (Including VAT)');
        //change the font size
        $this->excel->getActiveSheet()->getStyle("I$i")->getFont()->setSize(12);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle("I$i")->getFont()->setBold(true);


        $i=$i+1;
        $serial=1;
        $sum_of_number_of_calls=0;
        $sum_of_billsec=0;
        $sum_of_pulse=0;
        $sum_of_bill_amount=0;
        $sum_of_vat=0;
        $sum_of_bill_amount_with_vat=0;


        foreach ($report_data as $item){

            $cell_a="A$i";
            $cell_b="B$i";
            $cell_c="C$i";
            $cell_d="D$i";
            $cell_e="E$i";
            $cell_f="F$i";
            $cell_g="G$i";
            $cell_h="H$i";
            $cell_i="I$i";
            $this->excel->getActiveSheet()->setCellValue($cell_a, $serial);
            /*$output[]=array('description'=>$item['name'],'number_of_calls'=>$total_call_of_that_icx,'billsec'=>$total_bill_sec_of_that_icx,
            'pulse'=>$total_pulse_of_that_icx,'rate'=>$rate,
            'bill_amount'=>$total_bill_amount_of_that_icx,'vat'=>$total_vat,'bill_amount_with_vat'=>$total_bill_amount_with_vat_of_that_icx);*/
            $this->excel->getActiveSheet()->setCellValue($cell_b, $item['description']);

            $this->excel->getActiveSheet()->setCellValue($cell_c, number_format($item['number_of_calls'], 0, '.', ','));
            $this->excel->getActiveSheet()->getStyle($cell_c)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $this->excel->getActiveSheet()->setCellValue($cell_d, number_format($item['billsec'], 0, '.', ','));
            $this->excel->getActiveSheet()->getStyle($cell_d)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $this->excel->getActiveSheet()->setCellValue($cell_e, floor($item['pulse']/60).':'. fmod($item['pulse'],60));
            $this->excel->getActiveSheet()->getStyle($cell_e)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $this->excel->getActiveSheet()->setCellValue($cell_f, $item['rate']);
            $this->excel->getActiveSheet()->getStyle($cell_f)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $roundedUp=round( $item['bill_amount'], 3, PHP_ROUND_HALF_UP);
            $this->excel->getActiveSheet()->setCellValue($cell_g, number_format($roundedUp, 3, '.', ','));
            $this->excel->getActiveSheet()->getStyle($cell_g)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $roundedUp=round( $item['vat'], 3, PHP_ROUND_HALF_UP);
            $this->excel->getActiveSheet()->setCellValue($cell_h, number_format($roundedUp, 3, '.', ','));
            $this->excel->getActiveSheet()->getStyle($cell_h)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $roundedUp=round( $item['bill_amount_with_vat'], 3, PHP_ROUND_HALF_UP);
            $this->excel->getActiveSheet()->setCellValue($cell_i, number_format($roundedUp, 3, '.', ','));
            $this->excel->getActiveSheet()->getStyle($cell_i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $i++;
            $serial++;
            // total_calculation

            $sum_of_number_of_calls +=$item['number_of_calls'] ;
            $sum_of_billsec +=$item['billsec'];
            $sum_of_pulse +=$item['pulse'];
            $sum_of_bill_amount +=$item['bill_amount'];
            $sum_of_vat +=$item['vat'];
            $sum_of_bill_amount_with_vat +=$item['bill_amount_with_vat'];
        }
        //printing total 
        //i value have already increased in the last loop so no need to increase
        $cell_c="C$i";
        $cell_d="D$i";
        $cell_e="E$i";
        $cell_f="F$i";
        $cell_g="G$i";
        $cell_h="H$i";
        $cell_i="I$i";

        $this->excel->getActiveSheet()->setCellValue($cell_c, number_format($sum_of_number_of_calls, 0, '.', ','));
        $this->excel->getActiveSheet()->getStyle($cell_c)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle($cell_c)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $this->excel->getActiveSheet()->setCellValue($cell_d, number_format($sum_of_billsec, 0, '.', ','));
        $this->excel->getActiveSheet()->getStyle($cell_d)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle($cell_d)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $this->excel->getActiveSheet()->setCellValue($cell_e, floor($sum_of_pulse/60).':'. fmod($sum_of_pulse,60));
        $this->excel->getActiveSheet()->getStyle($cell_e)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $this->excel->getActiveSheet()->getStyle($cell_e)->getFont()->setBold(true);

        //cell f  is empty in total

        $roundedUp=round( $sum_of_bill_amount, 3, PHP_ROUND_HALF_UP);
        $this->excel->getActiveSheet()->setCellValue($cell_g, number_format($roundedUp, 3, '.', ','));
        $this->excel->getActiveSheet()->getStyle($cell_g)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $this->excel->getActiveSheet()->getStyle($cell_g)->getFont()->setBold(true);

        $roundedUp=round( $sum_of_vat, 3, PHP_ROUND_HALF_UP);
        $this->excel->getActiveSheet()->setCellValue($cell_h, number_format($roundedUp, 3, '.', ','));
        $this->excel->getActiveSheet()->getStyle($cell_h)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $this->excel->getActiveSheet()->getStyle($cell_h)->getFont()->setBold(true);

        $roundedUp=round( $sum_of_bill_amount_with_vat, 3, PHP_ROUND_HALF_UP);
        $this->excel->getActiveSheet()->setCellValue($cell_i, number_format($roundedUp, 3, '.', ','));
        $this->excel->getActiveSheet()->getStyle($cell_i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $this->excel->getActiveSheet()->getStyle($cell_i)->getFont()->setBold(true);

        $i=$i+2;
        //Amount in words
        //$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
        $this->excel->getActiveSheet()->mergeCells("A$i:I$i");
        $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        //$this->excel->getActiveSheet()->setCellValue("A$i", 'Amount (in word) : BDT  '.strtoupper($f->format($roundedUp)) .' ONLY');

        $i=$i+4;
        $this->excel->getActiveSheet()->mergeCells("A$i:D$i");
        $this->excel->getActiveSheet()->getStyle("A$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("A$i", '----------------------------');
        $i=$i+1;
        $this->excel->getActiveSheet()->mergeCells("A$i:D$i");
        $this->excel->getActiveSheet()->getStyle("A$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->excel->getActiveSheet()->setCellValue("A$i", 'Authorized Signature');

        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:D1');
        //set aligment to center for that merged cell (A1 to D1)
        //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $filename='operator_report.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

    }
	
	public function get_duplicate_contact_details()
    {
		$duplicates=array();
		$sql = "select t1.mobile,t2.mobile,count(*),group_concat(t2.id) as contact_ids from contact t1 inner join contact t2 on t2.mobile like concat('%',t1.mobile) where t1.mobile !='' group by t1.mobile having count(*) > 1 ";
        $result= $this->prime_model->getByQuery($sql);
		foreach($result as $row){
			$temp_result=$this->prime_model->getByQuery("select * from contact where id in (".$row['contact_ids'].")");
			foreach($temp_result as $item)
				$duplicates[]=$item;
		}
		return $duplicates;
    }
	
	   public function get_stock_by_id($id)
    {
        $query=  $this->querystock. "where naafco_predictive.id=$id";
        $results=  $this->prime_model->getByQuery($query);
        if(sizeof($results)>0)
            return $results[0];
        else return array();
    }
	
		public function insert_naafco_predictive($data)
	{
		$this->db->insert_batch('naafco_predictive', $data);
	}
    
       public function get_contact_by_primary_number($primary_number)
    {
        $sql = "select * from contact where mobile like '%$primary_number%' or office_phone like '%$primary_number%' or home_phone like '%$primary_number%'";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    
	
	  public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}
	
	public function insert_imported_data($data)
	{
		$this->db->insert_batch('naafco_predictive', $data);
	}
	
	public function download_c2c(){
			ini_set('memory_limit', '-1');
		    
			$sql="select * from stock  where 1  ";
		
			if(!$this->user_model->is_admin()){
				$user=$this->user_model->get_current_user();
				$name=$user['name'];
				$sql .=" and stock.user='$name' ";
			}
		
			$query_id =$this->session->userdata('query_id');
			$temp=$this->prime_model->getByID('query','id',$query_id);
			$sql .=$temp['value'];
			
			
            $cdr_data=  $this->prime_model->getByQuery($sql);

            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=report.csv');

            // create a file pointer connected to the output stream
            $output = fopen('php://output', 'w');
			fprintf($output, chr(0xEF).chr(0xBB).chr(0xBF));
            fputcsv($output, array('Created Time','user', 'msisdn','Counselor','Time','First Name', 'Last Name','Your Phone','Email','District You are in','Age','Education','Work Experience'
			,'Program','Current Status','Extra Comments','Company','Types of call','status','file name'
									
			));

            foreach($cdr_data as $item)
            {
			
				fputcsv($output,array($item['created_date'],$item['user'],$item['msisdn'],$item['counselor'],$item['time']
								,$item['first_name'],$item['last_name'],$item['your_phone'],$item['email'],$item['district'],$item['age'],$item['education']
								,$item['work_experience'],$item['program'],$item['current_status'],$item['extra_comments'],$item['company'],$item['types_of_call']
								,$item['status'],$item['file_name']
			
				));
            }

            fclose($output);                 

        }
		
		

  
}
