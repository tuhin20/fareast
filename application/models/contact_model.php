<?php
class contact_model extends CI_Model
{
    var $table = 'contact';
	public $queryContact="select * from contact ";
	
	public $queryTicket="select * from ticket ";
	
	public $orderPart=" order by contact.id desc";
    

	
	//public $orderPart=" order by client.name,client.id";
    //public $orderPart=" order by client.id desc";


    public function __construct()
    {
        parent::__construct();
        //$this->load->model('utility_model');
        $this->load->helper('date');
        $this->load->database();

    }
	public function add_team($data)
   {
        $this->db->insert('team',$data);
        return $this->db->insert_id(); 
   }

    public function ticket_status($data1)
    {

        $this->db->insert('ticket_status', $data1);
        return $this->db->insert_id();
    }

    public function assigned_to($data2)
    {

        $this->db->insert('assigned_to', $data2);
        return $this->db->insert_id();
    }

    public function solved_by($data3)
    {

        $this->db->insert('solved_by', $data3);
        return $this->db->insert_id();
    }


    public function problem_type($data4)
    {

        $this->db->insert('problem_type', $data4);
        return $this->db->insert_id();
    }





    public function is_mobile_already_exist($task,$id,$mobile) {
		if(substr($mobile,0,2)=='88')
			$mobile=substr($mobile,2);
		else if(substr($mobile,0,3)=='+88')
			$mobile=substr($mobile,3); 
        //$mobile=$this->db->escape(trim($mobile)); //It also automatically adds single quotes around the data so you don’t have to do that as well.
		if($task=='add')
			$sql="select * from contact where  mobile like '%$mobile%'";
		else 
			$sql="select * from contact where id<> $id and mobile like '%$mobile%'";
        $contacts=$this->prime_model->getByQuery($sql);
        if(sizeof($contacts)>0)
            return true;
        else return false;
    }

    public function save($params){
		//print_r($params);
        
        $data = array( 'id'=>$params['id'],
             //will unset during insert
            'name' => $params['name'],
          //  'designation' => $params['designation'],
         //   'pf_number' => $params['pf_number'],
            'telephone' => $params['telephone'],
             'email' => $params['email'],
          'address' => $params['address']

            
            );
		$contact_id=-1;
        if($params['id']==-1){
            unset($data['id']);
            $contact_id=$this->prime_model->insert_details('contact', $data);
			$success=true;
        }
        
        else{
            $contact_id=$params['id'];
            $data=$this->prime_model->update_details('contact', $data);
            $success=true;
        }
		
		
        if($success){
            return array('success'=>true,'msg'=>'Contact saved successfully','test'=>$data);
        }
        else{
            return array('success'=>false,'msg'=>'Unable to save Contact. Please try after sometime','test'=>$data);
        }
    }
	
	public function save_ticket($params){
		//print_r($params);
        
        $data = array( 'id'=>$params['id'],
             //will unset during insert
            'name' => $params['name'],
            'email' => $params['email'],
            'address' => $params['address'],
            'telephone' => $params['telephone'],


            'team' => $params['team'],
            'assigned_to' => $params['assigned_to'],
            'solved_by' => $params['solved_by'],
            'ticket_status' => $params['ticket_status'],
            'problem_type' => $params['problem_type'],
			'remarks' => $params['remarks']
            
            );


		$contact_id=-1;
        if($params['id']==-1){
            unset($data['id']);
            //$contact_id=$this->prime_model->insert_details('ticket', $data);
			$api_key="C20004225f1ba5c25f01b3.02267235";
			$prefix='88';
			$mobile=$data['telephone'];
			//$mobile='01786739367';
			$receiver=$prefix.$mobile;

			$senderid='8809612244244';
			$msg='Dear%20Colleague%0AYour%20Complain%20has%20been%20Received.%0A%0ARegards%2C%0AIT%20Department%0ASonyRangs';

			$sms_dlr=file_get_contents("http://103.36.103.64/smsapi?api_key=$api_key&type=text&contacts=$receiver&senderid=$senderid&msg=$msg");
			//print_r($url);
			$ticket_id=$this->prime_model->insert_ticket_details('ticket', $data);
			$ticket_no=date('My') . str_pad($ticket_id, 4, "0", STR_PAD_LEFT);
			$this->prime_model->update('ticket', array('id'=>$ticket_id,'ticket_no'=>$ticket_no,'sms_dlr'=>$sms_dlr));
			$success=true;
        }
        
        else{
            $contact_id=$params['id'];
			if($data['ticket_status']=='Closed')
			{
			$api_key="C20004225f1ba5c25f01b3.02267235";
			$prefix='88';
			$mobile=$data['telephone'];
			//$mobile='01786739367';
			$receiver=$prefix.$mobile;

			$senderid='8809612244244';
			$msg='Dear%20Colleague%0AYour%20Problem%20has%20been%20Solved.%0A%0ARegards%2C%0AIT%20Department%0AUniGroup';

			$sms_dlr=file_get_contents("http://103.36.103.64/smsapi?api_key=$api_key&type=text&contacts=$receiver&senderid=$senderid&msg=$msg");
				
			}
            $data=$this->prime_model->update_details('ticket', $data);
			
            $success=true;
        }
		
		
        if($success){
            return array('success'=>true,'msg'=>'Ticket saved successfully','test'=>$data);
        }
        else{
            return array('success'=>false,'msg'=>'Unable to save Ticket. Please try after sometime','test'=>$data);
        }
    }
	
	   public function get_all_contact_details()
    {
        $sql = "select * from contact";

        $query = $this->db->query($sql);
        return $query->result();
    }
	
	public function get_duplicate_contact_details($id)
    {
		$duplicates=array();
		$sql = "select t1.mobile,t2.mobile,count(*),group_concat(t2.id) as contact_ids from contact t1 inner join contact t2 on t2.mobile like concat('%',t1.mobile) where t1.mobile !='' group by t1.mobile having count(*) > 1 ";
        $result= $this->prime_model->getByQuery($sql);
		foreach($result as $row){
			$temp_result=$this->prime_model->getByQuery("select * from contact where id in (".$row['contact_ids'].")");
			foreach($temp_result as $item)
				$duplicates[]=$item;
		}
		return $duplicates;
    }
	
	public function get_contact_by_id($id)
    {
        $query=  $this->queryContact. "where contact.id=$id";
        $results=  $this->prime_model->getByQuery($query);
        if(sizeof($results)>0)
            return $results[0];
        else return array();
    }
    
	public function get_ticket_by_id($id)
    {
        $query=  $this->queryTicket. "where ticket.id=$id";
        $results=  $this->prime_model->getByQuery($query);
        if(sizeof($results)>0)
            return $results[0];
        else return array();
    }
	
       public function get_contact_by_primary_number($primary_number)
    {
        $sql = "select * from contact where mobile like '%$primary_number%' or office_phone like '%$primary_number%' or home_phone like '%$primary_number%'";
        $query = $this->db->query($sql);
        return $query->result();
    }
	
	public function download_contact_chart(){


            $isd_data=  $this->prime_model->getByQuery($this->queryContact.$this->orderPart);
       /* echo '<pre>';
        print_r($isd_data);
        die();*/

            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=contact.csv');

            // create a file pointer connected to the output stream
            $output = fopen('php://output', 'w');

            fputcsv($output, array('ID'



			,'Name'
			, 'Email'
			, 'Telephone'
			,'Address'
			,'Created_by'
			,'Created_date'
			,'Modified_by'
			,'Modified_date'
			/*,'Issue Date'
			,'Expiry Date'
			,'Concerned Employee'
			,'Lead Source'
			,'Group Name'
			,'Mobile'
			,'Office Phone'
			,'Home Phone'
			,'Mailing Street'
			,'Mailing PS'
			,'Mailing City'
			,'Mailing Postal'
			,'Mailing Country'
			,'Comments'*/
			));

            foreach($isd_data as $item)
            {
			
			fputcsv($output,array(
                $item['id']
			    , $item['name']
                , $item['email']
                 , $item['telephone']
                  , $item['address']
                 ,   $item['created_by']
                  ,   $item['created_date']
                  ,    $item['modified_by']
                   ,    $item['modified_date']



			/* $item['id']
			,$item['first_name']
			,$item['middle_name']
			,$item['last_name']
			,$item['email']
			,$item['company_name']
			,$item['department']
			,$item['birth_date']
			,$item['passport_number']
			,$item['issue_date']
			,$item['expiry_date']
			,$item['concerned_employee']
			,$item['lead_source']
			,$item['group_name']
			,$item['mobile']
			,$item['office_phone']
			,$item['home_phone']
			,$item['mailing_street']
			,$item['mailing_ps']
			,$item['mailing_city']
			,$item['mailing_postal']
			,$item['mailing_country']
			,$item['comments'],*/

			
			));
            }


            fclose($output);                 

        }

    public function download_contact_chart_ticket(){


        $isd_data=  $this->prime_model->getByQuery($this->queryContact.$this->orderPart);
        /* echo '<pre>';
         print_r($isd_data);
         die();*/

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=contact.csv');

        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        fputcsv($output, array('ID'


            ,'Issue Date'
            ,'Expiry Date'
            ,'Concerned Employee'
            ,'Lead Source'
            ,'Group Name'
            ,'Mobile'
            ,'Office Phone'
            ,'Home Phone'
            ,'Mailing Street'
            ,'Mailing PS'
            ,'Mailing City'
            ,'Mailing Postal'
            ,'Mailing Country'
            ,'Comments'
        ));

        foreach($isd_data as $item)
        {

            fputcsv($output,array(


                 $item['id']
                ,$item['first_name']
                ,$item['middle_name']
                ,$item['last_name']
                ,$item['email']
                ,$item['company_name']
                ,$item['department']
                ,$item['birth_date']
                ,$item['passport_number']
                ,$item['issue_date']
                ,$item['expiry_date']
                ,$item['concerned_employee']
                ,$item['lead_source']
                ,$item['group_name']
                ,$item['mobile']
                ,$item['office_phone']
                ,$item['home_phone']
                ,$item['mailing_street']
                ,$item['mailing_ps']
                ,$item['mailing_city']
                ,$item['mailing_postal']
                ,$item['mailing_country']
                ,$item['comments'],


            ));
        }


        fclose($output);

    }
		public function crm_call($id){
		$query=  $this->queryContact. "where contact.id=$id";
        $results=  $this->prime_model->getByQuery($query);
		$phone= $results[0]['telephone'];
		$data = array(
            'number' => $phone
            
            );
		
		$this->prime_model->insert_call_details('lms_call', $data);
			
			
		}
		
		public function crm_ticket_call($id){
		$query=  $this->queryTicket. "where ticket.id=$id";
        $results=  $this->prime_model->getByQuery($query);
		$phone= $results[0]['telephone'];
		$data = array(
            'number' => $phone
            
            );
		
		$this->prime_model->insert_call_details('lms_call', $data);
			
			
		}
		
		public function download_cdr_chart(){

            ini_set('memory_limit', '-1');
		    $query_id =$this->session->userdata('query_id');
			
			$temp=$this->prime_model->getByID('query','id',$query_id);
			$sql=$temp['value'];
			$sql="select * $sql";
			
            $cdr_data=  $this->prime_model->getByQuery($sql);
			
    

            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=ticket.csv');

            // create a file pointer connected to the output stream
            $output = fopen('php://output', 'w');
            fputcsv($output, array(
			'Ticket Number'
			, 'Name'
			, 'Email'
			,'Address'
			,'Telephone'
            ,'Ticket Status'
			,'Remarks'
			,'Received By'
			,'Assigned To'
			,'Solved By'
			,'Created Time'
			,'Updated Time'
			
			));

            foreach($cdr_data as $item)
            {
			
			fputcsv($output,array(
			$item['ticket_no']
			,$item['name']
			,$item['email']
			,$item['address']
			,$item['telephone']
            ,$item['ticket_status']
			,$item['remarks']
			,$item['created_by']
			,$item['assigned_to']
			,$item['solved_by']
			,$item['created_date']
			,$item['modified_date'],
			
			
			));
            }

            fclose($output);                 

        }
    
    
	
	  public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('contact');
	}

    public function delete_by_id_ticket_list($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('ticket');
	}
	
	


    


}
