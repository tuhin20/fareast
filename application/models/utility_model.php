<?php
class utility_model extends CI_Model {

    public $MyKey ='!#v*df';
    public function __construct() {
        parent::__construct();

    }
    public function encrypt($data)
    {
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($this->MyKey),  $data, MCRYPT_MODE_CBC, md5(md5($this->MyKey))));
        return $encrypted;
    }
    public function decrypt($encryptedData)
    {
        $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($this->MyKey), base64_decode($encryptedData), MCRYPT_MODE_CBC, md5(md5($this->MyKey))), "\0");
        return $decrypted;
    }


}