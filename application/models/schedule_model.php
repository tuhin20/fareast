<?php
class schedule_model extends CI_Model
{
    var $table = 'blast';
	public $queryblast="select * from naafco_predictive ";
    

	
	//public $orderPart=" order by client.name,client.id";
    public $orderPart=" order by product.id desc";


    public function __construct(){
        parent::__construct();
        //$this->load->model('utility_model');
        $this->load->helper('date');
        $this->load->database();
		$this->load->library('excel');

    }
	
	
	
	public function process_scheduled_blast(){
		
        //$this->prime_model->executeQuery("LOCK TABLE voice_blast WRITE");
		
		//sleep(10);
		
		$schedule_blasts=$this->prime_model->getByQuery("select * from voice_blast where is_scheduled='Y' and status='Approved'  and schedule_processed=0  and schedule_date<=now()");
		
		foreach($schedule_blasts as $item){
			$this->prime_model->executeQuery("update voice_blast set schedule_processed=-1,schedule_process_time=now() where id=$item[id]"); // processing 
		}
		
		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

		
		foreach($schedule_blasts as $item){
			$this->blast_model->push_to_blast_log($item['id']);
			$this->prime_model->executeQuery("update voice_blast set schedule_processed=1,schedule_process_time=now() where id=$item[id]");
		}
		
		
		
		$this->db->trans_complete(); # Completing transaction

		/*Optional*/

		if ($this->db->trans_status() === FALSE) {
			# Something went wrong.
			$this->db->trans_rollback();
			$success=false;
		} 
		else {
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
			$success=true;
		}
		
		if($success){
            return array('success'=>true,'msg'=>'Approved successfully');
        }
        else{
            return array('success'=>false,'msg'=>'Unable to approve');
        }
	}

  
}
