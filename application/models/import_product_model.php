<?php
class import_product_model extends CI_Model
{
    public $generalQuery="select * from contact";
    public $orderPart=" order by sale.customer_name,sale.id";
	public $queryNonExistingCustomer="select * from sale where mobile_phone_primary not in (select distinct mobile_phone_primary from customer) ";

        public function __construct() {
                parent::__construct();
                $this->load->model('utility_model');
                $this->load->model('prime_model');

        }

        public function save_isd_rate_chart($params)
        {
            $data = array(
                        'id'=>$params['id'], //will unset during insert
                        'name' => $params['name'],
                        'prefix'=>$params['prefix'],
                        'rate'=>$params['rate']
                        );
            if($params['id']==-1){
                    unset($data['id']);
                    $data=$this->prime_model->insert_details('isd_rate_chart', $data);
                    $success=true;
            }
            else{
                $data=$this->prime_model->update_details('isd_rate_chart', $data);
                $success=true;
            }

            if($success){
                    return array('success'=>true,'msg'=>'Product list saved successfully','test'=>$data);
            }
            else {
                    return array('success'=>false,'msg'=>'Unable to save Product list. Please try after sometime','test'=>$data);
            }
        }
        public function save_imported_product($result){
            //the first row 
            if(sizeof($result)<=1){
                return array('success'=>true,'msg'=>"Nothing to save.");
            }
            $first_row=true;
			$table_data=array();           
            //print_r($db_result);
            
            foreach($result as $value){
                if($first_row==TRUE){
                    $first_row=false;
                    continue;
                }
                /*$name = trim($value[0]);
                $prefix = trim($value[1]);
                $rate = trim($value[2]);*/
				
				
				/*$name = trim($value[0]);
				$designation = trim($value[1]);
				$pf_number = trim($value[2]);
				$telephone = trim($value[3]);*/


                $name = trim($value[0]);
                $email = trim($value[1]);
                $telephone = trim($value[2]);
                $address = trim($value[3]);

				
				
				
				
				
				
		        //validation
                if( (strlen($telephone) == 0)  ){
                        continue;
                }
                //end of validation
               
                    
				$contact =  array(
							
							/*'name' =>$name,
							'designation' =>$designation,
							'pf_number' =>$pf_number,
							'telephone' =>$telephone*/

                    'name' =>$name,
                'email' =>$email,
                'address' =>$address,
                'telephone' =>$telephone

							
				); 
				
				
				
				$table_data[]=$contact;
			} //end of foreach
			
			//print_r($table_data);
			// end of foreach
			$this->prime_model->bulkInsert('contact',$table_data);
			
			
			
			
			
			
			
            return array('success'=>true,'msg'=>'data imported successfully');
            //return array('success'=>false,'msg'=>"Unable to send SMS. Please try after sometime.");
            
        }

        public function get_isd_rate_chart_list()
        {
            $sql=  $this->generalQuery.$this->orderPart;
            $query=$this->db->query($sql);
            return $query;
        }

        public function download_isd_rate_chart(){

            $isd_data=  $this->prime_model->getByQuery($this->generalQuery.$this->orderPart);

            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=isd_rate_chart.csv');

            // create a file pointer connected to the output stream
            $output = fopen('php://output', 'w');
            fputcsv($output, array('Name', 'Prefix', 'Rate'));

            foreach($isd_data as $item)
            {
                fputcsv($output,array($item['name'],$item['prefix'],$item['rate'],));
            }

            fclose($output);                 

        }
        public function import_isd_rate_chart(){
            
        }
        public function download_isd_rate_chart_bak(){
            //load our new PHPExcel library
            $this->load->library('excel');
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('ISD Rate Chart');
            //$query=$this->session->userdata ( 'query_smsc_report');
            $isd_data=  $this->prime_model->getByQuery($this->generalQuery.$this->orderPart);


            $i=2;
            //Client name
            $this->excel->getActiveSheet()->mergeCells("A$i:C$i");
            $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setSize(14);
            $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle("A$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->setCellValue("A$i", 'Metronet Bangladesh Limited');

            $i=$i+1;
            //Client name
            $this->excel->getActiveSheet()->mergeCells("A$i:C$i");
            $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setSize(12);
            $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle("A$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->setCellValue("A$i", 'ISD Rate');




            $i=$i+3;
            $this->excel->getActiveSheet()->setCellValue("A$i", 'Name');
            //change the font size
            $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setSize(12);
            //make the font become bold
            $this->excel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);

            $this->excel->getActiveSheet()->setCellValue("B$i", 'Prefix');
            //change the font size
            $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setSize(12);
            //make the font become bold
            $this->excel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);

            //c
            $this->excel->getActiveSheet()->setCellValue("C$i", 'Rate');
            //change the font size
            $this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setSize(12);
            //make the font become bold
            $this->excel->getActiveSheet()->getStyle("C$i")->getFont()->setBold(true);


            $i=$i+1;
            $data_array=array();
            $cell_to_start="A$i";
            foreach ($isd_data as $item){

                /*$cell_a="A$i";
                $cell_b="B$i";
                $cell_c="C$i";
                $cell_d="D$i";
                $cell_e="E$i";
                $cell_f="F$i";
                $cell_g="G$i";
                $cell_h="H$i";
                $cell_i="I$i";*/



                $data_array[]=array(
                                $item['name']
                                ,$item['prefix']
                                ,$item['rate']
                                );

                            /*$this->excel->getActiveSheet()->setCellValue($cell_a,$serial)
                                        ->setCellValue($cell_b, $item['src'])
                                        ->setCellValue($cell_c, $item['dst'])
                                        ->setCellValue($cell_d, number_format($item['billsec'], 0, '.', ','))
                                        ->setCellValue($cell_e, floor($item['pulse']/60).':'. fmod($item['pulse'],60))
                                        ->setCellValue($cell_f, number_format($roundedUpBillAmount, 3, '.', ','))
                                        ->setCellValue($cell_g, number_format($roundedUpVat, 3, '.', ','))
                                        ->setCellValue($cell_h, number_format($roundedUpBillAmountWithVat, 3, '.', ','))
                                        ;*/




            }
            $this->excel->getActiveSheet()->fromArray($data_array, null, $cell_to_start);


            $filename='isd_rate.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');

        }

/*
//  Include PHPExcel_IOFactory
include 'PHPExcel/IOFactory.php';

$inputFileName = 'sample.xls';

//  Read your Excel workbook
try {
$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$objPHPExcel = $objReader->load($inputFileName);
} catch(Exception $e) {
die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
}

//  Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(0); 
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();

//  Loop through each row of the worksheet in turn
for ($row = 1; $row <= $highestRow; $row++){ 
//  Read a row of data into an array
$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                            NULL,
                            TRUE,
                            FALSE);
//  Insert row data array into database here using your own structure
* 
*      */

        public function get_isd_rate_chart_by_id($isd_rate_chart_id)
        {
             return $this->prime_model->getByID('isd_rate_chart','id',$isd_rate_chart_id);

        }

        public function delete_isd_rate_chart($id){
                $this->db->where('id', $id);
                $this->db->delete('isd_rate_chart');
                return true;		
        }

}
?>