<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class schedule extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->model('schedule_model');
        $this->load->model('blast_model');
		$this->load->model('prime_model');
        $this->load->model('user_model');
		$this->load->library('excel');
        $this->load->helper(array('form', 'url'));
		//$this->checkAuthorization();
		
    }
	
	public function checkAuthorization()
        {
			
			
            $current_user=$this->user_model->get_current_user();
            if(!$this->user_model->is_logged_in()){
				$this->session->set_userdata(array('redirect_after_login'=>current_url()));
                redirect('/login/index');
                return;
            }
            /*$hasPermission=$this->user_model->has_permission_for_role($this->role_manager_model->See_isd_rate_chart);
            if(!$hasPermission){
                redirect('/login/index');
                return;
            }*/
        }

    
	public function process_scheduled_blast(){
		
		/*$current_user=$this->user_model->get_current_user();
		print_r($current_user);*/
        $this->schedule_model->process_scheduled_blast();
    }
	

}
