<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class contact extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->model('contact_model');
        $this->load->model('prime_model');
        $this->load->model('user_model');
        $this->load->helper(array('form', 'url'));
        //$this->checkAuthorization();

    }

    public function checkAuthorization()
    {


        //$current_user=$this->user_model->get_current_user();
        if(!$this->user_model->is_logged_in()){
            //$this->session->set_userdata(array('redirect_after_login'=>site_url("ticket/search/$caller_number")));
            redirect('/login/index');
            return;
        }
        /*$hasPermission=$this->user_model->has_permission_for_role($this->role_manager_model->See_isd_rate_chart);
        if(!$hasPermission){
            redirect('/login/index');
            return;
        }*/
    }

    public function create(){


        //$primary_number=$this->uri->segment(3);
        $data['ui']=array('title'=>'Create','action'=>site_url('contact/save'),'okButton'=>'Save');
        //$data['params']=array('mobile'=>$primary_number);
        //$data['sale_info']=array();
        //$data['child_view']=$this->load->view('controls/ctrl_select_customer', $data, TRUE);

        $this->load->view('contact/create', $data );

    }

    public function create_ticket(){
/*email send with submit*/



        $contact_id=$this->input->get('contact_id');
        if($contact_id!=''){
            //$sale_id=$sale_id;
            $contact_info=$this->prime_model->getByID('contact','id',$contact_id);

                $data['params']=array('name'=>$contact_info['name'],
                'contact_id'=>$contact_info['id'],
                'email'=>$contact_info['email'],
                'address'=>$contact_info['address'],
                'telephone'=>$contact_info['telephone'],



            );


        }
        $data['ui']=array('title'=>'Create','action'=>site_url('contact/save_ticket'),'okButton'=>'Save');


        $this->load->view('contact/create_ticket', $data );

    }

    public function display_phonebook(){

        $sql ="select * from contact";

        $contacts=$this->prime_model->getByQuery($sql) ;
        $contact_xml="<YealinkIPPhoneDirectory>";
        foreach($contacts as $item){
            $name=$item['name'];
            $telephone=$item['telephone'];
            $contact_xml.="<DirectoryEntry><Name>$name</Name><Telephone>$telephone</Telephone></DirectoryEntry>";

        }

        $contact_xml.="<SoftKeyItem><Name>0</Name><URL>https://172.16.12.250/remote_pb/test.xml</URL></SoftKeyItem></YealinkIPPhoneDirectory>";
        echo $contact_xml;

    }


    public function save(){


        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $this->form_validation->set_rules('name','Name','trim|required');
        $this->form_validation->set_rules('telephone','Telephone','trim|required');
        //$this->form_validation->set_message('is_unique_mobile_for_add','Mobile Number must be unique.');

        $data=array('ui'=>array('title'=>'Create','action'=>site_url('contact/save'),'okButton'=>'Save')
        ,'params'=>$params
        );
        if($this->form_validation->run()){
            $return_value= $this->contact_model->save($params);

            $this->session->set_flashdata('return_value', $return_value);
            redirect('contact/create'); //for clearing input fields [stackoverflow :) ]
        }
        $this->load->view('contact/create', $data);

    }

    public function save_ticket(){


        $this->load->library('email');
        $email =$this->input->post('email');
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => '',
            'smtp_port' => 465,
            'smtp_user' => '',
            'smtp_pass' => '',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'


        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");


        $this->email->from('');
        $this->email->to($email);

        $this->email->subject('this is test email');
        // $message = $this->load->view('emailview');
        $this->email->message('Please check this ');
        $this->email->set_mailtype("html");
        $this->email->send();

        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $this->form_validation->set_rules('name','Name','trim|required');
        $this->form_validation->set_rules('telephone','Telephone','trim|required');
        //$this->form_validation->set_message('is_unique_mobile_for_add','Mobile Number must be unique.');

        $data=array('ui'=>array('title'=>'Create','action'=>site_url('contact/save_ticket'),'okButton'=>'Save')
        ,'params'=>$params
        );
        if($this->form_validation->run()){
            $return_value= $this->contact_model->save_ticket($params);

            $this->session->set_flashdata('return_value', $return_value);
            redirect('contact/create_ticket'); //for clearing input fields [stackoverflow :) ]
        }
        $this->load->view('contact/create_ticket', $data);

    }
    function is_unique_mobile_for_add($str){
        $field_value = $str; //this is redundant, but it's to show you how
        //the content of the fields gets automatically passed to the method
        if($this->contact_model->is_mobile_already_exist('add',$id=(int)$this->input->post('id'),$str)){
            return false;
        }
        else return true;
    }

    public function search(){
        $primary_number=$this->input->post('primary_number');
        $conditions='';
        $query_id=0;

        $sql=" from contact where 1 ";
        if($primary_number!=''){
            $conditions .=" and ( telephone like '%$primary_number%' ) ";
        }
        if(strlen($conditions)> 0){
            $sql .=$conditions;
            $query_id=$this->prime_model->insert("query",array('value'=>$sql));
        }
        $record_count=$this->get_count("select count(*)as total $sql");
        echo json_encode(array('record_count'=>$record_count,'query_id'=>$query_id,'conditions'=>$primary_number));




        if($start_date!=''){
            $sql .=" and (created_date >='$start_date 00:00:00') ";
        }

        if($end_date!=''){
            $sql .=" and (created_date <='$end_date 23:59:59') ";
        }



        //$sql .=" order by calldate desc";



    }
    public function search_ticket(){
        $params=$this->input->post(null);

        $received_by=addslashes(trim($params['received_by']));

        $solved_by=addslashes(trim($params['solved_by']));
        $start_date=addslashes(trim($params['start_date']));
        $end_date=addslashes(trim($params['end_date']));


        $conditions='';
        $query_id=0;



        $sql=" from ticket where 1 ";  //select *

        if($received_by!=''){
            $sql .=" and (created_by='$received_by') ";
        }
        if($solved_by!=''){
            $sql .=" and (solved_by='$solved_by') ";
        }
        if($start_date!=''){
            $sql .=" and (created_date >='$start_date 00:00:00') ";
        }

        if($end_date!=''){
            $sql .=" and (created_date <='$end_date 23:59:59') ";
        }


        //$sql .=" order by calldate desc";


        $query_id=$this->prime_model->insert("query",array('value'=>$sql));

        $this->session->set_userdata(array('query_id'=>$query_id));
        //$txt = "sohan writes the file";
        $txt = print_r($this->session->userdata('query_id'), true);//;
        file_put_contents('/var/www/html/cdr/query.txt', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
        //echo "Current User: ". $this->session->userdata('current_user');
        $records_total=$this->get_count("select count(*)as total $sql");
        //echo $sql;
        echo json_encode(array('query_id'=>$query_id,'records_total'=>$records_total));
    }

    public function search_by_group(){
        $params=$this->input->post(null);
        $group_name=addslashes(trim($params['group_name']));

        $conditions='';
        $query_id=0;

        $sql="select * from contact where 1 ";
        if($group_name!=''){
            $conditions .=" and group_name like '%$group_name%'";
        }

        if(strlen($conditions)> 0){
            $sql .=$conditions;
            $query_id=$this->prime_model->insert("query",array('value'=>$conditions));
        }
        $records_total=$this->get_count($sql);
        echo json_encode(array('records_total'=>$records_total,'query_id'=>$query_id));
    }

    public function get_count($sql){
        $query = $this->db->query($sql);
        return $query->num_rows($query);
    }

    public function get_count_faster($sql){
        $result=$this->prime_model->getByQuery($sql); //select count(*)as total from ($sql)as mytable
        return $result[0]['total'];
        //$query = $this->db->query($sql);
        //return $query->num_rows($query);
    }
    /*public function get_initial_parameters(){
        $recordsTotal=$this->get_count("select * from contact where 1");
        echo json_encode(array('recordsTotal'=>$recordsTotal,'query_id'=>0));
    }*/


    public function process_paging(){
        $sql=" from contact where 1 ";
        //$conditions='';
        $query_id=$this->input->post('query_id');
        if($query_id>0){
            $temp=$this->prime_model->getByID('query','id',$query_id);
            $sql=$temp['value'];
        }

        $count_sql="select count(*)as total $sql";
        $sql="select * $sql";
        $recordsTotal=$this->input->post('recordsTotal');
        if(!isset($_POST['records_total'])){  if($recordsTotal== NULL)
            $recordsTotal=$this->get_count_faster($count_sql);// get_count($count_sql)
        }
        else{
            $recordsTotal=$_POST['records_total'];
        }
        $recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied

        $draw=$this->input->post('draw');
        $search=$this->input->post('search');
        $start=$this->input->post('start');
        $length=$this->input->post('length');

        if($search['value']!=''){
            $value=$search['value'];
            $sql .=" and ( name like '%$value%' or email like '%$value%' or address like '%$value%' or telephone like '%$value%')";
            $recordsFiltered=$this->get_count($sql);
        }

        //for getting data with limit
        $sql .=" order by id asc limit $start,$length";

        $contacts=$this->prime_model->getByQuery($sql) ;
        $output=array();
        $i=$start+1;
        foreach($contacts as $item){
            //buttons
            $btn_details="<a href='". site_url('contact/view_contact_details/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-eye-open\" title=\"Details\"></a>" ;

            $btn_edit="<a href='". site_url('contact/edit/'.$item['id'])."' class=\"btn btn-warning glyphicon glyphicon-pencil\" title=\"Edit\"></a>" ;

          // $btn_call="<a href='". site_url('contact/crm_call/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-earphone\" title=\"Call\"></a>" ;

            $btn_delete="<a href='". site_url('contact/delete/'.$item['id'])."' class=\"btn btn-danger glyphicon glyphicon glyphicon-trash delete\" title=\"Delete\"></a>" ;

            //echo '<a href='. site_url().'/contact/edit/'.$row->id.' class="btn btn-warning glyphicon glyphicon-pencil" title="Edit"></a>';


            //echo '<a href='. site_url().'/contact/delete/'.$row->id.' class="btn btn-danger glyphicon glyphicon-trash delete" title="Delete Info"></a>';
            //end of buttons

            $output[]=array($i,$item['name'],$item['email'],$item['address'],$item['telephone']," $btn_details $btn_edit  $btn_delete");
            $i++;
        }
        $json_data = array(
            "draw"            => $draw,
            "recordsTotal"    => $recordsTotal ,
            "recordsFiltered" => $recordsFiltered,
            "data"            => $output   // total data array
        );
        echo json_encode($json_data);
    }

    public function process_paging_ticket(){
        $sql=" from ticket where 1 ";
        //$conditions='';
        $query_id=$this->input->post('query_id');
        if($query_id>0){
            $temp=$this->prime_model->getByID('query','id',$query_id);
            $sql=$temp['value'];
        }

        $count_sql="select count(*)as total $sql";
        $sql="select * $sql";
		//echo '<pre>';print_r($sql);die();
        $recordsTotal=$this->input->post('recordsTotal');
        if(!isset($_POST['records_total'])){  //if($recordsTotal== NULL)
            $recordsTotal=$this->get_count_faster($count_sql);
        }
        else{
            $recordsTotal=$_POST['records_total'];
        }
        $recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied

        $draw=$this->input->post('draw');
        $search=$this->input->post('search');
        $start=$this->input->post('start');
        $length=$this->input->post('length');

        if($search['value']!=''){
            $value=$search['value'];
            $sql .=" and ( name like '%$value%' or ticket_status like '%$value%' or email like '%$value%' or remarks like '%$value%' or created_by like '%$value%' or created_date like '%$value%' or modified_date like '%$value%')";
            $recordsFiltered=$this->get_count($sql);
        }

        //for getting data with limit
        $sql .=" order by created_date desc limit $start,$length";
       // file_put_contents("/var/www/html/sony/log.txt","$sql");
        $contacts=$this->prime_model->getByQuery($sql);
        $output=array();
		//echo '<pre>';print_r($output);die();
        $i=$start+1;
        foreach($contacts as $item){
            //buttons
            $btn_details="<a href='". site_url('contact/view_ticket_details/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-eye-open\" title=\"Details\"></a>" ;

            $btn_edit="<a href='". site_url('contact/edit_ticket/'.$item['id'])."' class=\"btn btn-warning glyphicon glyphicon-pencil\" title=\"Edit\"></a>" ;

           // $btn_call="<a href='". site_url('contact/crm_ticket_call/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-earphone\" title=\"Call\"></a>" ;

            $btn_delete="<a href='". site_url('contact/delete_ticket/'.$item['id'])."' class=\"btn btn-danger glyphicon glyphicon glyphicon-trash delete\" title=\"Edit\"></a>" ;
			

            //echo '<a href='. site_url().'/contact/edit/'.$row->id.' class="btn btn-warning glyphicon glyphicon-pencil" title="Edit"></a>';


            //echo '<a href='. site_url().'/contact/delete/'.$row->id.' class="btn btn-danger glyphicon glyphicon-trash delete" title="Delete Info"></a>';
            //end of buttons

            $output[]=array($i,$item['ticket_no'],$item['name'],$item['email'],$item['address'],$item['telephone'],$item['ticket_status'],$item['remarks'], $item['assigned_to'], $item['solved_by'], $item['created_date'], $item['modified_date']," $btn_details  $btn_delete $btn_edit ");
            $i++;
        }
        $json_data = array(
            "draw"            => $draw,
            "recordsTotal"    => $recordsTotal ,
            "recordsFiltered" => $recordsFiltered,
            "data"            => $output   // total data array
        );
        echo json_encode($json_data);
    }
    public function process_ctrl_paging(){
        $sql="select * from contact where 1 ";

        $conditions='';
        $query_id=$this->input->post('query_id');
        /*if($query_id>0){
            $temp=$this->prime_model->getByID('query','id',$query_id);
            $conditions=$temp['value'];
            $sql .=$conditions;
        }*/
        $recordsTotal=$this->get_count($sql);
        $recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied

        $draw=false;
        $search=$this->input->post('search');
        $start=0;
        $length=10;

        if($search['value']!=''){
            $value=$search['value'];
            $sql .=" and ( first_name like '%$value%' or last_name like '%$value%' or email like '%$value%' or mobile like '%$value%' or department like '%$value%' or lead_source like '%$value%' or issue_date like '%$value%' or expiry_date like '%$value%')";
            $recordsFiltered=$this->get_count($sql);
        }

        //for getting data with limit
        $sql .=" limit 0,10";

        $contacts=$this->prime_model->getByQuery($sql) ;
        $output=array();
        $i=$start+1;
        foreach($contacts as $item){
            $id=$item['id'];
            $name=$item['first_name']." ".$item['middle_name']." ".$item['last_name'];
            $output[]=array($i,$item['first_name'],$item['last_name'],$item['email'],"<a href='#' class='select_contact' contact_id='$id' contact_name='$name' title='select'>".$item['mobile']."</a>",$item['department']);
            $i++;
        }
        $json_data = array(
            "draw"            => $draw,
            "recordsTotal"    => $recordsTotal ,
            "recordsFiltered" => $recordsFiltered,
            "data"            => $output   // total data array
        );
        echo json_encode($json_data);
    }


    public function index(){



        $primary_number=$this->uri->segment(3);

        if($primary_number!=''){
            $data=array('search_contact'=>true,'primary_number'=>$primary_number);

            /*$extra="";
            if(sizeof($data['raw'])==0){
                $url=site_url("contact/create/$primary_number");
                $extra=" <a role='button' class='btn btn-info' href='$url'> Add to Contact</a>";
            }
            $data['search_info']="Search result of contacts against phone: $primary_number . $extra";*/
        }
        else{
            $data['raw'] = array();// $this->contact_model->get_all_contact_details();
        }

        $this->load->view('contact/contact_list',$data);
    }

    public function duplicate($id){

        $data['raw'] = $this->contact_model->get_duplicate_contact_details($id);

        $this->load->view('contact/duplicate_list',$data);

    }

    public function ticket_list(){




        $primary_number=$this->uri->segment(3);

        if($primary_number!=''){
            $data=array('search_ticket'=>true,'primary_number'=>$primary_number);

            /*$extra="";
            if(sizeof($data['raw'])==0){
                $url=site_url("contact/create/$primary_number");
                $extra=" <a role='button' class='btn btn-info' href='$url'> Add to Contact</a>";
            }
            $data['search_info']="Search result of contacts against phone: $primary_number . $extra";*/
        }
        else{
            $data['raw'] = array();// $this->contact_model->get_all_contact_details();
        }

        $this->load->view('contact/ticket_list',$data);
    }

    public function view_contact_details($id){
        //$data['ui']=array('title'=>'Edit','action'=>site_url("client_manager/update_client/$id"),'okButton'=>'Update');

        $data['contact_info']=$this->contact_model->get_contact_by_id($id);
        $this->load->view('contact/view_details',$data);


    }

    public function view_ticket_details($id){
        //$data['ui']=array('title'=>'Edit','action'=>site_url("client_manager/update_client/$id"),'okButton'=>'Update');

        $data['ticket_info']=$this->contact_model->get_ticket_by_id($id);
        $this->load->view('contact/view_ticket_details',$data);


    }

    /*public function crm_call($id){
        //$data['ui']=array('title'=>'Edit','action'=>site_url("client_manager/update_client/$id"),'okButton'=>'Update');

        $this->contact_model->crm_call($id);
        //$this->load->view('contact/view_ticket_details',$data);
        $this->load->view('contact/contact_list',$data);



    }*/

    public function crm_ticket_call($id){
        //$data['ui']=array('title'=>'Edit','action'=>site_url("client_manager/update_client/$id"),'okButton'=>'Update');

        $this->contact_model->crm_ticket_call($id);
        //$this->load->view('contact/view_ticket_details',$data);
        $this->load->view('contact/ticket_list',$data);



    }



    public function edit($id){

        $data['ui']=array('title'=>'Edit','action'=>site_url("contact/update/$id"),'okButton'=>'Update');

        $data['params']=$this->contact_model->get_contact_by_id($id);

        $this->load->view('contact/create',$data);

    }

    public function edit_ticket($id){

        $data['ui']=array('title'=>'Edit','action'=>site_url("contact/update_ticket/$id"),'okButton'=>'Update');

        $data['params']=$this->contact_model->get_ticket_by_id($id);

        $this->load->view('contact/create_ticket',$data);

    }

    public function update($id){
        $this->form_validation->set_rules('name','Name','trim|required');
        $this->form_validation->set_rules('telephone','Telephone','trim|required');


        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
        if($this->form_validation->run()){
            //$client=$this->client_manager_model->refreshObject($params);
            $return_value = $this->contact_model->save($params);

            $this->session->set_flashdata('return_value', $return_value);
            redirect('contact/edit/'.$id); //for clearing input fields [stackoverflow :) ]
        }
        else{
            $data['ui']=array('title'=>'Edit','action'=>site_url("contact/update/$id"),'okButton'=>'Update');

            $data['params']=$params;
            $this->load->view('contact/create', $data);

        }
    }

    public function update_ticket($id){
        $this->form_validation->set_rules('name','Name','trim|required');
        $this->form_validation->set_rules('telephone','Telephone','trim|required');


        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
        if($this->form_validation->run()){
            //$client=$this->client_manager_model->refreshObject($params);
            $return_value = $this->contact_model->save_ticket($params);

            $this->session->set_flashdata('return_value', $return_value);
            redirect('contact/edit_ticket/'.$id); //for clearing input fields [stackoverflow :) ]
        }
        else{
            $data['ui']=array('title'=>'Edit','action'=>site_url("contact/update_ticket/$id"),'okButton'=>'Update');

            $data['params']=$params;
            $this->load->view('contact/create_ticket', $data);

        }
    }

    function is_unique_mobile_for_edit($str)
    {
        $field_value = $str; //this is redundant, but it's to show you how
        //the content of the fields gets automatically passed to the method
        if($this->contact_model->is_mobile_already_exist('edit',$id=(int)$this->input->post('id'),$str)){
            return false;
        }
        else return true;
    }

    public function download_contact_chart(){
        $this->contact_model->download_contact_chart();
    }
    public  function download_contact_chart_ticket(){
        $this->contact_model->download_contact_chart_ticket();
    }
    public function download_cdr_chart(){
        $this->contact_model->download_cdr_chart();
    }



    public function delete($id)
    {
        $this->contact_model->delete_by_id($id);
        echo "1";
    }
	public function delete_ticket($id){
		$this->contact_model->delete_by_id_ticket_list($id);
        echo "1";
	}
	
	public function create_team(){
	$this->load->view('contact/create_team');

	}
	public function save_team(){
		
		 $data['name']=	$this->input->post('name');
	 $this->load->model('contact_model');

     $id = $this->contact_model->add_team($data);
	 $this->load->view('contact/create_team');
	}

    public function save_category(){
        $this->load->model('contact_model');

        $data['name']=	$this->input->post('name');
        $id = $this->contact_model->add_team($data);



        $data1['status']=$this->input->post('status');
        $id = $this->contact_model->ticket_status($data1);

        $data2['name']=$this->input->post('assignedname');
        $id = $this->contact_model->assigned_to($data2);

        $data3['name']=$this->input->post('solvedby');
        $id = $this->contact_model->solved_by($data3);

        $data4['type']=$this->input->post('type');
        $id = $this->contact_model->problem_type($data4);
       /* echo '<pre>';
        print_r($data);
        print_r($data1);
        print_r($data2);
        print_r($data3);
        print_r($data4);
        die();*/
        $this->load->view('contact/create_team');
    }


}
