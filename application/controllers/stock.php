<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class stock extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->model('stock_model');
		$this->load->model('prime_model');
        $this->load->model('user_model');
		$this->load->library('excel');
        $this->load->helper(array('form', 'url'));
		$this->checkAuthorization();
		
    }
	
	public function checkAuthorization()
        {
			
			
            //$current_user=$this->user_model->get_current_user();
            if(!$this->user_model->is_logged_in()){
				$this->session->set_userdata(array('redirect_after_login'=>current_url()));
                redirect('/login/index');
                return;
            }
            /*$hasPermission=$this->user_model->has_permission_for_role($this->role_manager_model->See_isd_rate_chart);
            if(!$hasPermission){
                redirect('/login/index');
                return;
            }*/
        }

    public function create(){
		$data['ui']=array('title'=>'New','action'=>site_url("stock/save"),'okButton'=>'Save');
        $this->load->view('stock/create',$data);
        
    }
    
    public function save(){
        
		//$this->form_validation->set_rules('mobile','Mobile','trim|required|callback_is_unique_mobile_for_edit');
		//$this->form_validation->set_message('is_unique_mobile_for_edit','Mobile Number must be unique.');
		
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
       
            //$client=$this->client_manager_model->refreshObject($params);
		$return_value = $this->stock_model->save_collection($params);

		$this->session->set_flashdata('return_value', $return_value);
		redirect('stock/create'); //for clearing input fields [stackoverflow :) ]
        
    }
	
	
	
	public function edit(){
		
		$id=$this->uri->segment(3);		
		$query="select * from collection where id=$id ";
		$result=$this->prime_model->getByQuery($query);
		$data['params']= $result[0];
		$data['ui']=array('title'=>'Edit','action'=>site_url("stock/update/$id"),'okButton'=>'Update');
	
		$result=$this->prime_model->getByQuery("select id from stock where table_name='collection' and table_link_id=$id order by id");
		$rows=array();
		foreach($result as $key=>$value){
			$data['stock_id']=$value['id'];
			$rows[]=$this->load->view('stock/single_row', $data,true);
        
		}
		$data['rows']=$rows;

        $this->load->view('stock/create',$data);
        
    }
    
    public function update(){
        
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
       
		$return_value = $this->stock_model->save_collection($params);

		$this->session->set_flashdata('return_value', $return_value);
		redirect('stock/edit/'.$params['id']); //for clearing input fields [stackoverflow :) ]
        
    }
	
	public function single_row_json(){
        ob_start();
		$data['stock_id']=$this->input->post('stock_id', TRUE);
        $this->load->view('stock/single_row', $data); 
        $content=ob_get_clean();
        $arr_data = array('success' => true,'content' => $content,'extra'=>$extra);
        print_r(json_encode($arr_data, JSON_HEX_QUOT | JSON_HEX_TAG)) ;
		//$string = $this->load->view('myfile', '', TRUE);
    }
	public function dropdown_json(){
        ob_start();
		$type=$this->input->post('type', TRUE);
		$data['params']=array('type'=>$type);
        $this->load->view('stock/dropdown', $data); 
        $content=ob_get_clean();
        $arr_data = array('success' => true,'content' => $content,'extra'=>$extra);
        print_r(json_encode($arr_data, JSON_HEX_QUOT | JSON_HEX_TAG)) ;
		//$string = $this->load->view('myfile', '', TRUE);
    }
	
	public function create_distribution(){		
				
		$data['ui']=array('title'=>'New','action'=>site_url("stock/save_distribution"),'okButton'=>'Save');
		$data['params']=array('type'=>'bp');
        $data['dropdown']=$this->load->view('stock/dropdown', $data,true);

		$this->load->view('stock/distribution',$data);
        
    }
	public function save_distribution(){
        
		//$this->form_validation->set_rules('mobile','Mobile','trim|required|callback_is_unique_mobile_for_edit');
		//$this->form_validation->set_message('is_unique_mobile_for_edit','Mobile Number must be unique.');
		
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
		$return_value = $this->stock_model->save_distribution($params);

		$this->session->set_flashdata('return_value', $return_value);
		if($return_value['success']==true){
			redirect('stock/edit_distribution/'.$return_value['distribution_id']); //for clearing input fields [stackoverflow :) ]
		}
		redirect('stock/create_distribution'); //for clearing input fields [stackoverflow :) ]
        
    }
	
	//public function get_dropdown
	
	public function edit_distribution(){
		
		$id=$this->uri->segment(3);
		$distribution=$this->prime_model->getByID('distribution','id',$id);
		$data['params']= $distribution;
		$data['ui']=array('title'=>'Edit','action'=>site_url("stock/update_distribution/$id"),'okButton'=>'Update');
		
		$data['dropdown']=$this->load->view('stock/dropdown', $data,true);
		
		if(($distribution['type']=='bp')||($distribution['type']=='nid')){
			$data['member_short_details']=$this->get_member_short_details($distribution);
		}
		
		
		$result=$this->prime_model->getByQuery("select id from stock where table_name='distribution' and table_link_id=$id order by id");
		$rows=array();
		foreach($result as $key=>$value){
			$data['stock_id']=$value['id'];
			$rows[]=$this->load->view('stock/single_row', $data,true);
        
		}
		$data['rows']=$rows;
        $this->load->view('stock/distribution',$data);
        
    }
	
	public function get_member_short_details($distribution){
		$by=$distribution['type'];
		if($by=='bp'){
			$value=$distribution['member_bp'];
		}
		else if($by=='nid'){
			$value=$distribution['member_nid'];
		}
		$data=array('by'=>$by,'value'=>$value);
		return $this->load->view('member/short_details',$data,true);
        
	}
    
    public function update_distribution(){
        
		//$this->form_validation->set_rules('mobile','Mobile','trim|required|callback_is_unique_mobile_for_edit');
		//$this->form_validation->set_message('is_unique_mobile_for_edit','Mobile Number must be unique.');
		
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
		$return_value = $this->stock_model->save_distribution($params);

		$this->session->set_flashdata('return_value', $return_value);
		redirect('stock/edit_distribution/'.$params['id']); //for clearing input fields [stackoverflow :) ]
        
    }

	public function paging_distribution(){
		//$sql="select * from naafco_predictive where 1 ";
		/*$sql="select stock.*,vicidial_log.call_date,vicidial_log.length_in_sec 
		from stock left join vicidial_log on stock.uniqueid=vicidial_log.uniqueid  where 1 and stock.uniqueid!='' ";*/
		$sql="select distribution.* from distribution  where 1  ";
		
		/*if(!$this->user_model->is_admin()){
			$user=$this->user_model->get_current_user();
			$name=$user['name'];
			$sql .=" and stock.user='$name' ";
		}*/
		
		$conditions='';
		$query_id=$this->input->post('query_id');
		if($query_id>0){
			$temp=$this->prime_model->getByID('query','id',$query_id);
			$conditions=$temp['value'];
			$sql .=$conditions;
		}
		$recordsTotal=$this->get_count($sql);
		$recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
		
		$draw=$this->input->post('draw');
		$search=$this->input->post('search');
		$start=$this->input->post('start');
		$length=$this->input->post('length');
		
		if($search['value']!=''){
			$value=$search['value'];
			$sql .=" and ( name like '%$value%' or code like '%$value%' or member_bp like '%$value%' or mobile like '%$value%' or present_address like '%$value%'  )";
			$recordsFiltered=$this->get_count($sql);
		}
		
		//for getting data with limit
		//$sql .=" order by id desc "; 
		$sql .=" order by id asc "; 
		$sql .=" limit $start,$length";
		
		
		$contacts=$this->prime_model->getByQuery($sql) ;
		$output=array();
		$i=$start+1;
		foreach($contacts as $item){
			$btn_print="<a target=_blank href='". site_url('stock/print_distribution/'.$item['id'])."' class=\"btn btn-info \" title=\"Print\"><i class=\"ti-printer\"></i></a>" ;
			//$btn_view="<a href='". site_url('stock/edit_distribution/'.$item['id'])."' class=\"btn btn-success \" title=\"Detail\"><i class=\"ti-zoom-in\"></i></a>" ;
			$btn_edit="<a href='". site_url('stock/edit_distribution/'.$item['id'])."' class=\"btn btn-warning \" title=\"Edit\"><i class=\"ti-pencil-alt\"></i></a>" ;
			$btn_delete="<a href='". site_url('stock/delete_distribution/'.$item['id'])."' class=\"delete btn btn-danger \" title=\"Delete\"><i class=\"ti-trash\"></i></a>" ;
			
			$output[]=array($i,$item['created_date'],$item['code'],$item['name'],$item['member_bp'],$item['mobile'],"$btn_print  $btn_edit $btn_delete");
			$i++;
		}
		$json_data = array(
					 "draw"            => $draw,   
					 "recordsTotal"    => $recordsTotal ,  
					 "recordsFiltered" => $recordsFiltered,
					 "data"            => $output   // total data array
					 );
		echo json_encode($json_data);
    }
	
	public function list_distribution(){
		
		$this->load->view('stock/list_distribution');
	}
	public function print_distribution(){
		
		$id=$this->uri->segment(3);
		$mdcn_per_page=8;//$this->uri->segment(4);
		$distribution=$this->prime_model->getByID('distribution','id',$id);
		$member=$this->prime_model->getByID('member','bp',$distribution['member_bp']);
		
		$data['params']= $distribution;
		$data['ui']=array('title'=>'Edit','action'=>site_url("stock/update_distribution/$id"),'okButton'=>'Update');
		
		
		$result=$this->prime_model->getByQuery("select medicine_name,qty,price_at_that_time as unit_price,qty*price_at_that_time as price from stock where table_name='distribution' and table_link_id=$id order by id");
		//print_r($result);
		$views=array();
		$sl=1;
		$count=0;
		$group_no=0;
		$gross_amount=0;
		foreach($result as $key=>$value){			
			
			$medicine_group[$group_no][]=array('sl'=>$sl,'medicine_name'=>$value['medicine_name'],'qty'=>$value['qty'],'unit_price'=>$value['unit_price'],'price'=>$value['price']);
			$count++;
			if($count>=$mdcn_per_page){
				$group_no++;
			}
			$sl++;
			$gross_amount +=$value['price'];
			if($key==sizeof($result)-1){
				//at last adding summary row
				$medicine_group[$group_no][]=array('sl'=>'','medicine_name'=>'<b>সর্বমোট</b>','qty'=>'','unit_price'=>'','price'=>"<b>$gross_amount</b>");
			}
			
		}
		foreach($medicine_group as $item){
			$data=array();
			$data['code']=$distribution['code'];
			$data['name']=$distribution['name'];
			$data['type']=$distribution['type'];
			$data['member_bp']=$distribution['member_bp'];
			$data['member_nid']=$distribution['member_nid'];
			$data['police_unit']=$distribution['police_unit'];
			$data['mobile']=$distribution['mobile'];
			$data['present_address']=$distribution['present_address'];
			$data['medicine_group']=$item;
			$data['signature']=$member['signature'];
			$views[]=$this->load->view('common/prescription', $data,true);	
		}
		
		$data=array();
		$data['prescription_views']=$views;
        $this->load->view('stock/print_distribution',$data);
		
	}
	
	
	function is_unique_mobile_for_add($str){
        $field_value = $str; //this is redundant, but it's to show you how
        //the content of the fields gets automatically passed to the method
        if($this->contact_model->is_mobile_already_exist('add',$id=(int)$this->input->post('id'),$str)){
            return false;
        }
        else return true;
    }
	
	/*public function search(){
		$primary_number=$this->input->post('primary_number');
		$conditions='';
		$query_id=0;
		
		$sql="select * from stock where 1 ";
		if($primary_number!=''){
			$conditions .=" and ( mobile like '%$primary_number%' or office_phone like '%$primary_number%' or home_phone like '%$primary_number%' ) ";
		}
		if(strlen($conditions)> 0){
			$sql .=$conditions;
			$query_id=$this->prime_model->insert("query",array('value'=>$conditions));
		}
		$record_count=$this->get_count($sql);
		echo json_encode(array('record_count'=>$record_count,'query_id'=>$query_id,'conditions'=>$primary_number));
    }*/
	
	public function get_count($sql){
		$query = $this->db->query($sql);
		return $query->num_rows($query);
	}
	
	public function delete_distribution(){
		$id=$this->uri->segment(3);
		echo $this->stock_model->delete_distribution($id);
		
		
	}
	
	public function paging(){
		
		$sql="select collection.* from collection  where 1  ";
		
		/*if(!$this->user_model->is_admin()){
			$user=$this->user_model->get_current_user();
			$name=$user['name'];
			$sql .=" and stock.user='$name' ";
		}*/
		
		$conditions='';
		$query_id=$this->input->post('query_id');
		if($query_id>0){
			$temp=$this->prime_model->getByID('query','id',$query_id);
			$conditions=$temp['value'];
			$sql .=$conditions;
		}
		$recordsTotal=$this->get_count($sql);
		$recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
		
		$draw=$this->input->post('draw');
		$search=$this->input->post('search');
		$start=$this->input->post('start');
		$length=$this->input->post('length');
		
		if($search['value']!=''){
			$value=$search['value'];
			$sql .=" and ( code like '%$value%' or description like '%$value%'  )";
			$recordsFiltered=$this->get_count($sql);
		}
		
		//for getting data with limit
		//$sql .=" order by id desc "; 
		$sql .=" order by id asc "; 
		$sql .=" limit $start,$length";
		
		
		$contacts=$this->prime_model->getByQuery($sql) ;
		$output=array();
		$i=$start+1;
		foreach($contacts as $item){
			$btn_edit="<a href='". site_url('stock/edit/'.$item['id'])."' class=\"btn btn-warning \" title=\"Edit\"><i class=\"ti-pencil-alt\"></i></a>" ;
			$btn_delete="<a href='". site_url('stock/delete/'.$item['id'])."' class=\"delete btn btn-danger \" title=\"Delete\"><i class=\"ti-trash\"></i></a>" ;
			
			$output[]=array($i,$item['created_date'],$item['code'],$item['description']," $btn_edit $btn_delete");
			$i++;
		}
		$json_data = array(
					 "draw"            => $draw,   
					 "recordsTotal"    => $recordsTotal ,  
					 "recordsFiltered" => $recordsFiltered,
					 "data"            => $output   // total data array
					 );
		echo json_encode($json_data);
    }
	
	
	public function index(){
		
		$this->load->view('stock/list');
	}
	
	public function delete(){
		$id=$this->uri->segment(3);
		echo $this->stock_model->delete_collection($id);
				
	}
	
	
	
	public function report(){
		$params = $this->security->xss_clean($this->input->post(NULL, TRUE));
		$data['params']=$params;
		$data['report']=$this->stock_model->get_report($params);
		$this->load->view('stock/report',$data);
	}
	public function download_report(){		
		$data['report']=$this->stock_model->download_report($from_date,$to_date);
	}
	
	public function report2(){
		$params = $this->security->xss_clean($this->input->post(NULL, TRUE));
		$data['params']=$params;
		$data['report']=$this->stock_model->get_report2($params);
		$this->load->view('stock/report2',$data);
	}
	
	public function download_report2(){		
		$data['report']=$this->stock_model->download_report2($from_date,$to_date);
	}
	public function report_search(){

		$params=$this->input->post(null);

	
		$start_date=addslashes(trim($params['start_date']));
		$end_date=addslashes(trim($params['end_date']));
		//$user=addslashes(trim($params['user']));
		//$status=addslashes(trim($params['status']));
		$phone_number=addslashes(trim($params['phone_number']));
		$file_name=addslashes(trim($params['file_name']));
        
		$created_date_from=addslashes(trim($params['created_date_from']));
		$created_date_to=addslashes(trim($params['created_date_to']));

		$conditions='';
		$query_id=0;


		
		$sql=" ";  //select *

        if($start_date!=''){
            $sql .=" and vl.call_date >='$start_date:00' ";
        }

        if($end_date!=''){
            $sql .=" and vl.call_date <='$end_date:59' ";
        }
		/*if($user!=''){
            $sql .=" and vl.user='$user' ";
        }*/
		//applied before -- $_POST['user'] will be set only in case of admin
		if(isset($_POST['user']) ){
			$str_users="";
			foreach($_POST['user'] as $item){
				$str_users .=",'$item'";	
			}
			$str_users=trim($str_users,",");
			$sql .=" and vl.user in($str_users) ";
		}
		// in case of not admin 
		/*if(!$this->user_model->is_admin()){
			$user=$this->user_model->get_current_user();
			$name=$user['name'];
			$sql .=" and vl.user in('$name') ";
		}*/
		
		if(isset($_POST['status']) ){
			$str_statuses="";
			foreach($_POST['status'] as $item){
				$str_statuses .=",'$item'";	
			}
			$str_statuses=trim($str_statuses,",");
			$sql .=" and c2c.status in($str_statuses) ";
		}
		
		/*if($status!=''){
            $sql .=" and c2c.status='$status' ";
        }*/
		if($phone_number!=''){
            $sql .=" and vl.phone_number like '%$phone_number%' ";
        }
		if($file_name!=''){
            $sql .=" and c2c.file_name like '%$file_name%' ";
        }
		
		if($created_date_from!=''){
            $sql .=" and c2c.created_date >='$created_date_from:00' ";
        }

        if($created_date_to!=''){
            $sql .=" and c2c.created_date <='$created_date_to:59' ";
        }
		
		$query_id=$this->prime_model->insert("query",array('value'=>$sql));       
		$this->session->set_userdata(array('query_id'=>$query_id));
	
		echo json_encode(array('query_id'=>$query_id,'params'=>$params)); 
    }
	
	function is_unique_mobile_for_edit($str)
    {
        $field_value = $str; //this is redundant, but it's to show you how
        //the content of the fields gets automatically passed to the method
        if($this->contact_model->is_mobile_already_exist('edit',$id=(int)$this->input->post('id'),$str)){
            return false;
        }
        else return true;
    }
	
	


}
