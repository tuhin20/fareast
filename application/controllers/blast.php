<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class blast extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->model('blast_model');
		$this->load->model('prime_model');
        $this->load->model('user_model');
		$this->load->library('excel');
        $this->load->helper(array('form', 'url'));
		$this->checkAuthorization();
		
    }
	
	public function checkAuthorization()
        {
			
			
            $current_user=$this->user_model->get_current_user();
            if(!$this->user_model->is_logged_in()){
				$this->session->set_userdata(array('redirect_after_login'=>current_url()));
                redirect('/login/index');
                return;
            }
            /*$hasPermission=$this->user_model->has_permission_for_role($this->role_manager_model->See_isd_rate_chart);
            if(!$hasPermission){
                redirect('/login/index');
                return;
            }*/
        }

    
	public function single_row_json(){
        ob_start();
		$data['blast_id']=$this->input->post('blast_id', TRUE);
        $this->load->view('blast/single_row', $data); 
        $content=ob_get_clean();
        $arr_data = array('success' => true,'content' => $content,'extra'=>$extra);
        print_r(json_encode($arr_data, JSON_HEX_QUOT | JSON_HEX_TAG)) ;
		//$string = $this->load->view('myfile', '', TRUE);
    }
	public function do_campaign_already_exist(){
		$id=$this->uri->segment(3);
		$campaign=$this->uri->segment(4);
		echo $this->blast_model->do_campaign_already_exist($id,$campaign);
	}
	
	public function create(){		
				
		$data['ui']=array('title'=>'New','action'=>site_url("blast/save"),'okButton'=>'Save');
        $this->load->view('blast/create',$data);
        
    }
	public function save(){
        
		//$this->form_validation->set_rules('mobile','Mobile','trim|required|callback_is_unique_mobile_for_edit');
		//$this->form_validation->set_message('is_unique_mobile_for_edit','Mobile Number must be unique.');
		
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
		$return_value = $this->blast_model->save($params);

		$this->session->set_flashdata('return_value', $return_value);
		redirect('blast/create'); //for clearing input fields [stackoverflow :) ]
        
    }
	
	public function edit(){
		
		$id=$this->uri->segment(3);
		$blast=$this->prime_model->getByID('voice_blast','id',$id);
		if(!$this->user_model->is_admin()){
			$current_user=$this->user_model->get_current_user();
			if($blast['created_by']!=$current_user['id']){
				redirect('blast');
			}
		}
		$data['params']= $blast;
		$data['ui']=array('title'=>'Edit','action'=>site_url("blast/update/$id"),'okButton'=>'Update');
		
        $this->load->view('blast/create',$data);
        
    }
    
    public function update(){
        
		//$this->form_validation->set_rules('mobile','Mobile','trim|required|callback_is_unique_mobile_for_edit');
		//$this->form_validation->set_message('is_unique_mobile_for_edit','Mobile Number must be unique.');
		
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
		$return_value = $this->blast_model->save($params);

		$this->session->set_flashdata('return_value', $return_value);
		redirect('blast/edit/'.$params['id']); //for clearing input fields [stackoverflow :) ]
        
    }
	
	public function details(){
		
		$id=$this->uri->segment(3);
		$blast=$this->blast_model->get_blast($id);		
		if(!$this->user_model->is_admin()){
			$current_user=$this->user_model->get_current_user();
			if($blast['created_by']!=$current_user['id']){
				redirect('blast');
			}
		}
		$data['blast']=$blast;
		//$data['ui']=array('title'=>'Edit','action'=>site_url("blast/update/$id"),'okButton'=>'Update');
		
        $this->load->view('blast/details',$data);
        
    }
	public function search(){

		$params=$this->input->post(null);

	
		$from_date=addslashes(trim($params['from_date']));
		$to_date=addslashes(trim($params['to_date']));
        

		$conditions='';
		$query_id=0;


		
		$sql=" ";  //select *

        if($from_date!=''){
            //$sql .=" and (vicidial_log.call_date >='$from_date:00') ";
            $sql .=" and (v.created_date >='$from_date:00') ";
        }

        if($to_date!=''){
            //$sql .=" and (vicidial_log.call_date <='$to_date:59') ";
            $sql .=" and (v.created_date <='$to_date:59') ";
        }
		
		if(isset($params['client_id'])&& $params['client_id']!=''){
            //$sql .=" and (vicidial_log.call_date <='$to_date:59') ";
            $sql .=" and v.created_by ='$params[client_id]' ";
        }
		
		$query_id=$this->prime_model->insert("query",array('value'=>$sql));       
		$this->session->set_userdata(array('query_id'=>$query_id));
		
		echo json_encode(array('query_id'=>$query_id)); 
    }
	public function paging(){
		$sql="select v.*,c.name as client_name  from voice_blast v left join client c on v.created_by=c.id  where 1  ";
		
		if(!$this->user_model->is_admin()){
			$current_user=$this->user_model->get_current_user();
			$sql .=" and v.created_by=$current_user[id] ";
		}
		
		$conditions='';
		$query_id=$this->input->post('query_id');
		if($query_id>0){
			$temp=$this->prime_model->getByID('query','id',$query_id);
			$conditions=$temp['value'];
			$sql .=$conditions;
		}
		$recordsTotal=$this->get_count($sql);
		$recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
		
		$draw=$this->input->post('draw');
		$search=$this->input->post('search');
		$start=$this->input->post('start');
		$length=$this->input->post('length');
		
		if($search['value']!=''){
			$value=$search['value'];
			$sql .=" and ( v.callerid like '%$value%' or v.`voice_file` like '%$value%'  or v.`code` like '%$value%' or v.`status` like '%$value%' or c.`name` like '%$value%' )";
			$recordsFiltered=$this->get_count($sql);
		}
		
		//for getting data with limit
		//$sql .=" order by id desc "; 
		$sql .=" order by v.created_date desc "; 
		$sql .=" limit $start,$length";
		
		
		$contacts=$this->prime_model->getByQuery($sql) ;
		$output=array();
		$i=$start+1;
		foreach($contacts as $item){
			$btn_details="<a target='_blank' href='". site_url('blast/details/'.$item['id'])."' class=\"btn btn-primary \" title=\"Details\"><i class=\"ti-zoom-in\"></i></a>" ;
			
			$btn_approve="";
			$show_approve_button=false;
			
			if($this->user_model->is_admin()&& $item['status']!='Approved'){
				$show_approve_button=true;
			}
			else if(!$this->user_model->is_admin()&& $item['status']!='Approved'&&$current_user['can_approve_personal_voice_blast']=='Yes'){
				$show_approve_button=true;				
			}
			
			if($show_approve_button){
				$btn_approve="<a href='". site_url('blast/approve/'.$item['id'])."' class=\"approve btn btn-success \" title=\"Approve\"><i class=\"ti-check-box\"></i></a>" ;
			}
			
			$btn_demo="";
			$btn_edit="";
			$btn_delete="";
			if($item['status']!='Approved'){
				$btn_demo="<a href='". site_url('blast/demo/'.$item['id'])."' class=\"demo btn btn-info \" title=\"Demo\"><i class=\"ti-game\"></i></a>" ;
				$btn_edit="<a href='". site_url('blast/edit/'.$item['id'])."' class=\"btn btn-warning \" title=\"Edit\"><i class=\"ti-pencil-alt\"></i></a>" ;
				$btn_delete="<a href='". site_url('blast/delete/'.$item['id'])."' class=\"delete btn btn-danger \" title=\"Delete\"><i class=\"ti-trash\"></i></a>" ;
			}

			$temp=array($i,$item['created_date'],$item['campaign'],$item['callerid'],$item['is_scheduled'],$item['max_channel'],$item['retry'],$item['status']," $btn_details $btn_approve $btn_demo $btn_edit $btn_delete");
			if($this->user_model->is_admin()){
				$client_arr = array( $item['client_name'] ); // not necessarily an array, see manual quote
				array_splice( $temp, 4, 0, $client_arr ); // splice in at position 4
			}
			
			$output[]=$temp;
			$i++;
		}
		$json_data = array(
					 "draw"            => $draw,   
					 "recordsTotal"    => $recordsTotal ,  
					 "recordsFiltered" => $recordsFiltered,
					 "data"            => $output   // total data array
					 );
		echo json_encode($json_data);
    }
	
	public function index(){
		
		$this->load->view('blast/list');
	}
	public function dashboard(){
		$this->load->library('calendar');
		$this->load->view('blast/dashboard');
	}
	
	public function approve()
    {
		$id=$this->uri->segment(3);
		if($this->user_model->is_admin()){
			$result= $this->blast_model->approve($id);
			print_r(json_encode($result, JSON_HEX_QUOT | JSON_HEX_TAG)) ;
		}
		else{
			$current_user=$this->user_model->get_current_user();
			$blast=$this->prime_model->getByID('voice_blast','id',$id);
			if(($blast['created_by']==$current_user['id'])&&$current_user['can_approve_personal_voice_blast']=='Yes'){
				$result= $this->blast_model->approve($id);
				print_r(json_encode($result, JSON_HEX_QUOT | JSON_HEX_TAG)) ;
			}
		}
    }
	
	public function demo(){
		$id=$this->uri->segment(3);
		$dst=$this->input->post('dst', TRUE);
		//if($this->user_model->is_admin()){
			$result= $this->blast_model->demo($id,$dst);
			print_r(json_encode($result, JSON_HEX_QUOT | JSON_HEX_TAG)) ;
		//}
    }
	
	public function delete(){
		$id=$this->uri->segment(3);
        echo $this->blast_model->delete_by_id($id);
        
    }
	
	
	function is_unique_mobile_for_add($str){
        $field_value = $str; //this is redundant, but it's to show you how
        //the content of the fields gets automatically passed to the method
        if($this->contact_model->is_mobile_already_exist('add',$id=(int)$this->input->post('id'),$str)){
            return false;
        }
        else return true;
    }
	
	
	public function get_count($sql){
		$query = $this->db->query($sql);
		return $query->num_rows($query);
	}
	
	
	
	public function download_report(){
		$this->blast_model->download_report();
    }
	public function download_summary(){
		$output_id=$this->uri->segment(3);
		$this->blast_model->download_summary($output_id);
    }
	public function download_c2c(){
		$this->blast_model->download_c2c();
    }
	
	
	public function report_search(){

		$params=$this->input->post(null);

	
		$from_date=addslashes(trim($params['from_date']));
		$to_date=addslashes(trim($params['to_date']));
        

		$conditions='';
		$query_id=0;


		
		$sql=" ";  //select *

        if($from_date!=''){
            //$sql .=" and (vicidial_log.call_date >='$from_date:00') ";
            $sql .=" and (b.created_date >='$from_date:00') ";
        }

        if($to_date!=''){
            //$sql .=" and (vicidial_log.call_date <='$to_date:59') ";
            $sql .=" and (b.created_date <='$to_date:59') ";
        }
		
		if($params['campaign']!=''){
            //$sql .=" and (vicidial_log.call_date <='$to_date:59') ";
            $sql .=" and v.campaign ='$params[campaign]' ";
        }
		
		$query_id=$this->prime_model->insert("query",array('value'=>$sql));       
		$this->session->set_userdata(array('report_query_id'=>$query_id));
		
		echo json_encode(array('query_id'=>$query_id)); 
    }
	
	function is_unique_mobile_for_edit($str)
    {
        $field_value = $str; //this is redundant, but it's to show you how
        //the content of the fields gets automatically passed to the method
        if($this->contact_model->is_mobile_already_exist('edit',$id=(int)$this->input->post('id'),$str)){
            return false;
        }
        else return true;
    }
	public function report_paging(){
		$sql="select v.campaign,b.tried_at,b.callerid,b.dst,b.call_status,c.duration,c.billsec , 
			CASE WHEN b.call_status ='Call_ended' THEN 'Success' ELSE b.call_status END as report_status
			from blast_log b left join cdr c on b.uniqueid=c.uniqueid inner join voice_blast v on b.blast_id=v.id   where 1  "; //where v.code <>""
		
		if(!$this->user_model->is_admin()){
			$current_user=$this->user_model->get_current_user();
			$sql .=" and v.created_by=$current_user[id] ";
		}
		
		$conditions='';
		$query_id=$this->input->post('query_id');
		if($query_id>0){
			$temp=$this->prime_model->getByID('query','id',$query_id);
			$conditions=$temp['value'];
			$sql .=$conditions;
		}
		$recordsTotal=$this->get_count($sql);
		$recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
		
		$draw=$this->input->post('draw');
		$search=$this->input->post('search');
		$start=$this->input->post('start');
		$length=$this->input->post('length');
		
		if($search['value']!=''){
			$value=$search['value'];
			$sql .=" and ( callerid like '%$value%' or `voice_file` like '%$value%'   )";
			$recordsFiltered=$this->get_count($sql);
		}
		
		//for getting data with limit
		//$sql .=" order by id desc "; 
		$sql .=" order by b.tried_at desc "; 
		$sql .=" limit $start,$length";
		
		
		$contacts=$this->prime_model->getByQuery($sql) ;
		$output=array();
		$i=$start+1;
		foreach($contacts as $item){
			
			$output[]=array($i,$item['campaign'],$item['tried_at'],$item['callerid'],$item['dst'],$item['report_status'],$item['duration'],$item['billsec']);
			$i++;
		}
		$json_data = array(
					 "draw"            => $draw,   
					 "recordsTotal"    => $recordsTotal ,  
					 "recordsFiltered" => $recordsFiltered,
					 "data"            => $output   // total data array
					 );
		echo json_encode($json_data);
    }
	
	public function report(){
		
		$this->load->view('blast/report');
	}
	public function summary(){
		$params=$this->input->post(null);
		$data['params']=$params;
		if(isset($params['generate_report'])){
			
			$output=$this->blast_model->get_summary($params);
			$output_id=$this->prime_model->insert("query",array('value'=>json_encode($output)));    
			$data['report']=$output;
			$data['output_id']=$output_id;
								
		}		
		$this->load->view('blast/summary',$data);
	}
	
	
	
	
	
	
	

}
