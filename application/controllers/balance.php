<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class balance extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->model('balance_model');
		$this->load->model('prime_model');
        $this->load->model('user_model');
		$this->load->library('excel');
        $this->load->helper(array('form', 'url'));
		$this->checkAuthorization();
		
    }
	
	public function checkAuthorization()
        {
			
			
            $current_user=$this->user_model->get_current_user();
            if(!$this->user_model->is_logged_in()){
				$this->session->set_userdata(array('redirect_after_login'=>current_url()));
                redirect('/login/index');
                return;
            }
			
			//This module only for admin
			
			if(!$this->user_model->is_admin()){
				redirect('/login/index');
			}
            /*$hasPermission=$this->user_model->has_permission_for_role($this->role_manager_model->See_isd_rate_chart);
            if(!$hasPermission){
                redirect('/login/index');
                return;
            }*/
        }

    
	
	
	public function create(){		
				
		$data['ui']=array('title'=>'New','action'=>site_url("balance/save"),'okButton'=>'Save');
        $this->load->view('balance/create',$data);
        
    }
	public function save(){
        
		//$this->form_validation->set_rules('mobile','Mobile','trim|required|callback_is_unique_mobile_for_edit');
		//$this->form_validation->set_message('is_unique_mobile_for_edit','Mobile Number must be unique.');
		
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
		$return_value = $this->balance_model->save($params);

		$this->session->set_flashdata('return_value', $return_value);
		redirect('balance/create'); //for clearing input fields [stackoverflow :) ]
        
    }
	
	public function edit(){
		
		$id=$this->uri->segment(3);
		$balance=$this->prime_model->getByID('voice_balance','id',$id);
		if(!$this->user_model->is_admin()){
			$current_user=$this->user_model->get_current_user();
			if($balance['created_by']!=$current_user['id']){
				redirect('balance');
			}
		}
		$data['params']= $balance;
		$data['ui']=array('title'=>'Edit','action'=>site_url("balance/update/$id"),'okButton'=>'Update');
		
        $this->load->view('balance/create',$data);
        
    }
    
    public function update(){
        
		//$this->form_validation->set_rules('mobile','Mobile','trim|required|callback_is_unique_mobile_for_edit');
		//$this->form_validation->set_message('is_unique_mobile_for_edit','Mobile Number must be unique.');
		
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
		$return_value = $this->balance_model->save($params);

		$this->session->set_flashdata('return_value', $return_value);
		redirect('balance/edit/'.$params['id']); //for clearing input fields [stackoverflow :) ]
        
    }
	
	public function details(){
		
		$id=$this->uri->segment(3);
		$balance=$this->balance_model->get_balance($id);		
		if(!$this->user_model->is_admin()){
			$current_user=$this->user_model->get_current_user();
			if($balance['created_by']!=$current_user['id']){
				redirect('balance');
			}
		}
		$data['balance']=$balance;
		//$data['ui']=array('title'=>'Edit','action'=>site_url("balance/update/$id"),'okButton'=>'Update');
		
        $this->load->view('balance/details',$data);
        
    }
	public function search(){

		$params=$this->input->post(null);

	
		$from_date=addslashes(trim($params['from_date']));
		$to_date=addslashes(trim($params['to_date']));
        

		$conditions='';
		$query_id=0;


		
		$sql=" ";  //select *

        if($from_date!=''){
            //$sql .=" and (vicidial_log.call_date >='$from_date:00') ";
            $sql .=" and (v.created_date >='$from_date:00') ";
        }

        if($to_date!=''){
            //$sql .=" and (vicidial_log.call_date <='$to_date:59') ";
            $sql .=" and (v.created_date <='$to_date:59') ";
        }
		
		if(isset($params['client_id'])&& $params['client_id']!=''){
            //$sql .=" and (vicidial_log.call_date <='$to_date:59') ";
            $sql .=" and v.created_by ='$params[client_id]' ";
        }
		
		$query_id=$this->prime_model->insert("query",array('value'=>$sql));       
		$this->session->set_userdata(array('query_id'=>$query_id));
		
		echo json_encode(array('query_id'=>$query_id)); 
    }
	public function paging(){
		$sql="select balance.*, client.name as client_name from balance left join client on balance.client_id=client.id  where 1  ";
		
		
		$conditions='';
		$query_id=$this->input->post('query_id');
		if($query_id>0){
			$temp=$this->prime_model->getByID('query','id',$query_id);
			$conditions=$temp['value'];
			$sql .=$conditions;
		}
		$recordsTotal=$this->get_count($sql);
		$recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
		
		$draw=$this->input->post('draw');
		$search=$this->input->post('search');
		$start=$this->input->post('start');
		$length=$this->input->post('length');
		
		if($search['value']!=''){
			$value=$search['value'];
			$sql .=" and ( balance.amount like '%$value%' or client.name like '%$value%'   )";
			$recordsFiltered=$this->get_count($sql);
		}
		
		//for getting data with limit
		//$sql .=" order by id desc "; 
		$sql .=" order by client.name "; 
		$sql .=" limit $start,$length";
		
		
		$contacts=$this->prime_model->getByQuery($sql) ;
		$output=array();
		$i=$start+1;
		foreach($contacts as $item){
			
			$output[]=array($i,$item['client_name'],$item['amount']);
			$i++;
		}
		$json_data = array(
					 "draw"            => $draw,   
					 "recordsTotal"    => $recordsTotal ,  
					 "recordsFiltered" => $recordsFiltered,
					 "data"            => $output   // total data array
					 );
		echo json_encode($json_data);
    }
	
	public function index(){
		
		$this->load->view('balance/list');
	}
	
	
	public function get_count($sql){
		$query = $this->db->query($sql);
		return $query->num_rows($query);
	}
	
	
	
	
	
	public function report_search(){

		$params=$this->input->post(null);

	
		$from_date=addslashes(trim($params['from_date']));
		$to_date=addslashes(trim($params['to_date']));
        

		$conditions='';
		$query_id=0;


		
		$sql=" ";  //select *

        if($from_date!=''){
            //$sql .=" and (vicidial_log.call_date >='$from_date:00') ";
            $sql .=" and (b.created_date >='$from_date:00') ";
        }

        if($to_date!=''){
            //$sql .=" and (vicidial_log.call_date <='$to_date:59') ";
            $sql .=" and (b.created_date <='$to_date:59') ";
        }
		
		if($params['campaign']!=''){
            //$sql .=" and (vicidial_log.call_date <='$to_date:59') ";
            $sql .=" and v.campaign ='$params[campaign]' ";
        }
		
		$query_id=$this->prime_model->insert("query",array('value'=>$sql));       
		$this->session->set_userdata(array('report_query_id'=>$query_id));
		
		echo json_encode(array('query_id'=>$query_id)); 
    }
	
	
	

}
