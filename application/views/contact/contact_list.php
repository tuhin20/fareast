<?php $this->load->view('common/header.php');

?>
<style>
		.bordered{
			border: 1px solid #dddddd;
		}
		.font-size20{
			font-size:20px;
		}
	</style>
<header class="page-header" >
	
	
		<div class="right-wrapper pull-right">
			
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	
		<div class='row'>
		<div class="col-md-12">
			<section class="panel" >
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>
			<h2 class="panel-title">View Contact List</h2>
				</header>
				<div class="panel-body ">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-primary" >
								  <div class="">
            <!--<input type="checkbox" class="selectAll" /> <span role='button' data-toggle='tooltip' data-placement='top' title='Delete selected' class='glyphicon glyphicon-trash linksDeleteSelected ' controllerMethod="<?php echo site_url('group_manager/deleteSelected') ?>" controllerReloadMethod="<?php echo site_url('group_manager/group_list') ?>" confirmationMsg='are you sure to delete selected group(s)' aria-hidden='true' onclick=''></span>-->

            <table class="table table-bordered table-hover table-striped" id="my_datatable">
                <thead>
                <tr>
                    <!--<th style="width:10%;"></th>-->
                    <th>Sl</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Telephone</th>
                    <th class="col-md-2">Action</th>
                    </tr>
                </thead>
                <tbody>
                 </tbody>
            </table>
        </div>
							</div>
						</div>
					</div>
			</div>
				
			</section>
						
		
		</div>
	</div>
	

   <div class='row'>
		<div class="col-md-12">
			<section class="panel" >
				<div class="row">
               <div class="col-lg-12">
        <?php if(isset($search_info)) echo "<h3>$search_info</h3>"?>
     <!--   <div > <h3 id="search_info"></h3>
          <br>
         </div>-->
      
     </div>
   </div>
			</section>
		</div>
	</div>




<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header" style="background:#cccccc" >Contact Details /
            <small>List</small>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-sm-3 col-md-3 col-lg-3">
        <?php
        //if(isset($it_user)&&$it_user==true){
        ?>

        <a STYLE="max-width:160px; max-height:70px;" id="downloadCSV" class="btn btn-success glyphicon glyphicon-download-alt" href="<?php echo site_url('/contact/download_contact_chart'); ?>"> Download CSV</a>
        <?php
        // }
        ?>
    </div>
</div>

<!--<script src='<?php echo base_url("assets/js/$paging_js_name?v3");?>'></script>-->
<?php $this->load->view('common/footer.php'); ?>

<script type="text/javascript">
    <?php
    if(isset($search_contact)&&$search_contact==true){
    ?>
    $(document).ready(function() {
        var primary_number="<?php echo $primary_number;?>";
        $.ajax({
            type: "POST"
            ,url: "<?php echo site_url('contact/search');?>"
            ,data:{primary_number:'<?php echo $primary_number;?>'}
            ,async: false
            ,success: function(response) { // on success..
                var obj=JSON.parse(response);
                if(obj.record_count==0){
                    $('#search_info').html("Number "+primary_number+" is not in your contact list. <a role='button' class='btn btn-info' href='<?php echo site_url("contact/create/$primary_number");?>'> Add to Contact</a>");
                }
                else {
                    $('#search_info').html("Search result of primary number: "+primary_number);
                }
                var query_id=obj.query_id;
                //console.log(response);
                $('#my_datatable').dataTable({
                    destroy: true, //use this to reinitiate the table, other wise problem will occur
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "<?php echo site_url('contact/process_paging');?>"
                        ,type: 'POST'
                        ,data:{query_id: query_id}
                    }
                });
            }
        });
    } );

    <?php
    }
    else{
    ?>
    $(document).ready(function() {
        $('#my_datatable').dataTable({
            destroy: true, //use this to reinitiate the table, other wise problem will occur
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?php echo site_url('contact/process_paging');?>"
                ,type: 'POST'
                ,data:{query_id: '0'}
            }
        });
    } );

    <?php
    }
    ?>
    $( "form" ).submit(function( e ) {
        e.preventDefault();
        e.stopImmediatePropagation();
        //alert('form submitted');
        $.ajax({ // create an AJAX call...
            data: $(this).serialize(), // get the form data
            type: $(this).attr('method'), // GET or POST
            url: $(this).attr('action'), // the file to call
            success: function(response) { // on success..
                var obj=JSON.parse(response);
                var query_id=obj.query_id;
                console.log(response);
                $('#my_datatable').dataTable({
                    destroy: true, //use this to reinitiate the table, other wise problem will occur
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "<?php echo site_url('contact/process_paging');?>"
                        ,type: 'POST'
                        ,data:{query_id: query_id}
                    }
                });

            }
        });
        return false;
    });
    jQuery(document.body).on('click', '.delete', function (e) {
        var this_holder = this;
        e.preventDefault();
        var delete_url= $(this).attr('href');


        bootbox.confirm("Are you sure you want to delete this entry?", function (response) {
            if (response) {
                $.ajax({
                    url: delete_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
                            $(this_holder).closest('td').closest('tr').hide(1000);
                        } else {

                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });

            }
        });
    });


    $(document).on('click', '.linksDeleteSelected', deleteSelected);
    $(document).on('change', '.selectAll', function () {
        if ($(this).is(":checked")) {
            $('.resultRow').prop('checked', true);
        }
        else
            $('.resultRow').prop('checked', false);
    });

    function deleteSelected(e) {
        if ($('.resultRow:checked').size() < 1) {
            alert('please select at least one record');
            return false;
        }
        var r = confirm(e.target.getAttribute("confirmationMsg"));
        if (r == false)
            return false;
        var controllerMethod = e.target.getAttribute("controllerMethod");
        var url = controllerMethod;
        /*var obj = {
            "flammable": "inflammable",
            "duh": "no duh"
        };
        $.each( obj, function( key, value ) {
            alert( key + ": " + value );
        });*/

        var selectedIDs = [];
        $('.resultRow:checked').each(function () {
            selectedIDs.push($(this).val());
        });
        console.log(selectedIDs);
        $.post(
            url,
            {selectedIDs: selectedIDs.join(", ")},
            function (data) {
                /*var obj = $.parseJSON(data);
                if(obj.success){
                    showStatusModal('Success','modal-info',obj.content);
                    reloadResult(e.target.getAttribute("controllerReloadMethod"));
                }
                else showStatusModal('Error','modal-warning',obj.content);*/
                window.location.replace(e.target.getAttribute('controllerReloadMethod'));
            }
        );
        return false;
    }

</script>