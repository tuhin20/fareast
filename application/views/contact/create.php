<?php $this->load->view('common/navbar.php');
//print_r($params);
?>
<div>
<div class=" col-md-12"> 	<!-- this is alert size-->
	<?php 
		$return_value=$this->session->flashdata('return_value');
		if(isset($return_value)) 
		{
			//print_r($return_value);
			$msg=$return_value['msg'];
			if($return_value['success'] == true)
			{

				echo "<div class='alert alert-success' role='alert'>$msg</div>";

			}
			else if($msg!='') //if not success and msg not empty
			{
				echo "<div class='alert alert-danger' role='alert'>$msg</div>";
			}
		}
	?>
</div>
<div class="row">
    <div class="col-lg-12" ><br/><br/>
        <h1 class="page-header" align="center"> Contact / <small>Create<?php //echo $ui['title']?> </small> </h1>
    </div>
</div>

<div class='row'>
		<div class="col-md-12">
			<section class="panel" >
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>
			<h2 class="panel-title" align="center">Input Information</h2>
				</header>
				<div class="panel-body ">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
  
	<form class="form-horizontal"  method="post" action="<?php echo $ui['action']; ?>" accept-charset="utf-8">
	<div class="col-md-6">
		<input type="hidden" name="id" value="<?php echo isset ($params['id'])? $params['id']:-1;  ?>">
  
		<!--  <h4 class="text-center">Information About the Contact</h4>-->
		  
		  
		<div class="form-group">
				<label class="control-label col-sm-5">Name</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="name" placeholder="Enter Name" name="name" value="<?php echo isset($params['name'])?$params['name']:'';?>">
				<?php echo form_error('name','<label class="error">','</label>');?>
			</div>
		</div>
		<!--<div class="form-group">
				<label class="control-label col-sm-5">Designation</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="designation" placeholder="Enter Designation" name="designation" value="<?php /*echo isset($params['designation'])?$params['designation']:'';*/?>">
			</div>
		</div>-->
		<div class="form-group">
				<label class="control-label col-sm-5">Email</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="email" placeholder="Enter Your Email" name="email" value="<?php echo isset($params['email'])?$params['email']:'';?>">
			</div>
		</div>
		
	
	<div class="form-group">
				<label class="control-label col-sm-5">Telephone</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="telephone" placeholder="Enter Telephone" name="telephone" value="<?php echo isset($params['telephone'])?$params['telephone']:'';?>">
				<?php echo form_error('telephone','<label class="error">','</label>');?>
			</div>
		</div>
        <div class="form-group">
            <label class="control-label col-sm-5">Address</label>
            <div class="col-sm-7">
                <input type="text" autocomplete="off" class="form-control input-sm" id="address" placeholder="Enter Address" name="address" value="<?php echo isset($params['address'])?$params['address']:'';?>">
                <?php echo form_error('address','<label class="error">','</label>');?>
            </div>
        </div>
		
	
	
	<div class="form-group">        
      <div class="col-sm-offset-10 col-sm-10" style="margin-left: 86.333333%">
        <button type="submit" class="btn btn-default btn-success"><?php echo $ui['okButton'];?></button>
      </div>
	  </div>
	</div>
	 </form>
  
   </div>
		</div>
				</div>
				<br><br><br><br><br><br>
				<br><br><br><br><br><br>
				<br><br><br><br><br><br>
				
				</div>
				
			</section>
						
		
		</div>
	</div>
</div>

  <!-- end of menu2 tab -->
    
      


             