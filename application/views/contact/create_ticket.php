<?php $this->load->view('common/navbar.php');
//print_r($params);
?>

<div class=" col-md-12"> 	<!-- this is alert size-->
	<?php 
		$return_value=$this->session->flashdata('return_value');
		if(isset($return_value)) 
		{
			//print_r($return_value);
			$msg=$return_value['msg'];
			if($return_value['success'] == true)
			{

				echo "<div class='alert alert-success' role='alert'>$msg</div>";

			}
			else if($msg!='') //if not success and msg not empty
			{
				echo "<div class='alert alert-danger' role='alert'>$msg</div>";
			}
		}
	?>
</div>
<div class="row">
    <div class="col-lg-12"><br/><br/>
        <h1 class="page-header" align="center"> Ticket / <small>Create<?php //echo $ui['title']?> </small> </h1>
    </div>
</div>

<div class='row' style="margin-right: 50px;">
	<div class="col-md-12">
		<section class="panel" >
			<div class="panel-body ">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
    <form class="form-horizontal" method="post" action="<?php echo $ui['action']; ?>" accept-charset="utf-8">
	<table class="text-center">
	<div class="col-md-6" >
		<input type="hidden" name="id" value="<?php echo isset ($params['id'])? $params['id']:-1;  ?>">
		<input type="hidden" name="contact_id" value="<?php echo isset ($params['contact_id'])? $params['contact_id']:-1;  ?>">
         <!--  <h4 class="text-center">Information About the Contact</h4>-->
		<div class="form-group">
				<label class="control-label col-sm-5" >Name</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="name" placeholder="Enter Name" name="name" value="<?php echo isset($params['name'])?$params['name']:'';?>">
				<?php echo form_error('name','<label class="error">','</label>');?>
			</div>
		</div>
		<div class="form-group">
				<label class="control-label col-sm-5">Email</label>
			<div class="col-sm-7">
				<input type="email" autocomplete="off" class="form-control input-sm" id="email" placeholder="Enter Email" name="email" value="<?php echo isset($params['email'])?$params['email']:'';?>">
			</div>
		</div>
        <div class="form-group">
            <label class="control-label col-sm-5">Address</label>
            <div class="col-sm-7">
                <input type="text" autocomplete="off" class="form-control input-sm" id="address" placeholder="Enter Address" name="address" value="<?php echo isset($params['address'])?$params['address']:'';?>">
            </div>
        </div>



		<div class="form-group">
				<label class="control-label col-sm-5">Telephone</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="telephone" placeholder="Enter Telephone" name="telephone" value="<?php echo isset($params['telephone'])?$params['telephone']:'';?>">
				<?php echo form_error('telephone','<label class="error">','</label>');?>
			</div>
		</div>
        <div class="form-group">
            <label class="control-label col-sm-5">Ticket Status</label>
            <div class="col-sm-7">
                <?php
                $collection=$this->prime_model->getByQuery("select * from ticket_status order by status");
                ?>
                <select  class="form-control input-sm"  name="ticket_status" id="ticket_status" >
                    <option value='' >Select</option>
                    <?php
                    foreach($collection as $item){
                        $selected='';
                        if(isset($params['ticket_status']) && (trim($item['status'])==trim($params['ticket_status'])))
                            $selected='selected';
                        $text=$item['status'];//str_replace('_',' ',$item['name']);
                        echo "<option value='$text' $selected>$text</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
		<div class="form-group">
				<label class="control-label col-sm-5" >Team</label>
			<div class="col-sm-7">
			<?php
							$collection=$this->prime_model->getByQuery("select * from team order by name");
						?>
						<select  class="form-control input-sm"  name="team" id="team" >
							<option value='' >Select</option>
							<?php	
							foreach($collection as $item){
								$selected='';
								if(isset($params['team']) && (trim($item['name'])==trim($params['team'])))
									$selected='selected';
								$text=$item['name'];//str_replace('_',' ',$item['name']);
								echo "<option value='$text' $selected>$text</option>";
							}
							?>
						</select>	
			</div>
		</div>
		
		
	
	
	
	</div>
	</table>
	<div class="col-md-6">
	
	<table>
	<tr>
		<div class="form-group" >
				<label class="control-label col-sm-5">Assigned To</label>
			<div class="col-sm-7">
			<?php
							$collection=$this->prime_model->getByQuery("select * from assigned_to order by name");
						?>
						<select  class="form-control input-sm"  name="assigned_to" id="assigned_to" >
							<option value='' >Select</option>
							<?php	
							foreach($collection as $item){
								$selected='';
								if(isset($params['assigned_to']) && (trim($item['name'])==trim($params['assigned_to'])))
									$selected='selected';
								$text=$item['name'];//str_replace('_',' ',$item['name']);
								echo "<option value='$text' $selected>$text</option>";
							}
							?>
						</select>	
			</div>
		</div>
	</tr>
	<tr>
		<div class="form-group">
				<label class="control-label col-sm-5">Solved By</label>
			<div class="col-sm-7">
			<?php
							$collection=$this->prime_model->getByQuery("select * from assigned_to order by name");
						?>
						<select  class="form-control input-sm"  name="solved_by" id="solved_by" >
							<option value='' >Select</option>
							<?php	
							foreach($collection as $item){
								$selected='';
								if(isset($params['solved_by']) && (trim($item['name'])==trim($params['solved_by'])))
									$selected='selected';
								$text=$item['name'];//str_replace('_',' ',$item['name']);
								echo "<option value='$text' $selected>$text</option>";
							}
							?>
						</select>	
			</div>
		</div>
		</tr>
      <tr>
		<div class="form-group">
				<label class="control-label col-sm-5">Problem Type</label>
			<div class="col-sm-7">
			<?php
							$collection=$this->prime_model->getByQuery("select * from problem_type order by type");
						?>
						<select  class="form-control input-sm"  name="problem_type" id="problem_type" >
							<option value='' >Select</option>
							<?php	
							foreach($collection as $item){
								$selected='';
								if(isset($params['problem_type']) && (trim($item['type'])==trim($params['problem_type'])))
									$selected='selected';
								$text=$item['type'];//str_replace('_',' ',$item['name']);
								echo "<option value='$text' $selected>$text</option>";
							}
							?>
						</select>	
			</div>
		</div>
		</tr>

	<tr>
	<div class="form-group">
				<label class="control-label col-sm-5">Remarks</label>
			<div class="col-sm-7">
				<textarea class="form-control input-sm" type="text" name="remarks" title="Please Insert Remarks"> <?php echo isset($params['remarks'])?$params['remarks']:''; ?></textarea>
			</div>
		</div>
	</tr>
	<tr>
	<div class="form-group">        
      <div class="col-sm-offset-10 col-sm-10">
        <button type="submit" class="btn btn-default btn-success"><?php echo $ui['okButton'];?></button>
      </div>
	  </div>
	</tr>
 </form>
  </div>
  </table>
  
</div>
						</div>
					</div>
				<br><br><br><br><br><br>
				<br><br><br><br><br><br>
				<br><br><br><br><br><br>
				
				</div>
				
			</section>
						
		
		</div>
	</div>


  <!-- end of menu2 tab -->
    
      


                <?php //$this->load->view('common/footer.php'); ?>
                    