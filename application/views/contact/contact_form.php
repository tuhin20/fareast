<?php $this->load->view('common/header.php'); 
$this->load->view('common/navbar.php');


?>
Contact/<small>Create</small>
<hr>
<div class="row">
  <div class="col-md-offset-2 col-md-6">
	<form class="form-horizontal" method="post" action="<?php echo $ui['action']; ?>" accept-charset="utf-8">
  <input type="hidden" name="id" value="<?php echo isset ($params['id'])? $params['id']:-1;  ?>">
  
      <h4 class="text-center">অভিযোগকারীর  বিবরন(Information About the Complainant)</h4>
	  
      
  
		<div class="form-group">
		  <label class="control-label col-sm-5">অভিযোগকারী (Complainant)</label>
		  <div class="col-sm-7">
			<select class="form-control input-sm" id="complainants_type" name="complainants_type" value="<?php echo isset($params['complainants_type'])?$params['complainants_type']:'';?>">
			<option value="individual" selected>Individual</option>
			  <option value="acting_victim">Acting on Behalf of Victim</option>
			  </select>
		  </div>
		</div>
    <div class="form-group">
      <label class="control-label col-sm-5">নাম (Name)</label>
      <div class="col-sm-7">
        <input type="text" class="form-control input-sm" id="complainants_name" placeholder="Enter Name" name="complainants_name" value="<?php echo isset($params['complainants_name'])?$params['complainants_name']:'';?>">
      </div>
    </div>
	
	
	
    
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default btn-success"><?php echo $ui['okButton'];?></button>
      </div>
	</div>
  </form>
  </div>
  
  
</div>  <!-- end of menu2 tab -->
    
      


                <?php $this->load->view('common/footer.php'); ?>
                     <script type="text/javascript">
					 $(function () {
						$('#fileupload').fileupload({
							dataType: 'json',
							done: function (e, data) {
								$.each(data.result.files, function (index, file) {
									console.log(file.name);
									$('<p/>').append(file.name).appendTo($('#file_names'));
									$('#file_names').append("<input type='hidden' name='victim_file_uploads[]' value='"+file.name+"'>");
									//$('#file_names').append(file.name).appendTo(document.body);
								});
							}
						});
					});
					 
					 
					 function load_victims_thana_permanent(val){
						 $.ajax({
							 type: "POST",
							 url: "<?php echo site_url("incident_info/get_ctrl_victims_thana_permanent");?>",
							 data: {victims_district_permanent: val},
							 beforeSend: function(  ) {
								//$('#loading_modal').modal('toggle');
							 }
						   })
						   .done(function( data ) {
							   //console.log(data);
								var obj = $.parseJSON(data);
								//console.log(obj);
								$('#div_victims_thana_permanent').html(obj.content);
								
							
						   });
					 }
					 
					  function load_victims_thana_present(val){
						 $.ajax({
								 type: "POST",
								 url: "<?php echo site_url("incident_info/get_ctrl_victims_thana_present");?>",
								 data: {victims_district_present: val},
								 beforeSend: function(  ) {
									//$('#loading_modal').modal('toggle');
								 }
							   })
						   .done(function( data ) {
							   //console.log(data);
								var obj = $.parseJSON(data);
								//console.log(obj);
								$('#div_victims_thana_present').html(obj.content);
			
		
							});
					 }
					 
	$("#victims_district_permanent").change(function() {
	var val = $(this).val();
	console.log(val);
	load_victims_thana_permanent(val);
		
		
});

$("#victims_district_present").change(function() {
	var val = $(this).val();
	console.log(val);
	load_victims_thana_present(val);
		
		
});

$("#respondents_district_permanent").change(function() {
	var val = $(this).val();
	console.log(val);
	$.ajax({
		 type: "POST",
		 url: "<?php echo site_url("incident_info/get_ctrl_respondents_thana_permanent");?>",
		 data: {respondents_district_permanent: val},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   //console.log(data);
			var obj = $.parseJSON(data);
			//console.log(obj);
			$('#div_respondents_thana_permanent').html(obj.content);
			
		
	   });
		
		
});

$("#respondents_district_present").change(function() {
	var val = $(this).val();
	console.log(val);
	$.ajax({
		 type: "POST",
		 url: "<?php echo site_url("incident_info/get_ctrl_respondents_thana_present");?>",
		 data: {respondents_district_present: val},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   //console.log(data);
			var obj = $.parseJSON(data);
			//console.log(obj);
			$('#div_respondents_thana_present').html(obj.content);
			
		
	   });
		
		
});

 $("#complainants_district_permanent").change(function() {
	var val = $(this).val();
	console.log(val);
	$.ajax({
		 type: "POST",
		 url: "<?php echo site_url("incident_info/get_ctrl_complainants_thana_permanent");?>",
		 data: {complainants_district_permanent: val},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   //console.log(data);
			var obj = $.parseJSON(data);
			//console.log(obj);
			$('#div_complainants_thana_permanent').html(obj.content);
			
		
	   });
		
		
});

$("#complainants_district_present").change(function() {
	var val = $(this).val();
	console.log(val);
	$.ajax({
		 type: "POST",
		 url: "<?php echo site_url("incident_info/get_ctrl_complainants_thana_present");?>",
		 data: {complainants_district_present: val},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   //console.log(data);
			var obj = $.parseJSON(data);
			//console.log(obj);
			$('#div_complainants_thana_present').html(obj.content);
			
		
	   });
		
		
});
$(document).ready(function() {
    //set initial state.
    //$('#textbox1').val($(this).is(':checked'));

    $('#victims_self').change(function() {
        if($(this).is(":checked")) {
             $('#victims_name').val($('#complainants_name').val());
			 $('#victims_fathers_name').val($('#complainants_fathers_name').val());
			 $('#victims_mothers_name').val($('#complainants_mothers_name').val());
			 $('#victims_spouses_name').val($('#complainants_spouses_name').val());
			 $('#victims_sex').val($('#complainants_sex').val());
			 $('#victims_age').val($('#complainants_age').val());
			 $('#victims_religion').val($('#complainants_religion').val());
			 $('#victims_village_permanent').val($('#complainants_village_permanent').val());
			 $('#victims_district_permanent').val($('#complainants_district_permanent').val());
			 load_victims_thana_permanent($('#complainants_district_permanent').val());
			 $("#victims_thana_permanent").selectmenu();
			 $("#victims_thana_permanent").selectmenu('refresh', true);
			 console.log($('#complainants_thana_permanent').val());
	         $('#victims_thana_permanent').val($('#complainants_thana_permanent').val());
	         $('#victims_phone_permanent').val($('#complainants_phone_permanent').val());
	         $('#victims_fax_permanent').val($('#complainants_fax_permanent').val());
	         $('#victims_email_permanent').val($('#complainants_email_permanent').val());
	         $('#victims_village_present').val($('#complainants_village_present').val());
	         $('#victims_district_present').val($('#complainants_district_present').val());
			 load_victims_thana_present($('#complainants_district_present').val());
			 console.log($('#complainants_thana_present').val());
			 $('#victims_thana_present').val($('#complainants_thana_present').val());
	         $('#victims_phone_present').val($('#complainants_phone_present').val());
	         $('#victims_fax_present').val($('#complainants_fax_present').val());
	         $('#victims_email_present').val($('#complainants_email_present').val());
			 
			 
			 
			 
			 
			 
			 
        }
            
    });
});

$("#victims_selected").click(function(){
	
	
	
	
});

        /*$(document).ready(function() {
            $('#create_user_form').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {

                                name: {
                                    validators: {
                                        notEmpty: {
                                                        message: 'name is empty...'
                                                }
                                    }
                                },
                                email: {
                                    validators: {
                                        email:  {
                                                        message: 'provide valid email'
                                                }
                                    }
                                }
                }
            });

        });*/
    </script>