<?php $this->load->view('common/header.php'); 



?>
<style>
    body {
        font-size: 12px;
		margin-left:30px;
    }
	.modal-backdrop.fade.in {
    z-index: 0;
	
	</style>
	
	<!--<script>
$(function() {
    $("#start_date").datepicker({dateFormat: "yy-mm-dd"});
	$("#end_date").datepicker({dateFormat: "yy-mm-dd"});
});
    
</script>-->
<div>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header" style="background:#cccccc"> Ticket Details /
            <small>List</small>
        </h1>
    </div>
</div>

<div class="table-responsive">
    <div class="col-lg-12">
		<?php if(isset($search_info)) echo "<h3>$search_info</h3>"?>
		<div> <h3 id="search_info"></h3>
<br/>

            <!--<input type="checkbox" class="selectAll" /> <span role='button' data-toggle='tooltip' data-placement='top' title='Delete selected' class='glyphicon glyphicon-trash linksDeleteSelected ' controllerMethod="<?php /*echo site_url('group_manager/deleteSelected') */?>" controllerReloadMethod="<?php /*echo site_url('group_manager/group_list') */?>" confirmationMsg='are you sure to delete selected group(s)' aria-hidden='true' onclick=''></span>-->
<table class="table table-bordered table-hover table-striped" id="my_datatable">
                <thead>
                <tr>
                    <!--<th style="width:10%;"></th>-->
                    <th>Sl</th>
                    <th>Ticket Number</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
					<th>Telephone</th>
					<th>Ticket Status</th>
					<th>Remarks</th>
				<!--	<th>Received By</th>-->
					<th>Assigned To</th>
					<th>Solved By</th>
					<th>Created Time</th>
					<th>Updated Time</th>
                    <th class="col-md-2">Action</th>

                </tr>
                </thead>
                
            </table>
      
		</div>
    </div>
	
	</div>
</div>
<script ></script>
<?php $this->load->view('common/footer.php'); ?>

<script type="text/javascript">




		<?php 
	if(isset($search_contact)&&$search_contact==true){
	?>
		$(document).ready(function() {
			$("#result").hide();
        
			var primary_number="<?php echo $primary_number;?>";
			$.ajax({ 		
				type: "POST" 
				,url: "<?php echo site_url('contact/search_ticket');?>" 
				,data:{primary_number:'<?php echo $primary_number;?>'}
				,async: false
				,success: function(response) { // on success..
					var obj=JSON.parse(response);
					if(obj.record_count==0){
					    $('#search_info').html("Number "+primary_number+" is not in your Sales list. <a role='button' class='btn btn-info' href='<?php echo site_url("sale_details/create_sale/$primary_number");?>'> Add to Sale</a>");
					}
					else {
						$('#search_info').html("Search result of primary number: "+primary_number);
					}
					var query_id=obj.query_id;
					console.log(response);
					$('#my_datatable').dataTable({
						destroy: true, //use this to reinitiate the table, other wise problem will occur
						processing: true,
						serverSide: true,
						ajax: {
							url: "<?php echo site_url('contact/process_paging_ticket');?>"
							,type: 'POST'
							,data:{query_id: query_id}
						}
					});
				}
			});
		} );
	
	<?	
	}
	else{
	?>
$(document).ready(function() {
	        $("#result").hide();
			$('#my_datatable').dataTable({
				destroy: true, //use this to reinitiate the table, other wise problem will occur
				processing: true,
				serverSide: true,
				ajax: {
					url: "<?php echo site_url('contact/process_paging_ticket');?>"
					,type: 'POST'
					,data:{query_id: '0'}
				}
			});
		} );
		
 <?
	}
 ?>		
		

	$( "form" ).submit(function( e ) {
		e.preventDefault();
		e.stopImmediatePropagation();
		$("#result").show();		
		//alert('form submitted');
		$.ajax({ // create an AJAX call...
			data: $(this).serialize(), // get the form data
			type: $(this).attr('method'), // GET or POST
			url: $(this).attr('action'), // the file to call
			success: function(response) { // on success..
				var obj=JSON.parse(response);
				var query_id=obj.query_id;
				console.log(response);
				$('#my_datatable').dataTable({
					destroy: true, //use this to reinitiate the table, other wise problem will occur
					processing: true,
					serverSide: true,
					ajax: {
						url: "<?php echo site_url('contact/process_paging_ticket');?>"
						,type: 'POST'
						,data:{query_id: query_id}
					}
				});
				
			}
		});
		return false;
	});
    jQuery(document.body).on('click', '.delete', function (e) {
        var this_holder = this;
        e.preventDefault();
		var delete_url= $(this).attr('href');
		

        bootbox.confirm("Are you sure you want to delete this entry?", function (response) {
            if (response) {
                $.ajax({
                    url: delete_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
                            $(this_holder).closest('td').closest('tr').hide(1000);
                        } else {

                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });

            }
        });
    });
	
		
    $(document).on('click', '.linksDeleteSelected', deleteSelected);
    $(document).on('change', '.selectAll', function () {
        if ($(this).is(":checked")) {
            $('.resultRow').prop('checked', true);
        }
        else
            $('.resultRow').prop('checked', false);
    });

    function deleteSelected(e) {
        if ($('.resultRow:checked').size() < 1) {
            alert('please select at least one record');
            return false;
        }
        var r = confirm(e.target.getAttribute("confirmationMsg"));
        if (r == false)
            return false;
        var controllerMethod = e.target.getAttribute("controllerMethod");
        var url = controllerMethod;
        /*var obj = {
            "flammable": "inflammable",
            "duh": "no duh"
        };
        $.each( obj, function( key, value ) {
            alert( key + ": " + value );
        });*/

        var selectedIDs = [];
        $('.resultRow:checked').each(function () {
            selectedIDs.push($(this).val());
        });
        console.log(selectedIDs);
        $.post(
            url,
            {selectedIDs: selectedIDs.join(", ")},
            function (data) {
                var obj = $.parseJSON(data);
                if(obj.success){
                    showStatusModal('Success','modal-info',obj.content);
                    reloadResult(e.target.getAttribute("controllerReloadMethod"));
                }
                else showStatusModal('Error','modal-warning',obj.content);
                window.location.replace(e.target.getAttribute('controllerReloadMethod'));
            }
        );
        return false;
    }

</script>