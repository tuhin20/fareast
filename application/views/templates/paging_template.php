    <input type='text' style='display: none'	id='lastPage'  value='<?php echo $paging['lastPage'];?>' />
    <input type='text' 	id='totalItem' style='display: none'  value='<?php echo $paging['totalItem'];?>' /> <!--setting total pages. so that next time no need to go to database-->
    <input type='text' style='display: none'	id='hiddenItemPerPage'  value='<?php echo $paging['itemPerPage'];?>' />

    <!--the below field depend on the hidden item per page-->
    <?php
    if(isset($paging['recordRange']))
    {
        echo "<b>showing records ". $paging['recordRange']."</b>&nbsp&nbsp&nbsp&nbsp";
    }
    ?>


    item per page: <input type='text' size='4' class='itemPerPage'  value='<?php echo $paging['itemPerPage'];?>' />&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    <input type='text'   id="controllerUrl" style='display: none' value='<?php echo $paging['controllerUrl'];?>' /> <!-- style='display: none'-->
    <input type='text'   id='queryWithoutLimit' style='display: none' class='paging' value="<?php  echo $paging['queryWithoutLimit'];?>" /> <!--query may contain '' so double quotation is used. setting whereClauseWithoutLimitForPaging-->
    
    <button id='firstPage' data-toggle='tooltip' data-placement='top' title='first' class='btn btn-xs btn-default paging' value='<?php echo $paging['firstPage'];?>' <?php echo $paging['firstPageDisabled']; ?> > <span class="glyphicon glyphicon-step-backward" aria-hidden="true"> </button>&nbsp&nbsp
    <button id='previousPage' data-toggle='tooltip' data-placement='top' title='previous' class='btn btn-default btn-xs paging' value='<?php echo $paging['previousPage'];?>'  <?php echo $paging['previousPageDisabled']; ?> > <span class="glyphicon glyphicon-backward" aria-hidden="true"> </button>&nbsp&nbsp

    <input type='text' size='5' class=' pageSearch'  value="<?php echo $paging['currentPage'];?>" /> of <?php echo $paging['lastPage'];?> &nbsp&nbsp

    <button id='nextPage' data-toggle='tooltip' data-placement='top' title='next' class='btn btn-xs btn-default paging' value='<?php echo $paging['nextPage'];?>' <?php echo $paging['nextPageDisabled'];?> > <span class="glyphicon glyphicon-forward" aria-hidden="true"> </button>&nbsp&nbsp
    <button id='lastPage' data-toggle='tooltip' data-placement='top' title='last' class='btn btn-xs btn-default paging' value='<?php echo $paging['lastPage'];?>' <?php echo $paging['lastPageDisabled'];?> > <span class="glyphicon glyphicon-step-forward" aria-hidden="true"> </button>&nbsp&nbsp