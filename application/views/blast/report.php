<?php 
	$this->load->view('common/header.php'); 
?>
<header class="page-header">
	<h2>Report</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="index.html">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>Report</span></li>
			<li><span>blast</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>


<div class="row">
	<div class="col-md-12">
		
    </div>
</div>	

<div class="row">
    <div class="col-lg-12">
		<section  class="panel">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>
		
				<h2 class="panel-title">Blast Report</h2>
			</header>
			<div class="panel-body">
				<form class="form-horizontal" method='post' id="create_user_form" action="<?php echo site_url('blast/report_search');?>">
			
					
					
					<div class="form-group">
						<div class="col-md-3">
							<input type="text"  autocomplete="off" class="form-control input-sm " id="from_date" placeholder="Start Date" name="from_date" value="<?php echo isset($params['from_date'])?$params['from_date']:'';?>"> 
					
						</div>
						<div class="col-md-3">
							<input type="text" autocomplete="off" class="form-control input-sm" id="to_date" placeholder="End Date" name="to_date" value="<?php echo isset($params['to_date'])?$params['to_date']:'';?>">
						</div>
						<!--<div class="col-md-3">
							<select data-plugin-selectTwo class="form-control input-sm"  name="code" id="code" >
								<option value='' >--</option>
								<?php
								/*$options=$this->blast_model->get_codes();
								foreach($options as $item)
								{
									$selected='';
									echo "<option value='$item[code]' $selected>$item[code]</option>";
								}*/

								?>
							</select>
						</div>-->
						<div class="col-md-3">
							<select data-plugin-selectTwo class="form-control input-sm"  name="campaign" id="campaign" >
								<option value='' >--</option>
								<?php
								$options=$this->blast_model->get_campaigns();
								foreach($options as $item)
								{
									$selected='';
									echo "<option value='$item[campaign]' $selected>$item[campaign]</option>";
								}

								?>
							</select>
						</div>
						<div class="col-md-2">
							<button type="submit" id="save_customer_details"  name="save_customer_details" class="btn btn-sm btn-warning glyphicon glyphicon-search"></button>
						</div>
					
					</div>
					
					
					
				</form>
				<br>
				<br>
				<?php //print_r($report);?>
				<!--<div class='table-responsive'>-->
					<a href="<?php echo site_url('blast/download_report');?>">
						<img src="<?php echo base_url('assets/images/download.png');?>" alt="Download" width="50" ><!--height="142"-->
					</a>
					<table class="table table-bordered table-hover table-striped " id="my_datatable">
						<thead>
							<!-- table header -->
							<tr>
								<th>SL</th>
								<th>Campaign</th>
								<th>Calldate</th>
								<th>Source</th>
								<th>Destination</th>
								<th>Status</th>
								<th>Duration</th>
								<th>Billsec</th>
								
							</tr>
							
							<!-- end of table header-->
						</thead>
						<tbody>	
							
							
							
						</tbody>
					</table>				
				<!--</div>-->

			</div>
		</section>	
		
        
		
	
	
    </div>
</div>


<?php $this->load->view('common/footer.php'); ?>

<script type="text/javascript">
	
	jQuery('#from_date').datetimepicker({format:'Y-m-d H:i'});//timepicker:false
	jQuery('#to_date').datetimepicker({format:'Y-m-d H:i'}); //timepicker:false
	
	$(document).ready(function() {
			//alert('doc loaded');
			//$("#result").hide();
			/*var table=$('#my_datatable').dataTable({
				destroy: true, //use this to reinitiate the table, other wise problem will occur
				processing: true,
				serverSide: true,
				//responsive: true,
				ajax: {
					url: "<?php echo site_url('blast/report_paging');?>"
					,type: 'POST'
					,data:{query_id: '0'}
				}
			});*/
			
			//var table = $('#example').DataTable();
 
			//$('#container').css( 'display', 'block' );
			//table.columns.adjust().draw();
			
		} );
		
	$( "form" ).submit(function( e ) {
        e.preventDefault();
        e.stopImmediatePropagation();
		$("#result").show();
		$.ajax({
                data: $(this).serialize(), // get the form data
				type: $(this).attr('method'), // GET or POST
				url: $(this).attr('action'), // the file to call
                dataType : 'json',
				beforeSend: function(  ) {
                    $('#loading_modal').modal('toggle');
                }
            })
            .done(function( response ) {
				console.log(response);
				//alert('success');
                //var obj=JSON.parse(response);
                var query_id=response.query_id;
                //var records_total=obj.records_total;
                
                $('#my_datatable').dataTable({
                    destroy: true, //use this to reinitiate the table, other wise problem will occur
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "<?php echo site_url('blast/report_paging')?>"
                        ,type: 'POST'
                        ,data:{query_id: query_id} //,'records_total': records_total
                    },
					dom: 'Bfrtip',
			
					buttons: [
						/*{ extend: 'print', exportOptions: {
															columns: ':visible'
														}
						},*/
						//{ extend: 'copy', className: 'copyButton' },
						//{ extend: 'excel', text: 'Excel',className: 'excelButton' }
						
					]
                });
                $('#loading_modal').modal('toggle');
            });
        //alert('form submitted');
        
        return false;
    });
	
</script>
