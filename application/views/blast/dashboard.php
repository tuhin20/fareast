<?php $this->load->view('common/header.php'); ?>
	<style>
		.bordered{
			border: 1px solid #dddddd;
		}
		.font-size20{
			font-size:20px;
		}
	</style>

	<header class="page-header">
		<h2>Metronet CRM</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Metronet CRM</span></li>
				<li><span>Dashboard</span></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	
	<div class='row'>
		<div class="col-md-12">
			<section class="panel" >
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>
			
					<h2 class="panel-title">Dashboard</h2>
				</header>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-3">
							<div class="panel panel-primary" >
								<div class="panel-heading">Balance</div>
								<div class="panel-body bordered font-size20" >BDT: <b><?php echo $this->user_model->get_balance();?></b> </div>
								<!--<div class="panel-footer">Panel Footer</div>-->
							</div>
						</div>
					</div>
				
				</div>
				
			</section>
						
		
		</div>
	</div>
					
						
						

<?php $this->load->view('common/footer.php'); ?>

