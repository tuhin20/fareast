<?php $this->load->view('common/header.php'); ?>

	<header class="page-header">
		<h2>Voice Blast</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Voice Blast</span></li>
				<li><span><?php echo $ui['title']; ?></span></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	<div class="row">
		<div class=" col-md-12"> 	<!-- this is alert size-->
			
		</div>
	</div>
	
	<div class='row'>
		<div class="col-md-7">
			<?php 
				$return_value=$this->session->flashdata('return_value');
				if(isset($return_value)){
	
					$msg=$return_value['msg'];
					$close_button="<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>";
					if($return_value['success'] == true){
						echo "<div class='alert alert-primary' role='alert'>$close_button $msg </div>";
					}
					else if($msg!='') //if not success and msg not empty
					{
						echo "<div class='alert alert-danger' role='alert'>$close_button $msg</div>";
					}
				}
			?>

			<form id="blast_form" method="post" action="<?php echo $ui['action']; ?>" class="form-horizontal" enctype="multipart/form-data">
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
							<a href="#" class="fa fa-times"></a>
						</div>

						<h2 class="panel-title">Voice Blast</h2>
						<p class="panel-subtitle">
							<!--Basic validation will display a label with the error after the form control.-->
						</p>
					</header>
					<div class="panel-body">
						<!--<div class="form-group">
							<label class="col-sm-3 control-label">Full Name <span class="required">*</span></label>
							<div class="col-sm-9">
								<input type="text" name="fullname" class="form-control" placeholder="eg.: John Doe" required/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email <span class="required">*</span></label>
							<div class="col-sm-9">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-envelope"></i>
									</span>
									<input type="email" name="email" class="form-control" placeholder="eg.: email@email.com" required/>
								</div>
							</div>
							<div class="col-sm-9">

							</div>
						</div>-->
						
						<input type="hidden" id="id" name="id" value="<?php echo isset ($params['id'])? $params['id']:-1;  ?>">
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Campaign <span class="required">*</span></label>
							<div class="col-sm-9">
								<input type="text"  name="campaign" id="campaign" class="form-control" placeholder="Campaign" value="<?php echo isset ($params['campaign'])? $params['campaign']:'';  ?>" required/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Callerid<span class="required">*</span></label>
							<div class="col-sm-9">
							
								<?php
									if($this->user_model->is_admin()){
								?>		
										<input type="text"  name="callerid" id="callerid" class="form-control" placeholder="Callerid" value="<?php echo isset ($params['callerid'])? $params['callerid']:'';  ?>" required/>
								<?php	}
								
								else{
									$options=$this->blast_model->get_callerids();
									//$options[]=array('id'=>'09612341514');
									//$options[]=array('id'=>'09612341509');		
								?>

									<select data-plugin-selectTwo class="form-control "  name="callerid" id="callerid" required>
										<option value='' >--</option>
										<?php
										foreach($options as $item)
										{
											$selected='';
											if(isset($params['callerid']) && $params['callerid']==$item['id']) 
												$selected='selected';
											echo "<option value='$item[id]' $selected>$item[id]</option>";
										}

										?>
									</select>
								<?php }?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Number List <span class="required">*</span></label>
							<div class="col-sm-5 " >
								<!--<textarea name="skills" rows="5" class="form-control" placeholder="Describe your skills" required></textarea>-->
								<input type="file" class="" id="number_file" name="number_file" >
								<p class="info">File types allowed:  xls|xlsx.</p>
    
								<br>
								
							</div>
							<div class="col-md-2">
								<a class="btn btn-info btn-sm glyphicon glyphicon-save" title="Sample File" href="<?php echo base_url('csv_files/sample_file.xlsx');?>"> Sample </a>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Voice File <span class="required">*</span></label>
							<div class="col-sm-5 ">
							
								<input type="file" class="" id="voice_file" name="voice_file" >
								<p class="info">File types allowed: gsm | wav.</p>
    
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"> Voice Conversion</label>
							<div class="col-sm-2">
								
								<input type="checkbox" name="convert" id="convert" class="js-switch2"  checked/>
								
							</div>							
						</div>
						
						
						<?php
							if($this->user_model->is_admin()){
								$max_limit=200;
							}						
							else{
								$max_limit=$this->blast_model->get_channel();
							}							
						?>
						<div class="form-group">
							<label class="col-sm-3 control-label">Channel <span class="required">*</span></label>
							<div class="col-sm-9">
								<input type="number" min="1" max="<?php echo $max_limit;?>" name="max_channel" id="max_channel" class="form-control" placeholder="1" value="<?php echo isset ($params['max_channel'])? $params['max_channel']:$max_limit;  ?>" required/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Retry <span class="required">*</span></label>
							<div class="col-sm-9">
								<input type="number" min="0" name="retry" id="retry" class="form-control" value="<?php echo isset ($params['retry'])? $params['retry']:0;  ?>" required/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Schedule </label>
							<div class="col-sm-2">
								
								<input type="checkbox" name="is_scheduled" id="is_scheduled" class="js-switch" <?php echo isset ($params['is_scheduled'])&& $params['is_scheduled']=='Y' ? 'checked':'';  ?> />
								
							</div>
							
							<div class="col-sm-7" id="div_schedule_date" style="<?php echo isset ($params['is_scheduled'])&& $params['is_scheduled']=='Y' ? '':'display:none';  ?>" >
								<input type="text"  autocomplete="off" name="schedule_date" id="schedule_date" class="form-control" placeholder="Schedule date" value="<?php echo isset ($params['schedule_date'])? $params['schedule_date']:'';  ?>" />
							</div>
						</div>
						
						<br><br>
						
					</div>
					<footer class="panel-footer">
						<div class="row">
							<div class="col-sm-9 col-sm-offset-3">
								<button type="submit" class="btn btn-primary btn-lg">&nbsp;&nbsp;<?php echo $ui['okButton'];?>&nbsp;&nbsp;</button>
								<!--<button type="reset" class="btn btn-default">Reset</button>-->
							</div>
						</div>
					</footer>
				</section>
			</form>
		</div>
	</div>
			
					

<?php $this->load->view('common/footer.php'); ?>

<script type="text/javascript">
	
	var elem = document.querySelector('.js-switch');
	var switchery = new Switchery(elem);

	var changeCheckbox = document.querySelector('.js-switch');

	changeCheckbox.onchange = function() {
	  if(changeCheckbox.checked){
		  $('#div_schedule_date').show();
	  }
	  else{
		  $('#div_schedule_date').hide();	  
	  }
	};
	
	var elem = document.querySelector('.js-switch2');
	var switchery = new Switchery(elem);


	$( "form" ).submit(function( e ) {
		
		if($("#blast_form").valid()){
			$('#loading_modal').modal('toggle'); //show modal to take time during form submit
			if( $.trim($('#campaign').val())!=""){ //$('#job_status').val()=='Regular'&&
				var ok=true;
				url="<?php echo site_url('blast/do_campaign_already_exist'); ?>";
				url +="/"+$("#id").val()+'/'+$.trim($('#campaign').val());
				//alert(url);
				$.ajax({
					url : url,
					//data : 'package='+1+'&day='+dayValue,
					data : 'id='+$("#id").val(),
					type : 'post',
					dataType : 'text',
					
					success : function(response){
						console.log(response);
						if(response==1){
							$('#loading_modal').modal('toggle'); //dismiss modal
							bootbox.alert({
								message: "<p class='error'> <strong>Danger!</strong> Campaign already exists. </p>",
								backdrop: true
							});
							ok=false;
						}
					},
					async: false
				});
				return ok;
			}
		}
	});


	$(document).ready(function() {
		jQuery('#schedule_date').datetimepicker({format:'Y-m-d H:i:s'});//timepicker:false
		$('#blast_form').validate({ // initialize the plugin
			rules: {
				number_file: {
					required: true,
					extension: "xls|xlsx"
				},
				voice_file: {
					extension: "gsm|wav|sln",
					required: true,
					//minlength: 5
				}
			}
		});
	} );
		
	

</script>
