<?php $this->load->view('common/header.php'); ?>

	<header class="page-header">
		<h2>Voice Blast</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Voice Blast</span></li>
				<li><span>List</span></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	
	<div class='row'>
		<div class="col-md-12">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>
			
					<h2 class="panel-title">List</h2>
				</header>
				<div class="panel-body ">
					<form class="form-horizontal" method='post' id="create_user_form" action="<?php echo site_url('blast/search');?>">
			
					
					
					<div class="form-group">
						<div class="col-md-3">
							<input type="text"  autocomplete="off" class="form-control input-sm " id="from_date" placeholder="Start Date" name="from_date" value="<?php echo isset($params['from_date'])?$params['from_date']:'';?>"> 
					
						</div>
						<div class="col-md-3">
							<input type="text" autocomplete="off" class="form-control input-sm" id="to_date" placeholder="End Date" name="to_date" value="<?php echo isset($params['to_date'])?$params['to_date']:'';?>">
						</div>
						<?php
							if($this->user_model->is_admin()){
						?>		
								<div class="col-md-3">
									<select data-plugin-selectTwo class="form-control input-sm"  name="client_id" id="client_id" >
										<option value='' >--</option>
										<?php
										$options=$this->blast_model->get_clients();
										foreach($options as $item)
										{
											$selected='';
											/*if(isset($params['police_unit']) && $item['police_unit_name']==trim($params['police_unit']) )
												$selected='selected';*/
											echo "<option value='$item[id]' $selected>$item[name]</option>";
										}

										?>
									</select>
								</div>
						<?php
							}
						?>
						<div class="col-md-2">
							<button type="submit" id="save_customer_details"  name="save_customer_details" class="btn btn-sm btn-warning glyphicon glyphicon-search"></button>
						</div>
					
					</div>
					
					<br><br>
					
				</form>
				
				
					<table class="table table-bordered table-hover table-striped" id="my_datatable">
						<thead>
							<tr>
								<th class='col-md-1'>#</th>
								<th class='col-md-2'>Created Date</th>
								<th class='col-md-1'>Campaign</th>
								<th class='col-md-1'>Callerid</th>
								<?php 
									if($this->user_model->is_admin()){
										echo "<th class='col-md-1'>Client</th>";
									}
								?>
								<th class='col-md-1' >Is Scheduled</th>
								<th class='col-md-1'>Max Channel</th>
								<th class='col-md-1'>Retry</th>
								<th class='col-md-1'>Status</th>
								<th class='col-md-3'>Action</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</section>
						
		
		</div>
	</div>
					
						
						

<?php $this->load->view('common/footer.php'); ?>
<script type="text/javascript">

	jQuery('#from_date').datetimepicker({format:'Y-m-d H:i'});//timepicker:false
	jQuery('#to_date').datetimepicker({format:'Y-m-d H:i'}); //timepicker:false

	$(document).ready(function() {
		//alert('doc loaded');
		$("#result").hide();
		$('#my_datatable').dataTable({
			destroy: true, //use this to reinitiate the table, other wise problem will occur
			processing: true,
			serverSide: true,
			//responsive: true,
			ajax: {
				url: "<?php echo site_url('blast/paging');?>"
				,type: 'POST'
				,data:{query_id: '0'}
			}
		});
	} );
		
	$( "form" ).submit(function( e ) {
        e.preventDefault();
        e.stopImmediatePropagation();
		$("#result").show();
		$.ajax({
                data: $(this).serialize(), // get the form data
				type: $(this).attr('method'), // GET or POST
				url: $(this).attr('action'), // the file to call
                dataType : 'json',
				beforeSend: function(  ) {
                    $('#loading_modal').modal('toggle');
                }
            })
            .done(function( response ) {
				console.log(response);
				//alert('success');
                //var obj=JSON.parse(response);
                var query_id=response.query_id;
                //var records_total=obj.records_total;
                
                $('#my_datatable').dataTable({
                    destroy: true, //use this to reinitiate the table, other wise problem will occur
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "<?php echo site_url('blast/paging')?>"
                        ,type: 'POST'
                        ,data:{query_id: query_id} //,'records_total': records_total
                    }
                });
                $('#loading_modal').modal('toggle');
            });
        //alert('form submitted');
        
        return false;
    });
		

	
	
	
		
    jQuery(document.body).on('click', '.delete', function (e) {
        var this_holder = this;
		//$(this).attr("disabled",true);
        e.preventDefault();
		var delete_url= $(this).attr('href');
		

        bootbox.confirm("Are you sure to Delete ", function (response) {
            if (response) {
                $.ajax({
                    url: delete_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
							console.log(data);
							$(this_holder).parents("tr").hide(1000);
                            //$(this_holder).closest('td').closest('tr').hide(1000);
							//alert('');
                        } else {
							bootbox.alert("Problem deleting data");
                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });
				

            }
        });
    });
	
	jQuery(document.body).on('click', '.demo', function (e) {
        var this_holder = this;
		//$(this).attr("disabled",true);
        e.preventDefault();
		var url= $(this).attr('href');
		
		bootbox.prompt({ 
			size: "small",
			title: "Phone Number",
			callback: function(result){ 
				/* result = String containing user input if OK clicked or null if Cancel clicked */ 
				if(result){
					$.ajax({
					url : url,
					//data : 'package='+1+'&day='+dayValue,
					data : 'dst='+result,
					type : 'post',
					dataType : 'json',
					beforeSend: function(  ) {
						$('#loading_modal').modal('toggle');
					},
					success : function(response){
						$('#loading_modal').modal('toggle');
						if(response.success){
							var msg="<p class='success'><strong>Success</strong> "+response.msg+"</p>";	
							
						}
						else{
							var msg="<p class='error'><strong>Danger!</strong> "+response.msg+"</p>";							
						}
						bootbox.alert({
							message: msg,
							backdrop: true,
							callback: function(){ 
								/*if(response.success){
								location.reload();
								} */
							}
						});
						
						console.log(response);
					},
					error: function (jqXhr, textStatus, errorThrown) {
						$('#loading_modal').modal('toggle');
                        alert(errorThrown);
                    }
				});
				}
			}
		});
    });
	
	jQuery(document.body).on('click', '.approve', function (e) {
        var this_holder = this;
		//$(this).attr("disabled",true);
        e.preventDefault();
		var url= $(this).attr('href');
		

        bootbox.confirm("Are you sure to Approve ", function (response) {
            if (response) {
				$.ajax({
					url : url,
					//data : 'package='+1+'&day='+dayValue,
					//data : type+'='+value,
					type : 'post',
					dataType : 'json',
					beforeSend: function(  ) {
						$('#loading_modal').modal('toggle');
					},
					success : function(response){
						console.log(response);
						$('#loading_modal').modal('toggle');
						if(response.success){
							var msg="<p class='success'><strong>Success</strong> "+response.msg+"</p>";	
							
						}
						else{
							var msg="<p class='error'><strong>Danger!</strong> "+response.msg+"</p>";							
						}
						bootbox.alert({
							message: msg,
							backdrop: true,
							callback: function(){ 
								if(response.success){
								location.reload();
							} }
						});
						
						console.log(response);
						//$('#member_short_details').html(response.content);
					},
					error: function (jqXhr, textStatus, errorThrown) {
						$('#loading_modal').modal('toggle');
                        alert(errorThrown);
                    }
				});
				
            }
        });
    });

</script>
