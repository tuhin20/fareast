<?php $this->load->view('common/header.php'); ?>

	<header class="page-header">
		<h2>Voice Blast</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Voice Blast</span></li>
				<li><span>Details</span></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	
	<div class='row'>
		<div class="col-md-6">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>
			
					<h2 class="panel-title">Details</h2>
				</header>
				<div class="panel-body">
					<table class="table table-bordered table-hover table-striped" id="">
						<tbody>
						<?php
							if(isset($blast)){ 
								echo "<tr><th>Created Date</th><td>$blast[created_date]</td></tr>";
								echo "<tr><th>Callerid</th><td>$blast[callerid]</td></tr>";
								echo "<tr><th>Number List</th>
											<td>	$blast[dst_file_name] &nbsp;&nbsp;
													<a class='btn btn-info btn-sm glyphicon glyphicon-save' title='Download' href=".base_url("csv_files/$blast[dst_file_name]").">  </a>
											</td>
									</tr>";
								echo "<tr>
											<th>Audio File</th>
											<td>	$blast[voice_file_name] &nbsp;&nbsp;
													<a class='btn btn-info btn-sm glyphicon glyphicon-save' title='Download' href=".base_url("voice_files/$blast[voice_file_name]").">  </a>
											</td>
									</tr>";	
								echo "<tr><th>Channel</th><td>$blast[max_channel]</td></tr>";	
								echo "<tr><th>Retry</th><td>$blast[retry]</td></tr>";	
								echo "<tr><th>Status</th><td>$blast[status]</td></tr>";	
								echo "<tr><th>Is Scheduled</th><td>$blast[is_scheduled]</td></tr>";	
								echo "<tr><th>Schedule Date</th><td>$blast[schedule_date]</td></tr>";	
								
							}
						?>
							
						</tbody>
					</table>
				</div>
			</section>
						
		
		</div>
	</div>
					
						
						

<?php $this->load->view('common/footer.php'); ?>
<script type="text/javascript">

   /*$('#start_date').datetimepicker({dateFormat: "yy-mm-dd", timeFormat: "HH:mm"});
    $('#end_date').datetimepicker({dateFormat: "yy-mm-dd", timeFormat: "HH:mm"});*/


		$(document).ready(function() {
			//alert('doc loaded');
			$("#result").hide();
			$('#my_datatable').dataTable({
				destroy: true, //use this to reinitiate the table, other wise problem will occur
				processing: true,
				serverSide: true,
				responsive: true,
				ajax: {
					url: "<?php echo site_url('blast/paging');?>"
					,type: 'POST'
					,data:{query_id: '0'}
				}
			});
		} );
		
	$( "form" ).submit(function( e ) {
        e.preventDefault();
        e.stopImmediatePropagation();
		$("#result").show();
		$.ajax({
                data: $(this).serialize(), // get the form data
				type: $(this).attr('method'), // GET or POST
				url: $(this).attr('action'), // the file to call
                beforeSend: function(  ) {
                    $('#loading_modal').modal('toggle');
                }
            })
            .done(function( response ) {
				console.log(response);
				//alert('success');
                var obj=JSON.parse(response);
                var query_id=obj.query_id;
                //var records_total=obj.records_total;
                
                $('#my_datatable').dataTable({
                    destroy: true, //use this to reinitiate the table, other wise problem will occur
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "<?php echo site_url('click2call/process_paging')?>"
                        ,type: 'POST'
                        ,data:{query_id: query_id} //,'records_total': records_total
                    }
                });
                $('#loading_modal').modal('toggle');
            });
        //alert('form submitted');
        
        return false;
    });
		

	
	
	
		
    jQuery(document.body).on('click', '.delete', function (e) {
        var this_holder = this;
		//$(this).attr("disabled",true);
        e.preventDefault();
		var delete_url= $(this).attr('href');
		

        bootbox.confirm("Are you sure to Delete ", function (response) {
            if (response) {
                $.ajax({
                    url: delete_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
							console.log(data);
							$(this_holder).parents("tr").hide(1000);
                            //$(this_holder).closest('td').closest('tr').hide(1000);
							//alert('');
                        } else {
							bootbox.alert("Problem deleting data");
                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });
				

            }
        });
    });
	
	jQuery(document.body).on('click', '.demo', function (e) {
        var this_holder = this;
		//$(this).attr("disabled",true);
        e.preventDefault();
		var url= $(this).attr('href');
		
		bootbox.prompt({ 
			size: "small",
			title: "Phone Number",
			callback: function(result){ 
				/* result = String containing user input if OK clicked or null if Cancel clicked */ 
				if(result){
					$.ajax({
					url : url,
					//data : 'package='+1+'&day='+dayValue,
					data : 'dst='+result,
					type : 'post',
					dataType : 'json',
					beforeSend: function(  ) {
						$('#loading_modal').modal('toggle');
					},
					success : function(response){
						$('#loading_modal').modal('toggle');
						if(response.success){
							var msg="<p class='success'><strong>Success</strong> "+response.msg+"</p>";	
							
						}
						else{
							var msg="<p class='error'><strong>Danger!</strong> "+response.msg+"</p>";							
						}
						bootbox.alert({
							message: msg,
							backdrop: true,
							callback: function(){ 
								/*if(response.success){
								location.reload();
								} */
							}
						});
						
						console.log(response);
					},
					error: function (jqXhr, textStatus, errorThrown) {
						$('#loading_modal').modal('toggle');
                        alert(errorThrown);
                    }
				});
				}
			}
		});
    });
	
	jQuery(document.body).on('click', '.approve', function (e) {
        var this_holder = this;
		//$(this).attr("disabled",true);
        e.preventDefault();
		var url= $(this).attr('href');
		

        bootbox.confirm("Are you sure to Approve ", function (response) {
            if (response) {
				$.ajax({
					url : url,
					//data : 'package='+1+'&day='+dayValue,
					//data : type+'='+value,
					type : 'post',
					dataType : 'json',
					beforeSend: function(  ) {
						$('#loading_modal').modal('toggle');
					},
					success : function(response){
						$('#loading_modal').modal('toggle');
						if(response.success){
							var msg="<p class='success'><strong>Success</strong> "+response.msg+"</p>";	
							
						}
						else{
							var msg="<p class='error'><strong>Danger!</strong> "+response.msg+"</p>";							
						}
						bootbox.alert({
							message: msg,
							backdrop: true,
							callback: function(){ 
								if(response.success){
								location.reload();
							} }
						});
						
						console.log(response);
						//$('#member_short_details').html(response.content);
					},
					error: function (jqXhr, textStatus, errorThrown) {
						$('#loading_modal').modal('toggle');
                        alert(errorThrown);
                    }
				});
				
            }
        });
    });

</script>
