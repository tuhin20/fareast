<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>পুলিশ মেডিকেল</title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="<?php echo base_url('assets/css/lib/weather-icons.css');?>" rel="stylesheet" />
        <link href="<?php echo base_url('assets/css/lib/owl.carousel.min.css');?>" rel="stylesheet" />  
        <link href="<?php echo base_url('assets/css/lib/owl.theme.default.min.css');?>" rel="stylesheet" />
        <link href="<?php echo base_url('assets/css/lib/font-awesome.min.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/lib/themify-icons.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/lib/menubar/sidebar.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/lib/bootstrap.min.css');?>" rel="stylesheet">
		<!-- http://howladerventures.com/icons/-->
		<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" -->

        <link href="<?php echo base_url('assets/css/lib/helper.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/style.css?family=amma');?>" rel="stylesheet">
    </head>

	
    <body style="background:#343957;">
	
			
		
			<div id="loginbox" style="text-center ;margin-top:50px;" class="text-center ">                    
				<div class="panel panel-info" >
					<div class="panel-heading">
						<div class="panel-title">Sign In</div>
					</div>     
					<form method="post" action="<?php echo base_url(); ?>index.php/login/validate_user">
						<div style="padding-top:30px" class="panel-body" >
							<?php	if(isset($msg)){	?>
							<div id="login-alert" class="alert alert-danger col-sm-12">
								<?php echo $msg; ?>
							</div>
							<?php	}	?>
							<form id="loginform" class="form-horizontal" role="form">
								<div style="margin-bottom: 25px" class="input-group">
								<?php //echo $password_encoded;?>
									<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
									<input id="login-username" type="text" class="form-control" name="username" value="" placeholder="username" required>                                        
								</div>
									
								<div style="margin-bottom: 25px" class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
									<input id="login-password" type="password" class="form-control" name="password" placeholder="password" required>
								</div>							

								<div style="margin-top:10px" class="form-group">
									<div class="col-sm-12 controls">
										<input type="submit" id="btn-login" href="#" class="btn btn-success" value="Login" />									
									</div>
								</div>									
							</form>                  
						</div>
					</form>
					
				</div>
			</div> 
			
			
			
<div class="row">	
	<div class="col-md-4" ></div>
	<div class="col-md-3">
		
		<div class="card">
			
			<div class="card-body">
			<h3 class="card-title"></h3>
			<br>
			<!--<div class="card shadow mb-12">
				<div class="card-header py-3">
				  <h6 class="m-0 font-weight-bold text-primary"></h6>
				</div>
				<div class="card-body">-->
			
				<form class="" method="post" action="<?php echo $ui['action']; ?>" accept-charset="utf-8" enctype="multipart/form-data">
	
					
							<div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Recipient's username">
                                            <div class="input-group-append">
                                                <span class="input-group-text">@example.com</span>
                                            </div>
                                        </div>
										
										
								<div class="form-group text-center">
									
									<input type="text" autocomplete="off" class="form-control input-lg " id="name" name="name" placeholder="user name" value="<?php echo isset($params['name'])?$params['name']:'';?>">
								</div>
								
								<div class="form-group">
									<input type="text" autocomplete="off" class="form-control input-lg" id="father" name="father" placeholder="" value="<?php echo isset($params['father'])?$params['father']:'';?>">
								</div>
								<br><br>
										
					
					
					<div class="form-group"> 
						<button type="submit" class="btn-block btn btn-lg btn-success">&nbsp;&nbsp;Login&nbsp;&nbsp;<br></button>
					</div>
				
				
	
	
				</form>	
				
				
			</div> <!-- end of card-body -->
		</div>
	</div>    
</div>
		
	
			
			


<!-- jquery vendor -->
        <script src="<?php echo base_url('assets/js/lib/jquery.min.js');?>"></script>
        <script src="<?php echo base_url('assets/js/lib/jquery.nanoscroller.min.js');?>"></script>
        <!-- nano scroller -->
        <script src="<?php echo base_url('assets/js/lib/menubar/sidebar.js');?>"></script>
        <script src="<?php echo base_url('assets/js/lib/preloader/pace.min.js');?>"></script>
        <!-- sidebar -->
        <script src="<?php echo base_url('assets/js/lib/bootstrap.min.js');?>"></script>

        <!-- bootstrap -->

        <script src="<?php echo base_url('assets/js/lib/circle-progress/circle-progress.min.js');?>"></script>
        <script src="<?php echo base_url('assets/js/lib/circle-progress/circle-progress-init.js');?>"></script>

		<script src="<?php echo base_url('assets/js/vendor/jquery.dataTables.min.js')?>"></script>
		<link href="<?php echo base_url('assets/css/jquery.dataTables.min.css')?>" rel="stylesheet">
	

        <!--  flot-chart js -->
        <script src="<?php echo base_url('assets/js/lib/flot-chart/jquery.flot.js');?>"></script>
        <script src="<?php echo base_url('assets/js/lib/flot-chart/jquery.flot.resize.js');?>"></script>
        <script src="<?php echo base_url('assets/js/lib/flot-chart/flot-chart-init.js');?>"></script>
        <!-- // flot-chart js -->
		
		
        <script src="<?php echo base_url('assets/js/lib/weather/jquery.simpleWeather.min.js');?>"></script>
        <script src="<?php echo base_url('assets/js/lib/weather/weather-init.js');?>"></script>
        <script src="<?php echo base_url('assets/js/lib/owl-carousel/owl.carousel.min.js');?>"></script>
        <script src="<?php echo base_url('assets/js/lib/owl-carousel/owl.carousel-init.js');?>"></script>
        <script src="<?php echo base_url('assets/js/scripts.js');?>"></script>
        <script src="<?php echo base_url('assets/js/bootbox.js');?>"></script>
        <!-- scripit init-->

    </body>

</html>

?>			