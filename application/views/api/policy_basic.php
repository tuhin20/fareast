<?php $this->load->view('common/header.php'); ?>
	<style>
		.bordered{
			border: 1px solid #dddddd;
		}
		.font-size20{
			font-size:20px;
		}
	</style>

	<header class="page-header">
		<h2>Metronet CRM</h2>
	
	</header>
	
	
	<div class='row'>
		<div class="col-md-12">
			<section class="panel" >
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>
			
					<h2 class="panel-title">Dashboard</h2>
				</header>
				<div class="panel-body ">
					<div class="row">
						<div class="col-md-12">
							<form  method="post" action="<?php echo site_url('api/GetPolicy_basic');?> ">
							<h3>Please input Policy Number</h3>
							<input type="number" name="policy_no">
							<button type="submit" class="btn btn-primary button-loading">Submit</button>
							</form>
							<div class="table-responsive">
								<table  class="table table-striped table-bordered" id="myTable">

								<thead>

									<tr>
								   
									<!--<th><input type="checkbox" name="chk[]" value="0"/></th>-->
						
								<th>POLICY_NO</th>
								<th>SALUTE</th>
							    <th> POLICYHOLDER</th>
								<th>DATE_OF_BIRTH</th>
							    <th> GENDER</th>
								<th>AGE</th>
							    <th> AGE_PROF</th>
							    <th> OCCUPATION</th>
								<th>ASSURED_NAME</th>
								<th>AGE_YEAR</th>
							    <th>FATHER</th>
							    <th> MOTHER_NAME</th>
							    <th> HUSBAND</th>
								<th>PHONE</th>
								 <th> ADDRESS1</th>
								<th>ADDRESS2</th>
							    <th> ADDRESS3</th>
								<th>POST_CODE</th>
							    <th> TABLE</th>
								<th>TERM</th>
							    <th>PLAN_NAME</th>
							    <th>  RISK</th>
								<th>COM_DATE</th>
								<th>SUM_ASSURED</th>
							    <th> SUM_AT_RISK</th>
							    <th> PAY_MODE</th>
							    <th> POLICY_OPTION</th>
							    <th> PREMIUM_RATE</th>
							    <th> POLICY_STATUS</th>
							    <th> MEDICAL_OR_NON_MEDICAL</th>
							    <th> STD_OR_SUB_STD</th>
							    <th> LEON_CODE_AND_TERM</th>
							    <th>LIFE_PREWMIUM_INSTALMENT</th>
								<th>AC_CODE</th>
								<th>CLASS</th>
								<th>ACCIDENTAL_PREMIUM</th>
							    <th> OE_RATE</th>
							    <th> OE_PREM</th>
							    <th>TOTAL_PREM</th>
							    <th> LAST_PAID_DATE</th>
							    <th> NEXT_DUE_DATE</th>
									<th> MATURITY_DATE</th>
									<th> SUSPENSE</th>
								  </tr>
								</thead>
								<tbody>
								  <?php foreach ($basic_info as $value){ ?>

								  <tr>
									<td><?php echo $value['POLICY_NO'];?></td>
							        <td><?php echo $value['SALUTE'];?></td>
									<td><?php echo $value['POLICYHOLDER'];?></td>
									<td><?php echo $value['DATE_OF_BIRTH'];?></td>
									<td><?php echo $value['GENDER'];?></td>
									<td><?php echo $value['AGE'];?></td>
									<td><?php echo $value['AGE_PROF'];?></td>
									<td><?php echo $value['OCCUPATION'];?></td>
									<td><?php echo $value['ASSURED_NAME'];?></td>
									<td><?php echo $value['AGE_YEAR'];?></td>
									<td><?php echo $value['FATHER'];?></td>
									<td><?php echo $value['MOTHER_NAME'];?></td>
									<td><?php echo $value['HUSBAND'];?></td>
									<td><?php echo $value['PHONE'];?></td>
									<td><?php echo $value['ADDRESS1'];?></td>
									<td><?php echo $value['ADDRESS2'];?></td>
									<td><?php echo $value['ADDRESS3'];?></td>
									<td><?php echo $value['POST_CODE'];?></td>
									<td><?php echo $value['TABLE'];?></td>
									<td><?php echo $value['TERM'];?></td>
									<td><?php echo $value['PLAN_NAME'];?></td>
									<td><?php echo $value['RISK'];?></td>
									<td><?php echo $value['COM_DATE'];?></td>
									<td><?php echo $value['SUM_ASSURED'];?></td>
									<td><?php echo $value['SUM_AT_RISK'];?></td>
									<td><?php echo $value['PAY_MODE'];?></td>
									<td><?php echo $value['POLICY_OPTION'];?></td>
									<td><?php echo $value['PREMIUM_RATE'];?></td>
									<td><?php echo $value['POLICY_STATUS'];?></td>
									<td><?php echo $value['MEDICAL_OR_NON_MEDICAL'];?></td>
									<td><?php echo $value['STD_OR_SUB_STD'];?></td>
									<td><?php echo $value['LEON_CODE_AND_TERM'];?></td>
									<td><?php echo $value['LIFE_PREWMIUM_INSTALMENT'];?></td>
									<td><?php echo $value['AC_CODE'];?></td>
									<td><?php echo $value['CLASS'];?></td>
									<td><?php echo $value['ACCIDENTAL_PREMIUM'];?></td>
									<td><?php echo $value['OE_RATE'];?></td>
									<td><?php echo $value['OE_PREM'];?></td>
									<td><?php echo $value['TOTAL_PREM'];?></td>
									<td><?php echo $value['LAST_PAID_DATE'];?></td>
									<td><?php echo $value['NEXT_DUE_DATE'];?></td>
									<td><?php echo $value['MATURITY_DATE'];?></td>
									<td><?php echo $value['SUSPENSE'];?></td>
								  </tr>
								  <?php } ?>
								  
								</tbody>
							  </table>
							</div>
							
							
						</div>
						
					</div>
				
				<br><br><br><br><br><br>
				<br><br><br><br><br><br>
				<br><br><br><br><br><br>
				
				
					
				</div>
				
			</section>
						
		
		</div>
	</div>
					
						
						

<?php $this->load->view('common/footer.php'); ?>
</script>