<?php $this->load->view('common/header.php'); ?>
	<style>
		.bordered{
			border: 1px solid #dddddd;
		}
		.font-size20{
			font-size:20px;
		}
	</style>

	<header class="page-header">
		<h2>Metronet CRM</h2>
	</header>
	
	
	<div class='row'>
		<div class="col-md-12">
			<section class="panel" >
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>
			
					<h2 class="panel-title">Dashboard</h2>
				</header>
				<div class="panel-body ">
					<div class="row">
						<div class="col-md-12">
							<form action="<?php echo site_url('api/GetMaturityPolicy');?>" method="post">
							<h3>Please input Policy Number</h3>
							<input type="number" name="policy_no">
							<button type="submit" class="btn btn-primary button-loading">Submit</button>
							</form>
							<div class="table-responsive">
								<table  class="table table-striped table-bordered" id="myTable">

								<thead>

									<tr>
								   
									<!--<th><input type="checkbox" name="chk[]" value="0"/></th>-->
									<th>CLAIM_TYPE</th>
									<th>POLICY_NO</th>
									<th>SALUTE</th>
									<th>PROPOSER</th>
									<th>INSTMODE</th>
									<th>RISKDATE</th>
									<th>TABLE_ID</th>
									<th>TERM</th>
									<th>MATURITY</th>
									<th>ISSUE_DATE</th>
									
									<th>FINAL_STATUS</th>
									<th>SC_CODE</th>
									<th>BK_CODE</th>
									<th>BK_BR_CODE</th>
									<th>ROUT_NO</th>
									<th>ACC_NO</th>
									<th>LOTNO</th>
									<th>BATCHNO</th>
									<th>LOT_CLS_ST</th>
									<th>BATCH_DATE</th>
									<th>PHONE</th>
									<th>BATCH_CLS_ST</th>
									
									<th>ASSURED</th>
									<th>BATCH_CLS_DT</th>
									<th>UPD_USERID</th>
									<th>BEFTN</th>
									<th>CHK_NO</th>
									<th>CHK_DATE</th>
									<th>MAT_TYPE</th>
									<th>MP_SUMASS</th>
									<th>CBONUS</th>
									<th>BONUS</th>
									<th>TERMINAL</th>
									
									<th>TOT_BON</th>
									<th>FORFIET_AMT</th>
									<th>DROP_PSUMASS</th>
									<th>TAX</th>
									<th>TOTAL_AMT</th>
									<th>CHK_AMT</th>
									<th>SUM_INSURE</th>
									<th>SUSPENSE</th>
									<th>NET_PAY</th>
									<th>TAXABLE_AMT</th>
									<th>SB_PAID</th>
									
									<th>SADAKA_FUND</th>
									<th>PROPNAME</th>
									<th>PR_AMT</th>
									<th>PROPNO1</th>
									<th>PROPNAME1</th>
									<th>PR_AMT1</th>
									<th>PROPNAME2</th>
									<th>PR_AMT2</th>
									<th>REN_POLICY</th>
									<th>REN_PHNAME</th>
									<th>ADJUST_AMT</th>
									<th>REN_POLICY1</th>
									<th>REN_PHNAME1</th>
									
									<th>ADJUST_AMT1</th>
									<th>INV_NO</th>
									<th>INV_POLICY</th>
									<th>MR_AMT</th>
									<th>SAL_ADV_POLICY_NO</th>
									<th>SAL_ADV_ADJ_CODE</th>
									<th>SAL_ADV_ADJ</th>
									<th>REMARKS</th>
									<th>PAY_TO_NAME</th>
									
									
								  </tr>
								</thead>
								
								  
								
								<tbody>
								  <?php foreach ($maturity as $value){ ?>

								  <tr>
									
								 
									<td><?php echo $value['CLAIM_TYPE'];?></td>
									<td><?php echo $value['POLICY_NO'];?></td>
									<td><?php echo $value['SALUTE'];?></td>
									<td><?php echo $value['PROPOSER'];?></td>
									<td><?php echo $value['INSTMODE'];?></td>
									<td><?php echo $value['RISKDATE'];?></td>
									<td><?php echo $value['TABLE_ID'];?></td>
									<td><?php echo $value['TERM'];?></td>
									<td><?php echo $value['MATURITY'];?></td>
									<td><?php echo $value['ISSUE_DATE'];?></td>
									<td><?php echo $value['FINAL_STATUS'];?></td>
									
									<td><?php echo $value['SC_CODE'];?></td>
									<td><?php echo $value['BK_CODE'];?></td>
									<td><?php echo $value['BK_BR_CODE'];?></td>
									<td><?php echo $value['ROUT_NO'];?></td>
									<td><?php echo $value['ACC_NO'];?></td>
									<td><?php echo $value['LOTNO'];?></td>
									<td><?php echo $value['BATCHNO'];?></td>
									<td><?php echo $value['LOT_CLS_ST'];?></td>
									<td><?php echo $value['BATCH_DATE'];?></td>
									<td><?php echo $value['PHONE'];?></td>
									<td><?php echo $value['BATCH_CLS_ST'];?></td>
									
									<td><?php echo $value['ASSURED'];?></td>
									<td><?php echo $value['BATCH_CLS_DT'];?></td>
									<td><?php echo $value['UPD_USERID'];?></td>
									<td><?php echo $value['BEFTN'];?></td>
									<td><?php echo $value['CHK_NO'];?></td>
									<td><?php echo $value['CHK_DATE'];?></td>
									<td><?php echo $value['MAT_TYPE'];?></td>
									<td><?php echo $value['MP_SUMASS'];?></td>
									<td><?php echo $value['CBONUS'];?></td>
									<td><?php echo $value['BONUS'];?></td>
									<td><?php echo $value['TERMINAL'];?></td>
									
									<td><?php echo $value['TOT_BON'];?></td>
									<td><?php echo $value['FORFIET_AMT'];?></td>
									<td><?php echo $value['DROP_PSUMASS'];?></td>
									<td><?php echo $value['TAX'];?></td>
									<td><?php echo $value['TOTAL_AMT'];?></td>
									<td><?php echo $value['CHK_AMT'];?></td>
									<td><?php echo $value['SUM_INSURE'];?></td>
									<td><?php echo $value['SUSPENSE'];?></td>
									<td><?php echo $value['NET_PAY'];?></td>
									<td><?php echo $value['TAXABLE_AMT'];?></td>
									<td><?php echo $value['SB_PAID'];?></td>
									
									<td><?php echo $value['SADAKA_FUND'];?></td>
									<td><?php echo $value['PROPNAME'];?></td>
									<td><?php echo $value['PR_AMT'];?></td>
									<td><?php echo $value['PROPNO1'];?></td>
									<td><?php echo $value['PROPNAME1'];?></td>
									<td><?php echo $value['PR_AMT1'];?></td>
									<td><?php echo $value['REN_POLICY'];?></td>
									<td><?php echo $value['REN_PHNAME'];?></td>
									<td><?php echo $value['ADJUST_AMT'];?></td>
									<td><?php echo $value['REN_POLICY1'];?></td>
									<td><?php echo $value['REN_PHNAME1'];?></td>
									
									<td><?php echo $value['ADJUST_AMT1'];?></td>
									<td><?php echo $value['INV_NO'];?></td>
									<td><?php echo $value['INV_POLICY'];?></td>
									<td><?php echo $value['MR_AMT'];?></td>
									<td><?php echo $value['SAL_ADV_POLICY_NO'];?></td>
									<td><?php echo $value['SAL_ADV_ADJ_CODE'];?></td>
									<td><?php echo $value['SAL_ADV_ADJ'];?></td>
									<td><?php echo $value['REMARKS'];?></td>
									<td><?php echo $value['PAY_TO_NAME'];?></td>
									
									
									
									
								  </tr>
								  <?php } ?>
								  
								</tbody>
							  </table>
							</div>
							
							
						</div>
						
					</div>
				
				<br><br><br><br><br><br>
				<br><br><br><br><br><br>
				<br><br><br><br><br><br>
				
				
					
				</div>
				
			</section>
						
		
		</div>
	</div>
					
						
						

<?php $this->load->view('common/footer.php'); ?>
</script>