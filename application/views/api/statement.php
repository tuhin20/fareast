<?php $this->load->view('common/header.php'); ?>
	<style>
		.bordered{
			border: 1px solid #dddddd;
		}
		.font-size20{
			font-size:20px;
		}
	</style>

	<header class="page-header">
		<h2>Metronet CRM</h2>
	
	</header>
	
	
	<div class='row'>
		<div class="col-md-12">
			<section class="panel" >
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>
			
					<h2 class="panel-title">Dashboard</h2>
				</header>
				<div class="panel-body ">
					<div class="row">
						<div class="col-md-12">
							<form  method="post" action="<?php echo site_url('api/GetStatement');?> ">
							<h3>Please input Policy Number</h3>
							<input type="number" name="policy_no">
							<button type="submit" class="btn btn-primary button-loading">Submit</button>
							</form>
							<div class="table-responsive">
								<table  class="table table-striped table-bordered" id="myTable">

								<thead>

									<tr>						
										<th>POLICY_NO</th>
										<th>PROPOSER</th>
									    <th> SUM_INSURE</th>
										<th>INSTPREM</th>
									    <th> RISKDATE</th>
										<th>MATURITY</th>
									    <th> NEXTPREM</th>
									    <th> STATUS</th>
										<th style="text-align: center">poldata</th>
								    </tr>
								</thead>
								
								  
								
								<tbody>
								  <?php foreach ($polinfo as $value){ ?>

								  <tr>
									<td><?php echo $value['POLICY_NO'];?></td>				
									<td><?php echo $value['PROPOSER'];?></td>
									<td><?php echo $value['SUM_INSURE'];?></td>
									<td><?php echo $value['INSTPREM'];?></td>
									<td><?php echo $value['RISKDATE'];?></td>
									<td><?php echo $value['MATURITY'];?></td>
									<td><?php echo $value['NEXTPREM'];?></td>
									<td><?php echo $value['STATUS'];?></td>
									<td>

	                                    <table class="table table-striped table-bordered">
	                                    	<thead>
		                                        <tr>
		                                         	<th>INSTALNO</th>
		                                         	<th>PR_NO</th>
													<th>PR_DATE</th>
													<th>AMOUNT</th>
		                                        </tr>
	                                        </thead>
	                                        <tbody>
		                                       	<tr>
													<?php foreach ($value['poldata'] as $row){ ?>
														<tr>
															<td><?=$row['INSTALNO'];?></td>
															<td><?=$row['PR_NO'];?></td>
															<td><?=$row['PR_DATE'];?></td>
															<td><?=$row['AMOUNT'];?></td>
														</tr>
													<?php } ?>
		                                       	</tr>
	                                       </tbody>
	                                    </table>
                                                                          
                                    </td>
									</tr>
								  <?php } ?>
								  
								</tbody>
							  </table>
							</div>
							
							
						</div>
						
					</div>
				
				<br><br><br><br><br><br>
				<br><br><br><br><br><br>
				<br><br><br><br><br><br>
				
				
					
				</div>
				
			</section>
						
		
		</div>
	</div>
					
						
						

<?php $this->load->view('common/footer.php'); ?>
</script>