<?php $this->load->view('common/navbar.php'); ?>
<!DOCTYPE html>
<html>


<br/><br/>
<body> 
<style>
.tablink{
	    width: 16%;

}
</style><div>
<div class="container" style="">
<button class="tablink" onclick="openPage('Basic_Info', this, 'green')" id="defaultOpen">Basic Info</button>
<button class="tablink" onclick="openPage('Policy_Statement', this, 'green')" >Policy Statement</button>
<button class="tablink" onclick="openPage('Maturity', this, '')">Maturity</button>
<button class="tablink" onclick="openPage('BEFTN', this, 'green')">BEFTN</button>
<button class="tablink" onclick="openPage('Survival_Benefit', this, 'green')">Survival Benefit</button>
<button class="tablink" onclick="openPage('Death_Claim', this, 'green')">Death Claim</button>

</div>
<br/>
<!-- Basic Info -->

<div id="Basic_Info" class="tabcontent container" onclick="startTab()"; style="height: auto;">
  <div class="container col-md-12" style="padding:80px;">
    <div class=" container col-md-12">
	
	  <div class="col-md-9"style=" padding: 10px !important;">
	     <div class="col-md-12 container"> 
			<div class="col-md-12">
					<div class="col-md-2">
					<label >Name:</label>
					</div>
					
					<div class="col-lg-12" style="padding-right: 0px;" >

  	    			<input class="col-md-4" type="text"  value="<?php echo $basic_info[0]['POLICYHOLDER'];?>"  readonly  >
					</div>
  	    	</div>

			<div class="col-md-12" style="padding: 10px;">
					<div class="col-md-6">
						<label class="col-md-5"  style="text-align: left; padding-left:0px; padding-right: 0px;">Policy Number:</label>
            			<input class="col-md-7" type="text" id="policyno"  value="<?php echo $basic_info[0]['POLICY_NO'];?>" readonly>
					</div>
					<div class="col-md-6">
						<label class="col-md-3" >Mobile:</label>
            			<input class="col-md-8" type="text"  value="<?php echo $basic_info[0]['PHONE'];?>" readonly>
					</div>
			</div>	
		</div>			
	  </div>
	
	    <div class="col-md-3" >
			<label class="col-md-12">User Name</label>
  	    		<input class="col-md-12" type="text" readonly>
	    </div>
	</div>
	
	
	<div class="col-md-12" style=" padding-left: 5px; padding-right: 5px;">
	
		<div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 0px;">
				<h3 style="text-align: center; font-weight: bold; "> Policyholder's Information </h3>
    	<div class="container col-md-12" style="  margin-top: 10px !important; padding-top: 20px; padding-bottom: 20px;">
			
			
		  <div class="col-md-12" style="padding: 0px;">	
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4"  >Salute</label>
				<input class="col-md-7" type="text"  value="<?php echo $basic_info[0]['SALUTE'];?>" readonly>
			</div>
			
			<div class="col-md-6" style="padding: 5px;">
				<label class="col-md-4" >Age(At.Entry )</label>
				<input class="col-md-7" type="text" value="<?php echo $basic_info[0]['AGE'];?>" readonly>
			</div>
		  </div>
		  
		   <div class="col-md-12" style="padding-top: 5px; padding-left: 0px; padding-right:0px;">	
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4">PolicyHolder </label>
				<input class="col-md-7" type="text"value="<?php echo $basic_info[0]['POLICYHOLDER'];?>" readonly>
			</div>
			
			<div class="col-md-6" style="padding: 5px;">
				<label class="col-md-4" >Age Prof.</label>
				<input class="col-md-7" type="text"  value="<?php echo $basic_info[0]['AGE_PROF'];?>" readonly>
			</div>
		  </div>
		  
		  <div class="col-md-12" style="padding-top: 5px; padding-left: 0px; padding-right:0px;">	
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4" >Date of Birth</label>
				<input class="col-md-7" type="text"  value="<?php echo $basic_info[0]['DATE_OF_BIRTH'];?>" readonly>
			</div>
			
			<div class="col-md-6" style="padding: 5px;">
				<label class="col-md-4" >Occupation</label>
				<input class="col-md-7" type="text"  value="<?php echo $basic_info[0]['OCCUPATION'];?>" readonly>
			</div>
		  </div>
		  
		   <div class="col-md-12" style="padding-top: 5px;  padding-left: 0px; padding-right:0px;">	
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4" >Gender</label>
				<input class="col-md-7" type="text" value="<?php echo $basic_info[0]['GENDER'];?>" readonly>
			</div>
		  </div>
		  
		</div>
	</div>
	
   </div>
  
  
  <div class="col-md-12" style=" padding-left: 5px; padding-right: 5px;">
	
	<div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 0px;">
				<h3 style="text-align: center; font-weight: bold; "> Assured/Second Life</h3>
    	<div class="container col-md-12" style="  margin-top: 10px !important; padding-top: 20px; padding-bottom: 20px;">
			
			
		  <div class="col-md-12" style="padding: 0px;">	
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4" >Assure Name</label>
				<input class="col-md-7" type="text" value="<?php echo $basic_info[0]['ASSURED_NAME'];?>" readonly>
			</div>
			
			<div class="col-md-6" style="padding: 5px;">
				<label class="col-md-4" >Age(year)</label>
				<input class="col-md-7" type="text" value="<?php echo $basic_info[0]['AGE_YEAR'];?>" readonly>
			</div>
		  </div>
		
		 
		</div>
	</div>
	
  </div>
  
  
  <div class="col-md-12" style=" padding-left: 5px; padding-right: 5px;">
	
	  <div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 0px;">
				<h3 style="text-align: center; font-weight: bold; "> Policyholder's Address</h3>
    	<div class="container col-md-12" style="  margin-top: 10px !important; padding-top: 20px; padding-bottom: 20px; ">
			
			
		  <div class="col-md-12" style="padding: 0px;">	
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4" >Father's Name</label>
				<input class="col-md-7" type="text" value="<?php echo $basic_info[0]['FATHER'];?>" readonly>
			</div>
			
			<div class="col-md-6" style="padding: 5px;">
				<label class="col-md-4" >Phone</label>
				<input class="col-md-7" type="text" value="<?php echo $basic_info[0]['PHONE'];?>" readonly>
			</div>
		  </div>
		  
		   <div class="col-md-12" style="padding-top: 5px; padding-left: 0px; padding-right:0px;">	
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4" >Mother's Name</label>
				<input class="col-md-7" type="text" value="<?php echo $basic_info[0]['MOTHER_NAME'];?>" readonly>
			</div>
		  </div>
		  
		  <div class="col-md-12" style="padding-top: 5px; padding-left: 0px; padding-right:0px;">	
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4" >Husband's Name</label>
				<input class="col-md-7" type="text" value="<?php echo $basic_info[0]['HUSBAND'];?>" readonly>
			</div>
		  </div>
		  
		   <div class="col-md-12" style="padding-top: 5px;  padding-left: 0px; padding-right:0px;">	
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4" >Permanent Address</label>
				<input class="col-md-7" type="text"  value="">
			</div>
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4" >Post Code</label>
				<input class="col-md-7" type="text" value="<?php echo $basic_info[0]['POST_CODE'];?>" readonly>
			</div>
		  </div>
		  
		</div>
	  </div>
	
   </div>
   
   
     <div class="col-md-12" style=" padding-left: 5px; padding-right: 5px;">
	
	  <div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 0px;">
				<h3 style="text-align: center; font-weight: bold; "> Basic Information</h3>
    	<div class="container col-md-12" style="  margin-top: 10px !important; padding-top: 20px; padding-bottom: 20px; ">
			
			
		  <div class="col-md-12" style="padding: 0px;">	
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4">Table & Term</label>
				<input class="col-md-4" type="text" value="<?php echo $basic_info[0]['TABLE'];?>" style="width: 20.333333%; margin-right:10px; " readonly>
				<input class="col-md-4" type="text"  value="<?php echo $basic_info[0]['TERM'];?>"  style="width: 20.333333%; " readonly>
			</div>
			
			<div class="col-md-6" style="padding: 5px;">
				<label class="col-md-4" >Plan Name</label>
				<input class="col-md-7" type="text" value="<?php echo $basic_info[0]['PLAN_NAME'];?>" readonly>
			</div>
		  </div>
		  
		  
		  
		   <div class="col-md-12" style="padding-top: 10px; padding-left: 0px; padding-right:0px;">	
		   
			<div class="col-md-6" style="padding: 5px; background-color: #f7ffcb; margin: 20px;"> 
			    <h4 style="text-align: center; font-weight: bold; "> Premium Info</h4>
			   
			   <div class="col-md-12" style="padding-bottom: 15px;">
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Risk & Com Date</label>
				<input class="col-md-4" type="text"  value="<?php echo $basic_info[0]['RISK'];?>" style="width:  32.333333%; margin-right:8px; " readonly>
				<input class="col-md-4" type="text"  value="<?php echo $basic_info[0]['COM_DATE'];?>" style="width:  32.333333%; " readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Sum Assured</label>
				<input class="col-md-8" type="text"  value="<?php echo $basic_info[0]['SUM_ASSURED'];?>"readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Sum at Risk</label>
				<input class="col-md-8" type="text" value="<?php echo $basic_info[0]['SUM_AT_RISK'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Pay Mode</label>
				<input class="col-md-8" type="text"  value="<?php echo $basic_info[0]['PAY_MODE'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Policy Option</label>
				<input class="col-md-8" type="text"  value="<?php echo $basic_info[0]['POLICY_OPTION'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Premium Rate</label>
				<input class="col-md-8" type="text" value="<?php echo $basic_info[0]['PREMIUM_RATE'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Policy Status</label>
				<input class="col-md-8" type="text" value="<?php echo $basic_info[0]['POLICY_STATUS'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Medical/Non-Medical</label>
				<input class="col-md-8" type="text" value="<?php echo $basic_info[0]['MEDICAL_OR_NON_MADICAL'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Std/Sub-Std</label>
				<input class="col-md-8" type="text"  value="<?php echo $basic_info[0]['STD_OR_SUB_STD'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Leon Code & Term</label>
				<input class="col-md-8" type="text"  value="<?php echo $basic_info[0]['LEON_CODE_AND_TERM'];?>" readonly>
				</div>
				
			  </div>
			</div>
			
			
			<div class="col-md-5" style="padding: 5px; padding-top:60px !important;"> 
			   
			   <div class="col-md-12" style="padding-bottom: 15px;">
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Life Premium Installment</label>
				<input class="col-md-8" type="text"  value="<?php echo $basic_info[0]['LIFE_PREWMIUM_INSTALMENT'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Ac. Code</label>
				<input class="col-md-8" type="text"  value="<?php echo $basic_info[0]['AC_CODE'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Accidental Premium</label>
				<input class="col-md-8" type="text"  value="<?php echo $basic_info[0]['ACCIDENTAL_PREMIUM'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >OE Rate</label>
				<input class="col-md-8" type="text" value="<?php echo $basic_info[0]['OE_RATE'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >OE Prem.</label>
				<input class="col-md-8" type="text" value="<?php echo $basic_info[0]['OE_PREM'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Total Prem.</label>
				<input class="col-md-8" type="text"  value="<?php echo $basic_info[0]['TOTAL_PREM'];?>" readonly>
				</div>
				
			  </div>
			</div>
			
		  </div>
		  
		  
	
		  <div class="col-md-12" style="padding-top: 10px; padding-left: 0px; padding-right:0px;">
		  	
			<div class="col-md-6" style="padding: 5px; background-color: #f7ffcb; margin: 20px;"> 
			    <h4 style="text-align: center; font-weight: bold; "> Short PR/BM Entry</h4>
			   
			   <div class="col-md-12" style="padding-bottom: 15px;">
				
				<div class="col-md-4" style="padding:5px;">
				<label class="col-md-12" >PR No.</label>
				<input class="col-md-12" type="text" readonly  >
				</div>
				<div class="col-md-4" style="padding:5px;">
				<label class="col-md-12" >PR Date</label>
				<input class="col-md-12" type="text" readonly >
				</div>
				<div class="col-md-4" style="padding:5px;">
				<label class="col-md-12" >Amount</label>
				<input class="col-md-12" type="text" readonly  >
				</div>
			  </div>
			</div>
			
			<div class="col-md-5" style="padding: 5px; padding-top:20px !important;"> 
			   
			   <div class="col-md-12" style="padding-bottom: 15px;">
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Last Paid Date</label>
				<input class="col-md-8" type="text"  value="<?php echo $basic_info[0]['LAST_PAID_DATE'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Next Due Date</label>
				<input class="col-md-8" type="text"   value="<?php echo $basic_info[0]['NEXT_DUE_DATE'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Suspense</label>
				<input class="col-md-8" type="text"  value="<?php echo $basic_info[0]['SUSPENSE'];?>" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Maturity Date</label>
				<input class="col-md-8" type="text"  value="<?php echo $basic_info[0]['MATURITY_DATE'];?>" readonly>
				</div>
				
			  </div>
			</div>
			
		  </div>
		  
		  
		</div>
	</div>
	
   </div>
  
  
  </div>
</div>




<!-- Policy_Statement -->



<div id="Policy_Statement" class="tabcontent container" style="height: auto;">
	
	<div class="container col-md-12" style="padding:80px;" >

	  <div class="container col-md-12"> 
		
		<div class="col-md-12" style="padding: 0px;">	
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4" >Policy Number</label>
				<input class="col-md-7" type="text"   value="<?php echo $polinfo[0]['POLICY_NUMBER'];?>" readonly>
			</div>
			
			<div class="col-md-6" style="padding: 5px;">
				<label class="col-md-4" >Risk Date</label>
				<input class="col-md-7" type="text" value="<?php echo $polinfo[0]['RISK_DATE'];?>" readonly>
			</div>
		 </div>
		 
		 <div class="col-md-12" style="padding: 0px;">	
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4" >Sum Assured</label>
				<input class="col-md-7" type="text"  value="<?php echo $polinfo[0]['SUM_ASSURED'];?>" readonly>
			</div>
			
			<div class="col-md-6" style="padding: 5px;">
				<label class="col-md-4" >Maturity</label>
				<input class="col-md-7" type="text"  value="<?php echo $polinfo[0]['MATURITY'];?>" readonly>
			</div>
		 </div>
		
		<div class="col-md-12" style="padding: 0px;">	
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4" >Ins. Prem.</label>
				<input class="col-md-7" type="text"   value="<?php echo $polinfo[0]['INST_PREM'];?>" readonly>
			</div>
			
			<div class="col-md-6" style="padding: 5px;">
				<label class="col-md-4" >Next Permanent</label>
				<input class="col-md-7" type="text"  value="<?php echo $polinfo[0]['NEXT_PREMENT'];?>"readonly>
			</div>
		 </div>
		 
		 <div class="col-md-12" style="padding: 0px;">	
			<div class="col-md-6" style="padding: 5px;"> 
				<label class="col-md-4" >Inst. Mode</label>
				<input style="color: red; font: italic;" class="col-md-7" type="text"  value="<?php if($polinfo[0]['INST_MODE']=='1'){
               echo "Yearly";}
               else if ($polinfo[0]['INST_MODE']=='2')
               echo  "H-Yearly";
               else if ($polinfo[0]['INST_MODE']=='3')
               echo  "Quarterly";
               else if ($polinfo[0]['INST_MODE']=='4')
               echo  "Single";
               else if ($polinfo[0]['INST_MODE']=='5')
               echo  "Monthly";
					?>" readonly>
			</div>
			
			<div class="col-md-6" style="padding: 5px;">
				<label class="col-md-4" >Status</label>
				<input style="color: red"; class="col-md-7" type="text"  value="<?php

/* Incorrect Method: */
if ($polinfo[0]['STATUS']=='1') {
	echo "Inforce";
}
else if ($polinfo[0]['STATUS']=='2')
    echo  "Special revive";
 else if ($polinfo[0]['STATUS']=='3') 
    echo  "Ordinary revive";

  else if ($polinfo[0]['STATUS']=='4')
    echo  "Survival Benefit";
 else if ($polinfo[0]['STATUS']=='5')
    echo  "Paidup";  
    else if ($polinfo[0]['STATUS']=='6')
    echo  "Death Claim"; 
 else if ($polinfo[0]['STATUS']=='7')
    echo  "Suspended";
    else if ($polinfo[0]['STATUS']=='8')
    echo  "Death Claim"; 
    else if ($polinfo[0]['STATUS']=='9')
    echo  "Surrender"; 
    else if ($polinfo[0]['STATUS']=='10')
    echo  "Refund";
     else if ($polinfo[0]['STATUS']=='11')
    echo  "Transfer";
    else if ($polinfo[0]['STATUS']=='12')
    echo  "Matured";


?>" readonly>
			</div>
		 </div>
		
		
	  </div>
	  

	  <div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding: 0px;">
		
		<table id="customers">
		<thead>
			<tr>
				<th>Inst.No.</th>
				<th>PR. No.</th>
				<th>PR. Date</th>
				<th>Prem. Amount</th>
			</tr>
		</thead>
			 <tbody>
		                                       	<tr>
												
												 <?php  foreach ($polinfo as $value){ ?>
												
													<?php foreach ($value['poldata'] as $row){ ?>
													
														<tr>
															<td><?=$row['INSTALNO'];?></td>
															<td><?=$row['PR_NO'];?></td>
															<td><?=$row['PR_DATE'];?></td>
															<td><?=$row['PREM_AMOUNT'];?></td>
														</tr>
														<?php $PREM_AMOUNT+=$row['PREM_AMOUNT'];?>
													<?php } ?>
												
		                                       	</tr>
												
	                                       </tbody>
										   <tr style="color:red; font-weight: bold"><td></td><td></td><td>Total Prem Amount:</td><td> <?php echo $PREM_AMOUNT; }?></td></tr>
		</table>
		
		
	  </div>
		
	</div>
    
</div>





<!-- Maturity -->

<div id="Maturity" class="tabcontent container" style="height: auto;">
    
	
	<div class="col-md-12" >
	
		<div class=" col-md-4" >
		  <div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 15px; padding-bottom: 15px;"> 
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Policy No. </label>
				<input class="col-md-7" type="text"  value="<?php echo $maturity[0]['POLICY_NO'];?>" readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Proposer Name </label>
				<input class="col-md-7" type="text"  value="<?php echo $maturity[0]['PROPOSER_NAME'];?>" readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >SC Code </label>
				<input class="col-md-7" type="text"  value="<?php echo $maturity[0]['S_C_CODE'];?>" readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >SC Name</label>
				<input class="col-md-7" type="text"   value="<?php echo $maturity[0]['S_C_NAME'];?>" readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Table ID </label>
				<input class="col-md-7" type="text"  value="<?php echo $maturity[0]['TABLE_ID'];?>"readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Term </label>
				<input class="col-md-7" type="text"  value="<?php echo $maturity[0]['TERM'];?>"readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Table Name</label>
				<input class="col-md-7" type="text" value="<?php echo $maturity[0]['TABLE_NAME'];?>" readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Maturity Date</label>
				<input class="col-md-7" type="text"  value="<?php echo $maturity[0]['MATURITY_DATE'];?>"readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Maturity Status </label>
				<input class="col-md-7" type="text"  value="<?php echo $maturity[0]['MATURITY_STATUS'];?>" readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Entry Date </label>
				<input class="col-md-7" type="text" value="<?php echo $maturity[0]['ENTRY_DATE'];?>" readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Final Status</label>
				<input class="col-md-7" type="text" value="<?php echo $maturity[0]['FINAL_STATUS'];?>" readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >BFTN Status</label>
				<input class="col-md-7" type="text" value="<?php echo $maturity[0]['BEFTN_STATUS'];?>" readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Lot No.</label>
				<input class="col-md-7" type="text"  value="<?php echo $maturity[0]['LOT_NO'];?>"readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Lot Close Status</label>
				<input class="col-md-7" type="text" value="<?php echo $maturity[0]['LOT_CLOSE_STATUS'];?>" readonly>
			</div>
		  </div>
		</div>
		
		
		<div class=" col-md-8">
		 
		 <div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 15px; padding-bottom: 30px;"> 
			
			<h3 style="text-align: center; font-weight: bold; "> Maturity Value</h3>
		   
		   <div class="col-md-12">
		   
		    <div class="col-md-6" style="margin-top:15px"> 
			
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Sum Assured</label>
				<input class="col-md-7" type="text" value="<?php echo $maturity[0]['SUM_ASSURED'];?>" readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Raidup/Rem. SA</label>
				<input class="col-md-7" type="text"  value=""readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >5% C. Bonus</label>
				<input class="col-md-7" type="text"  value="<?php echo $maturity[0]['5C_BONUS'];?>"readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Rev. Bonus</label>
				<input class="col-md-7" type="text"  value="<?php echo $maturity[0]['RAV_BONUS'];?>"readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Terminal Bonus</label>
				<input class="col-md-7" type="text"  value="<?php echo $maturity[0]['TERMINAL_BONUS'];?>"readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Total Bonus</label>
				<input class="col-md-7" type="text"  value="<?php echo $maturity[0]['TOTAL_BONUS'];?>"readonly>
			</div>
			
		  </div>
			
			
			
		  <div class="col-md-6" style="margin-top:15px"> 
			
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Total Payable</label>
				<input class="col-md-7" type="text"  value="<?php echo $maturity[0]['TOTAL_PAYABLE'];?>"readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Total Deposit</label>
				<input class="col-md-7" type="text" value="<?php echo $maturity[0]['TOTAL_DEPOSIT'];?>" readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Taxable Amount</label>
				<input class="col-md-7" type="text" value="<?php echo $maturity[0]['TAXABLE_AMOUNT'];?>" readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Tax Amount</label>
				<input class="col-md-7" type="text" value="<?php echo $maturity[0]['TAX_AMOUNT'];?>"readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Suspense</label>
				<input class="col-md-7" type="text"value="<?php echo $maturity[0]['SUSPENSE'];?>" readonly>
			</div>
			<div class="col-md-12" style="padding: 5px;"> 
				<label class="col-md-5" >Total Amount</label>
				<input class="col-md-7" type="text" value="<?php echo $maturity[0]['TOTAL_AMOUNT'];?>" readonly>
			</div>
	
		  </div>
			
			
		  </div>	
		  
		  
		  <div class="col-md-12" style="padding-top: 40px;">
				
			<div class="col-md-3">
			    <h5 style="text-align: center; font-weight: bold; "> New Policy </h5>
				<input class="col-md-12" style="margin:3px;" type="text" readonly>	
				<input class="col-md-12" style="margin:3px;" type="text" readonly>	
				<input class="col-md-12" style="margin:3px;" type="text" readonly>	
			
			</div>
			
			<div class="col-md-9" style="padding-top: 20px;" >
				
				<div class="col-md-12">
				<label class="col-md-4" style="text-align: right; margin-top:10px;">Prof. No.01</label>
				<input class="col-md-4" type="number"  style="width: 29.333333%; margin-right:10px; margin-top:10px;" value="<?php echo $maturity[0]['PROF_NO_01'];?>"readonly>
				<input class="col-md-4" type="text" style="width: 29.333333%;margin-top:10px; " value=""readonly >
				</div>
				<div class="col-md-12">
				<label class="col-md-4"  style="text-align: right; margin-top:10px;">Prof. No.02</label>
				<input class="col-md-4" type="text"  style="width: 29.333333%; margin-right:10px;margin-top:10px; " value="<?php echo $maturity[0]['PROF_NO_02'];?>"readonly >
				<input class="col-md-4" type="text" style="width: 29.333333%;margin-top:10px;"value="" readonly >
				</div>
				<div class="col-md-12">
				<label class="col-md-4"  style="text-align: right; margin-top:10px;">Prof. No.03</label>
				<input class="col-md-4" type="text"  style="width: 29.333333%; margin-right:10px; margin-top:10px;" value="<?php echo $maturity[0]['PROF_NO_03'];?>"readonly >
				<input class="col-md-4" type="text"  style="width: 29.333333%; margin-top:10px;" readonly >
				</div>
				
			</div>
			
			</div>
			
		  </div>
		  
		 
		 
		 
		 <div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 15px; padding-bottom: 30px;"> 
		 
			<h3 style="text-align: center; font-weight: bold; "> Adjustment</h3>
			
			<div class="col-md-12">
				<label class="col-md-2"  style="text-align: center; margin-top:10px;">Policy NO</label>
				<input class="col-md-2" type="text"  style="width: 10.333333%; margin-right:10px; margin-top:10px;" value="<?php echo $maturity[0]['POLICY_NO_ADJ'];?>"readonly >
				<label class="col-md-2"  style="text-align: center; margin-top:10px;">PH No</label>
				<input class="col-md-2" type="text"  style="width: 17.333333%; margin-top:10px;"value="<?php echo $maturity[0]['PH_NO_ADJ'];?>" readonly >
				<label class="col-md-2"  style="text-align: center; margin-top:10px;">Amount</label>
				<input class="col-md-2" type="text"  style="width: 18.333333%; margin-top:10px;"value="<?php echo $maturity[0]['AMOUNT_ADJ'];?>" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-2"  style="text-align: center; margin-top:10px;">Policy NO ADJ1</label>
				<input class="col-md-2" type="text" style="width: 10.333333%; margin-right:10px; margin-top:10px;" value="<?php echo $maturity[0]['POLICY_NO_ADJ1'];?>"readonly >
				<label class="col-md-2"  style="text-align: center; margin-top:10px;">PH No ADJ1</label>
				<input class="col-md-2" type="text" style="width: 17.333333%; margin-top:10px;"value="<?php echo $maturity[0]['PH_NO_ADJ1'];?>"  readonly >
				<label class="col-md-2"  style="text-align: center; margin-top:10px;">Amount ADJ1</label>
				<input class="col-md-2" type="text"  style="width: 18.333333%; margin-top:10px;"value="<?php echo $maturity[0]['AMOUNT_ADJ1'];?>" readonly >
			</div>
		
			
		 </div>
		 
		 <div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 15px; padding-bottom: 30px;"> 
		 
			<h3 style="text-align: center; font-weight: bold; "> BFTN Bank/Branch and Account Information</h3>
			
			<div class="col-md-12">
				<label class="col-md-3"  style="text-align: center; margin-top:10px;">Bank Name</label>
				<input class="col-md-8" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $maturity[0]['BANK_NAME'];?>"readonly >
				
			</div>
			<div class="col-md-12">
				<label class="col-md-3" style="text-align: center; margin-top:10px;">Branch Code & Name</label>
				<input class="col-md-4" type="text" style="width: 31.333333%; margin-right:15px; margin-top:10px;"value="<?php echo $maturity[0]['BRANCH_CODE_NAME'];?>" readonly >
				<input class="col-md-4" type="text" style=" margin-top:10px;"  value="<?php echo $maturity[0]['BRANCH_CODE'];?>" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-3"  style="text-align: center; margin-top:10px;">Routing Number</label>
				<input class="col-md-8" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $maturity[0]['ROUTING_NUMBER'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-3"  style="text-align: center; margin-top:10px;">Account Number</label>
				<input class="col-md-3" type="text"  style="width: 29.333333%; margin-top:10px;" value="<?php echo $maturity[0]['ACCOUNT_NUMBER'];?>"readonly >
				<label class="col-md-2"  style="text-align: right; margin-top:10px;">Mob. No.</label>
				<input class="col-md-3" type="text" style="width: 20.333333%; margin-top:10px;"value="<?php echo $maturity[0]['MOB_NO'];?>" readonly >
			</div>
			
		 </div>
		 
		</div>
		
	</div>
	   
</div>
	
	
	
	






<!-- BFTN -->
<div id="BEFTN" class="tabcontent container" style="height: auto;">

	<div class=""><form  method="post" align="center" action="<?php echo site_url('api/dropdowntest');?> ">
<select id='installno' name="installno">
	<label for="cars">Choose</label>
    <option value="">--Select--</option>
    <option value="M">Maturity</option>
    <option value="1ST">First SB</option>
    <option value="2ND">Second SB</option>
    <option value="3RD">Third SB</option>
    <option value="4TH">Fourth SB</option>
    
  </select>
 
		</form></div>
  <div class="col-md-12">
	
	<div class="col-md-6" style="  margin-top: 55px;"> 
    
		<div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 15px; padding-bottom: 30px;"> 
		 
			<h3 style="text-align: center; font-weight: bold; "> Basic Information</h3>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Policy No.</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="" readonly > 
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Proposer</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;"value="" readonly >
			</div>
		</div>
		
		<div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 15px; padding-bottom: 30px;"> 
		 
			<h3 style="text-align: center; font-weight: bold; "> Bank Information</h3>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Bank Name</label>
				<input class="col-md-7" id="bankName1" type="text"style="margin-right:10px; margin-top:10px;" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Branch Name</label>
				<input class="col-md-7" id="branchName" type="text" style="margin-right:10px; margin-top:10px;"value="" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Route No.</label>
				<input class="col-md-7" id="routeName" type="text"  style="margin-right:10px; margin-top:10px;" value=""readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Acc No.</label>
				<input class="col-md-7" id="accountNO" type="text" style="margin-right:10px; margin-top:10px;" value=""readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Pay Status</label>
				<input class="col-md-7" id="#paystatus" type="text" style="margin-right:10px; margin-top:10px;"value="" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Bank Posting Date</label>
				<input class="col-md-7" id="bankpostingDate" type="text" style="margin-right:10px; margin-top:10px;"value="" readonly >
			</div>
		</div>	
	
	</div>
	
	<div class="col-md-6" style="  margin-top: 55px;"> 
    
		<div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 15px; padding-bottom: 30px;"> 
		 
			<h3 style="text-align: center; font-weight: bold; "> Other Information</h3>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Payment Info</label>
				<input class="col-md-7" id="paymentInfo" type="text"  style="margin-right:10px; margin-top:10px;" value=""readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Lot No.</label>
				<input class="col-md-7" id="lotno" type="text" style="margin-right:10px; margin-top:10px;" value=""readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Lot Date</label>
				<input class="col-md-7" id="lotdate" type="text"  style="margin-right:10px; margin-top:10px;"value="" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Batch No.</label>
				<input class="col-md-7" id="batchNo" type="text" style="margin-right:10px; margin-top:10px;"value="" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Batch Date</label>
				<input class="col-md-7" id="batchDate" type="text" style="margin-right:10px; margin-top:10px;" value=""readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Amount</label>
				<input class="col-md-7" id="amount" type="text" style="margin-right:10px; margin-top:10px;" value=""readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Fund No.</label>
				<input class="col-md-7" id="foundNo" type="text"  style="margin-right:10px; margin-top:10px;"value="" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Fund Create Date</label>
				<input class="col-md-7"id="fundCreateDate" type="text" style="margin-right:10px; margin-top:10px;" value=""readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Pay to</label>
				<input class="col-md-7" id="payTo_"   style="margin-right:10px; margin-top:10px;"value="" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Return Batch Date</label>
				<input class="col-md-7"id="returnBatchDate" type="text" style="margin-right:10px; margin-top:10px;"value="" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Install No.</label>
				<input class="col-md-7" id="installNo" type="text"  style="margin-right:10px; margin-top:10px;"value="" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">BFTN</label>
				<input class="col-md-7" id="beftn_" type="text"  style="margin-right:10px; margin-top:10px;"  readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Rrf No.</label>
				<input class="col-md-7" id="refNo" type="text"  style="margin-right:10px; margin-top:10px;"value="" readonly >
			</div>
		</div>	
	
	</div>
	
	
  </div>
  
</div>





<!-- Survival_Benefit -->
<div id="Survival_Benefit" class="tabcontent container" style="height: auto;">

    <div><form  method="post" align="center" action="<?php //echo site_url('api/dropdowntest');?> ">
  <select id='installno1' name="installno">
	<label for="test">Choose</label>
    <option value="">--Select--</option>
    <option value="1ST">1ST</option>
    <option value="2ND">2ND</option>
    <option value="3RD">3RD</option>
    <option value="4TH">4TH</option>
  </select>
 
		</form></div>
    <div class="col-md-12" style="padding:80px;">
	
	
	   <div class=" container col-md-12">
	
	    <div class="col-md-9"style=" padding: 10px !important;">
	     <div class="col-md-12 container"> 
			<div class="col-md-12">
					<div class="col-md-2">
					<label >Name:</label>
					</div>
					
					<div class="col-md-8" style="padding-left: 31px;">
  	    			<input  type="text" readonly >
					</div>
  	    	</div>

			<div class="col-md-12" style="padding: 10px;">
					<div class="col-md-6">
						<label class="col-md-5"  style="text-align: left; padding-left:0px; padding-right: 0px;">Policy Number:</label>
            			<input class="col-md-7" type="text" id="policyno" readonly>
					</div>
					<div class="col-md-6">
						<label class="col-md-3" >Mobile:</label>
            			<input class="col-md-8" type="text"  value="<?php echo $survival_banifit[0]['PHONE_NUMBER'];?>" readonly>
					</div>
			</div>	
		 </div>			
	    </div>
	
	    <div class="col-md-3" >
			<label class="col-md-12">User Name</label>
  	    		<input class="col-md-12" type="text" readonly>
	    </div>
		
	   </div>
	  
	  
	  <div class="col-md-12" style=" padding-left: 5px; padding-right: 5px;">
	
	    <div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 0px; padding-bottom: 25px;">
    	
				
		 <div class="col-md-12" style="padding-top: 10px; padding-left: 0px; padding-right:0px;">	
		   
			<div class="col-md-6" style="padding: 5px;"> 
			   
			   <div class="col-md-12" style="padding-bottom: 15px;">
			   
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >SC Code</label>
				<input class="col-md-8" id="scCode" type="text"  readonly  >
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >SC Name</label>
				<input class="col-md-8" id="scNameSB" type="text"  readonly  >
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Proposer</label>
				<input class="col-md-8" id="proposer" type="text" value="" readonly >
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" for="fname">Risk Date</label>
				<input class="col-md-8" id="riskdate" type="text" value="" readonly>
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Pre. Inst. Paid</label>
				<input class="col-md-3" id="preinspaid" type="text"  value="" readonly >
				<label class="col-md-2" >SB Due</label>
				<input class="col-md-3" id="sbdue" type="text" value="" readonly >
				</div>
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Last Paid</label>
				<input class="col-md-3" type="text" id="last_paid" value="" readonly >
				<label class="col-md-2" >Status</label>
				<input class="col-md-3" id="status" type="text"  value="" readonly >
				</div>
				
				
			  </div>
			  
			  <div class="col-md-12" style="padding: 5px; background-color: #f7ffcb; margin: 20px;"> 
			    <h4 style="text-align: center; font-weight: bold; ">PR/New Policy Amount</h4>
			   
			   <div class="col-md-12" style="padding-bottom: 15px;">
				
				<label class="col-md-4" >PR No.1</label>
				<input class="col-md-4" type="text" style="width: 29.333333%; margin-right:10px;" id="pr_no_1" readonly >
				<input class="col-md-4" type="text" style="width: 29.333333%;"value="" readonly >
				
				</div>
				
			   <div class="col-md-12" style="padding-bottom: 15px;">
				
				<label class="col-md-4" >PR No.2</label>
				<input class="col-md-4" type="text" style="width: 29.333333%; margin-right:10px;" id="pr_no_2" readonly >
				<input class="col-md-4" type="text" style="width: 29.333333%;"value="" readonly >
				
				</div>
				
				<div class="col-md-12" style="padding-bottom: 15px;">
				
				<label class="col-md-4" >PR No.3</label>
				<input class="col-md-4" type="text" style="width: 29.333333%; margin-right:10px;" id="pr_no_3" readonly >
				<input class="col-md-4" type="text" style="width: 29.333333%;" value="" readonly >
				
				</div>
				
			   </div>
			
			</div>
			 
		  
			
			<div class="col-md-5" style="padding: 5px; padding-top:20px;"> 
			   
			  <div class="col-md-12" style="padding-bottom: 15px;">
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Sum Ensured</label>
				<input class="col-md-8" type="text" id="sum_ensured" readonly >
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Some at Risk</label>
				<input class="col-md-8" type="text" id="some_at_risk" readonly >
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Table ID</label>
				<input class="col-md-8" type="text" id="table_id" readonly >
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Term</label>
				<input class="col-md-8" type="text" id="term" readonly >
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >SB Rate(%)</label>
				<input class="col-md-8" type="text" id="sb_rate" value=""readonly  >
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >SB Amount</label>
				<input class="col-md-8" type="text" id="sb_amount" readonly >
				</div>
				
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Issue Date</label>
				<input class="col-md-8" type="text" id="issue_date" readonly >
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Lot/Sheet No.</label>
				<input class="col-md-8" type="text" id="lot_no" readonly >
				</div>
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Lot Close Status</label>
				<input class="col-md-8" type="text" id="lot_close_status" readonly >
				</div>
				
			  
			   </div>
		    </div>
		   
		 </div>
		 
		 <div class="col-md-12" style="padding:5px;">
				<label class="col-md-1" >Adjust Amt.</label>
				<input class="col-md-2"  id="adjust_amount" readonly >
				<label class="col-md-1" >Investment Amt.</label>
				<input class="col-md-2"  id="investment_amt" readonly >
				<label class="col-md-1" >Invest No.</label>
				<input class="col-md-1" type="text" id="invest_no" readonly >
				<label class="col-md-2" >Check Amt.</label>
				<input class="col-md-2" type="text" id="check_amt" readonly  >
		</div>
		
		</div>	
		
		
		
		<div class="col-md-7" style="padding: 5px; background-color: white; margin-top:35px; padding-top:20px;"> 
			   
			   <div class="col-md-12" style="padding-bottom: 15px;">
				
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" for="fname">Bank Name</label>
				<input class="col-md-7" type="text" id="bank_name_sb" readonly   >
				</div>
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" for="fname">Branch Code & Name</label>
				<input class="col-md-7" type="text" id="branch_code_name" readonly   >
				</div>
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4"> Routing No.</label>
				<input class="col-md-7" type="text" style="width: 28.333333%" id="routing_number" readonly >
				</div>
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Account No.</label>
				<input class="col-md-9" type="text" style="width: 29.333333%" id="account_no" readonly  >
				</div>
				<div class="col-md-12" style="padding:5px;">
				<label class="col-md-4" >Phone No.</label>
				<input class="col-md-7" type="text" style="width: 28.333333%" id="phone_no" readonly >
				</div>
				
			  </div>
		</div>
			
		
	  </div>
	
    </div>
	  
	
</div>
	  
<!-- Death_Claim-->
<div id="Death_Claim" class="tabcontent container" style="height: auto;">
    
  <div class="col-md-12" style="padding:80px;">
	
	 <div class=" container col-md-12">
	
	  <div class="col-md-9"style=" padding: 10px !important;">
	     <div class="col-md-12 container"> 
			<div class="col-md-12">
					<div class="col-md-2">
					<label >Name:</label>
					</div>
					
					<div class="col-md-8" style="padding-left: 31px;">
  	    			<input  "type="text" >
					</div>
  	    	</div>

			<div class="col-md-12" style="padding: 10px;">
					<div class="col-md-6">
						<label class="col-md-5"  style="text-align: left; padding-left:0px; padding-right: 0px;">Policy Number:</label>
            			<input class="col-md-7" type="text"  value="<?php echo $basic_info[0]['POLICY_NO'];?>" readonly>
					</div>
					<div class="col-md-6">
						<label class="col-md-3" >Mobile:</label>
            			<input class="col-md-8" type="text"  value="<?php echo $basic_info[0]['PHONE'];?>" readonly>
					</div>
			</div>	
		</div>			
	  </div>
	
	    <div class="col-md-3" >
			<label class="col-md-12">User Name</label>
  	    		<input class="col-md-12" type="text" readonly>
	    </div>
	</div>
	
	
	<div class="col-md-6" style="  margin-top: 35px;"> 
		
		<div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 15px; padding-bottom: 30px;"> 	
			
					
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Claim No.</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['CLAIM_NO'];?>" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Information Date</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['INFORMATION_DATE'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Policy No.</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;"value="<?php echo $death_claim[0]['POLICY_NO'];?>" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Proposal No.</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['PROPOSAL_NO'];?>" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Father's Name</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;"value="<?php echo $death_claim[0]['FATHERS_NAME'];?>" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Mother's Name</label>
				<input class="col-md-7" type="text"  style="margin-right:10px; margin-top:10px;"value="<?php echo $death_claim[0]['MOTHERS_NAME'];?>" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Husband Name</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;"value="<?php echo $death_claim[0]['HUSBAND_NAME'];?>" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Address</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['ADDRESS1'];?>"readonly >
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px; margin-left: 185px;" value="<?php echo $death_claim[0]['ADDRESS2'];?>"readonly >
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px; margin-left: 185px;" value="<?php echo $death_claim[0]['ADDRESS3'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">District</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['DISTRICT'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Phone No.</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['PHONE_NO'];?>" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Occupation</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['OCCUPATION'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Option</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['OPTION'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Table</label>
				<input class="col-md-3" type="text" style="margin-right:10px; margin-top:10px; width: 20%;" value="<?php echo $death_claim[0]['TABLE'];?>"readonly >
				<label class="col-md-2" style="text-align: center; margin-top:10px;">Term</label>
				<input class="col-md-3" type="text" style="margin-right:10px; margin-top:10px; width: 19.5%;" value="<?php echo $death_claim[0]['TERM'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Paymode</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['PAYMODE'];?>"readonly >
			</div>
			
			
		</div>
	
	</div>
	
	
		<div class="col-md-6" style="  margin-top: 35px;"> 
		
		<div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 15px; padding-bottom: 30px;"> 	
			
					
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Claim Type</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['CLAIM_TYPE'];?>" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" 	 style="text-align: center; margin-top:10px;">Policy Status</label>
				<input class="col-md-7" type="text"  style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['POLICY_STATUS'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Supplementary Cont.</label>
				<input class="col-md-7" type="text"  style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['SUPPLEMENTARY_CONT'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Claim Paper Rcv.</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['CLAIM_PAPER_REV'];?>" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Date of Birth</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['DATE_OF_BIRTH'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Age at Risk</label>
				<input class="col-md-3" type="text" style="margin-right:10px; margin-top:10px; width: 20%;" value="<?php echo $death_claim[0]['AGE_AT_RISK'];?>" >
				<label class="col-md-2" style="text-align: center; margin-top:10px;">Sex</label>
				<input class="col-md-3" type="text" style="margin-right:10px; margin-top:10px; width: 19.5%;" value="<?php echo $death_claim[0]['SEX'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Risk Date</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['RISK_DATE'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Maturity Date</label>
				<input class="col-md-7" type="text"  style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['MATURITY_DATE'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Date of Death</label>
				<input class="col-md-7" type="text"  style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['DATE_OF_DEATH'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Place of Death</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['PLACE_OF_DEATH'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Duration of Illness</label>
				<input class="col-md-7" type="text"  style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['DURATION_OF_ILLNESS'];?>"readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Age of Death</label>
				<input class="col-md-7" type="text"  style="margin-right:10px; margin-top:10px;"value="<?php echo $death_claim[0]['AGE_OF_DEATH'];?>" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4" style="text-align: center; margin-top:10px;">Period Death from Risk</label>
				<input class="col-md-7" type="text"  style="margin-right:10px; margin-top:10px;"value="" readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Evidence for Age Proof</label>
				<input class="col-md-7" type="text"  style="margin-right:10px; margin-top:10px;" value="<?php echo $death_claim[0]['EVIDENCE_FOR_AGE_PROOF'];?>"readonly >
			</div>
			
			
		</div>
		
		 <div class="col-md-12" style=" background-color: white !important; margin-top: 55px; padding-top: 15px; padding-bottom: 30px;"> 
		 
			<h3 style="text-align: center; font-weight: bold; "> Cause of Death</h3>
			
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Primary</label>
				<input class="col-md-7" type="text" style=" margin-right:10px; margin-top:10px;" value=""readonly >
			</div>
			<div class="col-md-12">
				<label class="col-md-4"  style="text-align: center; margin-top:10px;">Secondary</label>
				<input class="col-md-7" type="text" style="margin-right:10px; margin-top:10px;"value=""readonly >
				
			</div>
		
			
		 </div>
	
	</div>
	
  </div>

	
</div>

</div>
<script>
    function openPage(pageName,elmnt,color) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].style.backgroundColor = "";
        }
        document.getElementById(pageName).style.display = "block";
        elmnt.style.backgroundColor = color;
    }
	
	

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>
<script type="text/javascript">
	$('#installno1').change(function() {

		var installno = $(this).val();
		var policyno = $('#policyno').val();
    $.ajax({
        url: '<?php echo site_url('api/get_survivalinfo'); ?>',
        type: 'POST',
        data: { //data pass to controller
            installno: installno,
            policyno: policyno,
        },

        dataType: 'json',
        success: function(data) {
        		console.log(data);
          if (data == 'EMPTY')  {

        	$('#scCode').val();
        	$('#scNameSB').val('');
        	$('#proposer').val();
        	$('#riskdate').val();
        	$('#preinspaid').val();
        	$('#sbdue').val();
        	$('#last_paid').val();
        	$('#status').val();
        	$('#pr_no_1').val();
        	$('#pr_no_2').val();
        	$('#pr_no_3').val();
        	$('#sum_ensured').val();
        	$('#some_at_risk').val();
        	$('#table_id').val();
        	$('#term').val();
        	$('#sb_rate').val();
        	$('#sb_amount').val();
        	$('#issue_date').val();
        	$('#lot_no').val();
        	$('#lot_close_status').val();
        	$('#adjust_amount').val();
        	$('#investment_amt').val();
        	$('#invest_no').val();
        	$('#check_amt').val();
        	$('#bank_name_sb').val();
        	$('#branch_code_name').val();
        	$('#routing_number').val();
        	$('#account_no').val();
        	$('#phone_no').val();
      }else
      
           	$('#scNameSB').val(data[0].SC_NAME);
        	$('#scCode').val(data[0].SC_CODE);
        	$('#proposer').val(data[0].PROPOSER);
        	$('#riskdate').val(data[0].RISK_DATE);
        	$('#preinspaid').val(data[0].PREM_INST_PAID);
        	$('#sbdue').val(data[0].SB_DUE);
        	$('#last_paid').val(data[0].LAST_PAID);
        	$('#status').val(data[0].STATUS);
        	$('#pr_no_1').val(data[0].PROP_NO_1);
        	$('#pr_no_2').val(data[0].PROP_NO_2);
        	$('#pr_no_3').val(data[0].PROP_NO_3);
        	$('#sum_ensured').val(data[0].SUM_ENSURED);
        	$('#some_at_risk').val(data[0].SUM_AT_RISK);
        	$('#table_id').val(data[0].TABLE_ID);
        	$('#term').val(data[0].TERM);
        	$('#sb_rate').val(data[0].SB_RATE);
        	$('#sb_amount').val(data[0].SB_AMOUNT);
        	$('#issue_date').val(data[0].ISSUE_DATE);
        	$('#lot_no').val(data[0].LOT_SGEET_NO);
        	$('#lot_close_status').val(data[0].LOT_CLS_ST);
        	$('#adjust_amount').val(data[0].ADJUST_AMT);
        	$('#investment_amt').val(data[0].INVESTMENT_AMT);
        	$('#invest_no').val(data[0].INVEST_NO);
        	$('#check_amt').val(data[0].CHECK_AMOUNT);
        	$('#bank_name_sb').val(data[0].BANK_NAME);
        	$('#branch_code_name').val(data[0].BRANCH_CODE_NAME);
        	$('#routing_number').val(data[0].ROUTING_NUMBER);
        	$('#account_no').val(data[0].ACCOUNT_NUMBER);
        	$('#phone_no').val(data[0].PHONE_NUMBER);
           // console.log(data);
    
        
}


    });
    //return false;
});
</script>
<script type="text/javascript">
	$('#installno').change(function() {

		var installno = $(this).val();
		var policyno = $('#policyno').val();
    $.ajax({
        url: '<?php echo site_url('api/get_bftninfo'); ?>',
        type: 'POST',
        data: { //data pass to controller
            installno: installno,
            policyno: policyno,
        },

        dataType: 'json',
        success: function(data) {
        		console.log(data);
          if (data == 'EMPTY')  {
        	$('#bankName1').val(' ');
        	$('#branchName').val();
        	$('#routeName').val();
        	$('#accountNO').val();
        	$('#paystatus').val();
        	$('#bankpostingDate').val();
        //	$('#paymentInfo').val(data[0].ROUT_NO);
        	$('#lotno').val();
        	$('#lotdate').val();
        	$('#batchNo').val();
        	$('#batchDate').val();
        	$('#amount').val();
        	$('#foundNo').val();
        	$('#fundCreateDate').val();
        	$('#returnBatchDate').val();
        	$('#installNo').val();
        	$('#beftn_').val();
        	$('#refNo').val();
        	$('#payTo_').val('');
           // console.log(data);
      }else
            $('#bankName1').val(data[0].BANK_NAME);
        	$('#branchName').val(data[0].BRANCH_NAME);
        	$('#routeName').val(data[0].ROUT_NO);
        	$('#accountNO').val(data[0].ACC_NO);
        	$('#paystatus').val(data[0].PAY_STATUS);
        	$('#bankpostingDate').val(data[0].BANK_POSTING_DATE);
        //	$('#paymentInfo').val(data[0].ROUT_NO);
        	$('#lotno').val(data[0].LOTNO);
        	$('#lotdate').val(data[0].LOT_DATE);
        	$('#batchNo').val(data[0].BATCHNO);
        	$('#batchDate').val(data[0].BATCH_DATE);
        	$('#amount').val(data[0].AMOUNT);
        	$('#foundNo').val(data[0].FUND_NO);
        	$('#fundCreateDate').val(data[0].FUND_CREATE_DATE);
        	$('#returnBatchDate').val(data[0].RETURN_BATCH_DATE);
        	$('#installNo').val(data[0].INSTALNO);
        	$('#beftn_').val(data[0].BEFTN);
        	$('#refNo').val(data[0].REF_NO);
        	$('#payTo_').val(data[0].q2_sm[0].PAYTO);
           // console.log(data);
    
        }


    });
    //return false;
});
</script>

</body>
</html>