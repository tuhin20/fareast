<?php $this->load->view('common/header.php'); ?>
	<style>
		.bordered{
			border: 1px solid #dddddd;
		}
		.font-size20{
			font-size:20px;
		}
	</style>

	<header class="page-header">
		<h2>Metronet CRM</h2>
	
	</header>
	
	
	<div class='row'>
		<div class="col-md-12">
			<section class="panel" >
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>
			
					<h2 class="panel-title">Dashboard</h2>
				</header>
				<div class="panel-body ">
					<div class="row">
						<div class="col-md-12">
							<form  method="post" action="<?php echo site_url('api/GetBeftn');?> ">
							<h3>Please input Policy Number</h3>
							<input type="text" name="policy_no">
							<button type="submit" class="btn btn-primary button-loading">Submit</button>
							</form>
							<div class="table-responsive">
								<table  class="table table-striped table-bordered" id="myTable">

								<thead>

									<tr>
								   
									<!--<th><input type="checkbox" name="chk[]" value="0"/></th>-->
						
								<th>FUND_NO</th>
								<th>PAYMENT_INFO</th>
							    <th> POLICY_NO</th>
								<th>PROPOSER</th>
							    <th> AMOUNT</th>
								<th>BANK_NAME</th>
							    <th> BRANCH_NAME</th>
							    <th> ROUT_NO</th>
								<th>ACC_NO</th>
							    <th> PAYTO</th>
							    <th> LOTNO</th>
								
							    <th> LOT_DATE</th>
								 <th> BATCHNO</th>
								<th>BATCH_DATE</th>
							    <th> RETURN_BATCH_DATE</th>
								<th>INSTALNO</th>
							    <th> BEFTN</th>
								<th>REFNO</th>
							    <th>FUND_CREATE_DATE</th>
							    <th>PAY_STATUS</th>
								<th>BANK_POSTING_DATE</th>
								<th>q2_sm</th>
							    </tr>
								</thead>
								<tbody>
								  <?php foreach ($beftn as $value){ ?>

								  <tr>
									<td><?php echo $value['FUND_NO'];?></td>
									<td><?php echo $value['PAYMENT_INFO'];?></td>
									<td><?php echo $value['POLICY_NO'];?></td>
									<td><?php echo $value['PROPOSER'];?></td>
									<td><?php echo $value['AMOUNT'];?></td>
									<td><?php echo $value['BANK_NAME'];?></td>
									<td><?php echo $value['BRANCH_NAME'];?></td>
									<td><?php echo $value['ROUT_NO'];?></td>
									<td><?php echo $value['ACC_NO'];?></td>
									<td><?php echo $value['PAYTO'];?></td>
									<td><?php echo $value['LOTNO'];?></td>
									
									<td><?php echo $value['LOT_DATE'];?></td>
									<td><?php echo $value['BATCHNO'];?></td>
									<td><?php echo $value['BATCH_DATE'];?></td>
									<td><?php echo $value['RETURN_BATCH_DATE'];?></td>
									<td><?php echo $value['INSTALNO'];?></td>
									<td><?php echo $value['BEFTN'];?></td>
									<td><?php echo $value['REFNO'];?></td>
									<td><?php echo $value['FUND_CREATE_DATE'];?></td>
									<td><?php echo $value['PAY_STATUS'];?></td>
									<td><?php echo $value['BANK_POSTING_DATE'];?></td>
									<td> 
									 <table class="table table-striped table-bordered">
	                                    	<thead><section>
		                                        <tr>
		                                         	<th>PAYTO</th>
		                                         	<th>SUM</th>
													
		                                        </tr></section>
	                                        </thead>
	                                        <tbody>
		                                       	<tr>
													<?php foreach ($value['q2_sm'] as $row){ ?>
														<tr >
															<td><?=$row['PAYTO'];?></td>
															<td><?=$row['SUM'];?></td>
															
														</tr>
													<?php } ?>
													
		                                       	</tr>
	                                       </tbody>
	                                    </table>
										</td>
									
								  </tr>
								  <?php } ?>
								  
								</tbody>
							  </table>
							</div>
							
							
						</div>
						
					</div>
					
				</div>
				
			</section>
						
		
		</div>
	</div>
					
						
						

<?php $this->load->view('common/footer.php'); ?>
</script>