<?php
//prime_model is required for this control
$queries=$this->prime_model->getByQuery('select distinct division from metro_areadetails');

?>
<!--<div id="divClient" class="forReloadSelect" >-->

<select class="form-control input-sm" type="client" name="division" id="division">
    <option value='' >--</option>
    <?php
    foreach($queries as $item)
    {
        $selected='';
        if(isset($params['division']) && $item['division']==$params['division'])
            $selected='selected';
        $text=str_replace('_',' ',$item['division']);
        $value=$item['division'];
        echo "<option value='$value' $selected>$text</option>";
    }

    ?>
</select>
<!--</div>-->