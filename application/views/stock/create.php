<?php $this->load->view('common/header.php');
//print_r($params);
?>

<header class="page-header">
	<h2>মজুদ</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="index.html">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>মজুদ </span></li>
			<li><span><?php echo $ui['title']; ?></span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>  
<div class="row">
	<div class=" col-md-12"> 	<!-- this is alert size-->
		<?php 
			$return_value=$this->session->flashdata('return_value');
			if(isset($return_value)) {
				//print_r($return_value);
				$msg=$return_value['msg'];
				$close_button="<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>";
				if($return_value['success'] == true){
					echo "<div class='alert alert-primary' role='alert'>$close_button $msg</div>";
				}
				else if($msg!=''){ //if not success and msg not empty
					echo "<div class='alert alert-danger' role='alert'>$close_button$msg</div>";
				}
			}
		?>
    
	
			
		<form class="" method="post" action="<?php echo $ui['action']; ?>" accept-charset="utf-8">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>

					<h2 class="panel-title"><?php echo $ui['title']; ?></h2>
					<p class="panel-subtitle">
						<!--Basic validation will display a label with the error after the form control.-->
					</p>
				</header>
				<div class="panel-body">	
					<div class="row">
						<div class="col-md-6">
						
							<input type="hidden" id="id" name="id" value="<?php echo isset ($params['id'])? $params['id']:-1;  ?>">
							
							
							
							<div class="form-group">
								<label for="close_relative">বিবরণ ( ঐচ্ছিক ) :</label>
								<textarea type="text" autocomplete="off" class="form-control " id="description" placeholder="" rows='4' name="description"><?php echo isset($params['description'])?$params['description']:''; ?></textarea>
							</div>
						
						</div>
					
						<div class="col-md-12">
							<br><br>
							
							<table id="distribute" class="table table-bordered table-hover table-striped" id="my_datatable">
								<thead>
								<tr>
									<!--<th style="width:10%;"></th>-->
									
									<th >ঔষধের নাম</th> 
									<th >পরিমাণ</th> 
									<th ><button class="add_row btn btn-sm btn-primary">&nbsp;<span class="ti-plus"></span>&nbsp;</button></th>
									

								</tr>
								</thead>
								<tbody>
									<?php
									//while editing
									foreach($rows as $item){
										echo $item;
									}
									?>
								</tbody>
							</table>

							<br><br>
						</div>
					</div><!-- end of row-->
				</div><!-- end of panel body-->
				<div class="panel-footer">
					<div class="form-group">
						
						<button type="submit" class="btn btn-lg btn-primary">&nbsp;&nbsp;<?php echo $ui['okButton'];?>&nbsp;&nbsp;</button>
						<?php if(isset($params['id'])&&$params['id']!=-1){
							//echo "<a target=_blank href='". site_url('stock/print_distribution/'.$params['id'])."' class=\"btn btn-lg btn-info \" title=\"Print\"><i class=\"ti-printer\"></i>&nbsp;Print&nbsp;&nbsp;</a>";
						}?>
					</div>
				</div>
			</section>
		</form>	
			
	</div>    
</div>
		
	
	
      


<?php $this->load->view('common/footer.php'); ?>
<script>

jQuery(document.body).on('click', '.add_row', function (e) {
			
	$.ajax({
		url : "<?php echo site_url('stock/single_row_json'); ?>",
		//data : 'package='+1+'&day='+dayValue,
		data : 'stock_id=-1',
		type : 'post',
		dataType : 'json',
		success : function(response){
			console.log(response);
			//var obj=jQuery.parseJSON(response);
			$("#distribute tbody").append(response.content);
			//$(this).closest('table').attr('id')
		}
	});
	
	
	
	return false;
});
jQuery(document.body).on('click', '.delete_row', function (e) {
	$(this).parents("tr").remove();
	/*var this_holder=this;
	$(this_holder).closest('td').closest('tr').removeClass('bg_red bg_green');*/
});
$(document).ready(function(){
	//alert('doc loaded');
	
	
	//for new input table should have one empty row , so clicking add button automatically
	var id=$('#id').val();
	if(id==-1){
		$( ".add_row" ).trigger( "click" );
	}
		
});


</script>
