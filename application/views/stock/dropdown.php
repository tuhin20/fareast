<br>
<?php if($params['type']=='bp'){
?>
	<label for="bp">আইডি/বিপি নম্বরঃ</label>
	<?php
	$options=$this->prime_model->getByQuery("select bp from member where job_status='regular'");
	?>

	<select data-plugin-selectTwo class="form-control dropdown_dynamic" type="bp" name="member_bp" id="member_bp" >
		<option value='' >--</option>
		<?php
		foreach($options as $item)
		{
			$selected='';
			if(isset($params['member_bp']) && $item['bp']==trim($params['member_bp']) )
				$selected='selected';
			echo "<option value='$item[bp]' $selected>$item[bp]</option>";
		}

		?>
	</select>
<?php 
}
else if($params['type']=='unit'){
?>
	<label for="">পুলিশ ইউনিটঃ</label>
	<?php
	$options=$this->prime_model->getByQuery("select police_unit_name from police_unit order by police_unit_name");
	?>

	<select data-plugin-selectTwo class="form-control "  name="police_unit" id="police_unit" >
		<option value='' >--</option>
		<?php
		foreach($options as $item)
		{
			$selected='';
			if(isset($params['police_unit']) && $item['police_unit_name']==trim($params['police_unit']) )
				$selected='selected';
			echo "<option value='$item[police_unit_name]' $selected>$item[police_unit_name]</option>";
		}

		?>
	</select>
<?php 
}
else if($params['type']=='nid'){
?>
	<label for="">জাতীয় পরিচয়পত্র / পাসপোর্ট নম্বরঃ</label>
	<?php
	$options=$this->prime_model->getByQuery("select nid from member where job_status='retired' order by nid");
	?>

	<select data-plugin-selectTwo class="form-control dropdown_dynamic" type="nid"  name="member_nid" id="member_nid" >
		<option value='' >--</option>
		<?php
		foreach($options as $item)
		{
			$selected='';
			if(isset($params['member_nid']) && $item['nid']==trim($params['member_nid']) )
				$selected='selected';
			echo "<option value='$item[nid]' $selected>$item[nid]</option>";
		}

		?>
	</select>
<?php 
}
?>