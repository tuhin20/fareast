<?php 
	$this->load->view('common/header.php'); 
?>
<header class="page-header">
	<h2>রিপোর্ট</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="index.html">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>রিপোর্ট</span></li>
			<li><span>২</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>


<div class="row">
	<div class="col-md-12">
		
    </div>
</div>	

<div class="row">
    <div class="col-lg-12">
		<section  class="panel">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>
		
				<h2 class="panel-title">রিপোর্ট-২</h2>
			</header>
			<div class="panel-body">
				<form class="form-inline" method='post' id="create_user_form" action="<?php echo site_url('stock/report2');?>">
			
					<input type="text"  autocomplete="off" class="form-control input-sm " id="from_date" placeholder="Start Date" name="from_date" value="<?php echo isset($params['from_date'])?$params['from_date']:'';?>"> 
					
					<input type="text" autocomplete="off" class="form-control input-sm" id="to_date" placeholder="End Date" name="to_date" value="<?php echo isset($params['to_date'])?$params['to_date']:'';?>">
					
					
					<button type="submit" id="save_customer_details"  name="save_customer_details" class="btn btn-sm btn-warning glyphicon glyphicon-search"></button>
					
				</form>
				<br>
				<br>
				<?php //print_r($report);?>
				<div class='table-responsive'>
			
					<table class="table table-bordered table-hover table-striped table2excel" id="my_datatable">
						<thead>
							<!-- table header -->
							<tr>
								<th > ক্রমিক নং</th>
								<th >ঔষধ গ্রহণকারী সদস্যের  নাম </th>
								<th >পদবী </th>
								<th >ঔষধ গ্রহণের তারিখ</th>
								<th >ঔষধের নাম</th>
								<th class='text-right'>পরিমাণ</th>
								<th class='text-right'>মোট মূল্য</th>
								<th >মোবাইল</th>
								<th >স্বাক্ষর</th>
								
							</tr>
							
							<!-- end of table header-->
						</thead>
						<tbody>	
							
							<?php
							foreach($report as $row){
								echo "<tr>";
								foreach($row as $key=>$item){
									$class="";
									if($key==5||$key==6){
										$class='text-right';
									}
										echo "<td class='$class'>$item</td>";
								}
								echo "</tr>";
							}
							
							?>
							
						</tbody>
					</table>				
				</div>

			</div>
		</section>	
		
        
		
	
	
    </div>
</div>


<?php $this->load->view('common/footer.php'); ?>

<script type="text/javascript">
	
	jQuery('#from_date').datetimepicker({format:'Y-m-d H:i'});//timepicker:false
	jQuery('#to_date').datetimepicker({format:'Y-m-d H:i'}); //timepicker:false
	
	$(document).ready(function() {
		/*
		$("#my_datatable").tableExport({
			headings: true,                    // (Boolean), display table headings (th/td elements) in the <thead>
			footers: true,                     // (Boolean), display table footers (th/td elements) in the <tfoot>
			formats: ["xlsx"],    				// (String[]), filetypes for the export ,"xls", "csv", "txt"
			fileName: "id",                    // (id, String), filename for the downloaded file
			bootstrap: true,                   // (Boolean), style buttons using bootstrap
			position: "bottom",                 // (top, bottom), position of the caption element relative to table
			ignoreRows: null,                  // (Number, Number[]), row indices to exclude from the exported file(s)
			ignoreCols: null,                  // (Number, Number[]), column indices to exclude from the exported file(s)
			ignoreCSS: ".tableexport-ignore",  // (selector, selector[]), selector(s) to exclude from the exported file(s)
			emptyCSS: ".tableexport-empty",    // (selector, selector[]), selector(s) to replace cells with an empty string in the exported file(s)
			trimWhitespace: false              // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s)

		});*/
	} );

	$(document).ready(function() {
		$("#result").hide();
		$('#my_datatable').DataTable( {
			"aaSorting": [],
			dom: 'Bfrtip',
			
				buttons: [
					{ extend: 'print'
					, customize: function(win) {

						$(win.document.body).find('table tbody td:nth-child(6)').css('text-align', 'right');
						$(win.document.body).find('table tbody td:nth-child(7)').css('text-align', 'right');
                      //$(win.document.body).append('sdfds'); //before the table
                    }
					, exportOptions: {
														stripHtml : false,
														columns: ':visible'
													}
					},
					//{ extend: 'copy', className: 'copyButton' },
					{ extend: 'excel', className: 'excelButton',exportOptions: {
														stripHtml : false
													} }
					
				]
			
		} );
	} );
		
	$( "form" ).submit(function( e ) {
		$('#loading_modal').modal('toggle');
        return true;
    });
		

    jQuery(document.body).on('click', '.redial', function (e) {
        //var this_holder = this;
		$(this).attr("disabled",true);
        e.preventDefault();
		var redial_url= $(this).attr('href');
		

        //bootbox.confirm("Are you sure you want to Redial ", function (response) {
            //if (response) {
                $.ajax({
                    url: redial_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
                            //$(this_holder).closest('td').closest('tr').hide(1000);
							//alert('');
                        } else {

                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });

            //}
        //});
    });
	
	/*$("#send_waiver_code").click(function(){
		console.log('sms clicked');
		console.log($('#agent_id').val());
		if($('#wv_code').val().trim()==''){
			alert('No waiver code');
			return false;
		}
		$.ajax({
			url: 'http://172.20.15.14/mariestopes/index.php/customer/send_sms',  //'http://172.16.252.202/marie_api.php' , //  //
			type: 'POST',
			data: { text: $('#wv_code').val(),to:$('#wv_mobile').val() },
			async:false,
			success: function(data) {
				console.log(data);
				var obj=JSON.parse(data);
				if(obj.hasOwnProperty('result')&&obj.result.length>0){
					var temp=obj.result[0];
					$('#sms_response').html('<div class=" alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+temp.message+'</div>');
					//alert(temp.message);
				}                                
				//console.log(obj);
			}

		});
		return false;
	});*/
	
		
    $(document).on('click', '.linksDeleteSelected', deleteSelected);
    $(document).on('change', '.selectAll', function () {
        if ($(this).is(":checked")) {
            $('.resultRow').prop('checked', true);
        }
        else
            $('.resultRow').prop('checked', false);
    });

    function deleteSelected(e) {
        if ($('.resultRow:checked').size() < 1) {
            alert('please select at least one record');
            return false;
        }
        var r = confirm(e.target.getAttribute("confirmationMsg"));
        if (r == false)
            return false;
        var controllerMethod = e.target.getAttribute("controllerMethod");
        var url = controllerMethod;
        /*var obj = {
            "flammable": "inflammable",
            "duh": "no duh"
        };
        $.each( obj, function( key, value ) {
            alert( key + ": " + value );
        });*/

        var selectedIDs = [];
        $('.resultRow:checked').each(function () {
            selectedIDs.push($(this).val());
        });
        console.log(selectedIDs);
        $.post(
            url,
            {selectedIDs: selectedIDs.join(", ")},
            function (data) {
                /*var obj = $.parseJSON(data);
                if(obj.success){
                    showStatusModal('Success','modal-info',obj.content);
                    reloadResult(e.target.getAttribute("controllerReloadMethod"));
                }
                else showStatusModal('Error','modal-warning',obj.content);*/
                window.location.replace(e.target.getAttribute('controllerReloadMethod'));
            }
        );
        return false;
    }

</script>
