<?php $this->load->view('common/header.php'); 



?>

<header class="page-header">
	<h2>মজুদ </h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="index.html">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>মজুদ </span></li>
			<li><span>List</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>
<form role="form" method='post' id="create_user_form" action="<?php echo site_url('click2call/search');?>">
    <?php
    $this->load->view('control/ctrl_loading_modal');
    ?>
	
    <!--<div class="row">
      
		<div class="col-md-1" style="width:230px">
            <input type="text" autocomplete="off" class="form-control input-sm" id="start_date" placeholder="Start Date" name="start_date" >

        </div>

        <div class="col-md-1" style="width:230px">
            <input type="text" autocomplete="off" class="form-control input-sm" id="end_date" placeholder="End Date" name="end_date" >

        </div>
        <div class="col-md-1">
            <button type="submit" id="save_customer_details"  name="save_customer_details" class="btn btn-warning glyphicon glyphicon-search"></button>
        </div>
    </div>-->
	

 
</form>
<div class="row">
    <div class="col-sm-3 col-md-3 col-lg-3">
        <!--<a class="downloadCSV" STYLE="max-width:160px; max-height:25px;" id="downloadCSV" href="<?php echo site_url('/group_manager/downloadCSV'); ?>">Download csv</a>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>
		
				<h2 class="panel-title">List</h2>
			</header>
			<div class="panel-body">
					

					<table class="table table-bordered table-hover table-striped" id="my_datatable">
						<thead>
						<tr>
							<!--<th style="width:10%;"></th>-->
							<th>Sl</th>
							<th>তারিখ</th>
							<th>কোড</th> 
							<th>বিবরণ</th>
							<th>Action</th>
							

						</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
					
					

    
			</div>
		</section>
	</div>
</div>

<?php $this->load->view('common/footer.php'); ?>

<script type="text/javascript">

   /*$('#start_date').datetimepicker({dateFormat: "yy-mm-dd", timeFormat: "HH:mm"});
    $('#end_date').datetimepicker({dateFormat: "yy-mm-dd", timeFormat: "HH:mm"});*/


		$(document).ready(function() {
			//alert('doc loaded');
			$("#result").hide();
			$('#my_datatable').dataTable({
				destroy: true, //use this to reinitiate the table, other wise problem will occur
				processing: true,
				serverSide: true,
				ajax: {
					url: "<?php echo site_url('stock/paging');?>"
					,type: 'POST'
					,data:{query_id: '0'}
				}
			});
		} );
		
	$( "form" ).submit(function( e ) {
        e.preventDefault();
        e.stopImmediatePropagation();
		$("#result").show();
		$.ajax({
                data: $(this).serialize(), // get the form data
				type: $(this).attr('method'), // GET or POST
				url: $(this).attr('action'), // the file to call
                beforeSend: function(  ) {
                    $('#loading_modal').modal('toggle');
                }
            })
            .done(function( response ) {
				console.log(response);
				//alert('success');
                var obj=JSON.parse(response);
                var query_id=obj.query_id;
                //var records_total=obj.records_total;
                
                $('#my_datatable').dataTable({
                    destroy: true, //use this to reinitiate the table, other wise problem will occur
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "<?php echo site_url('click2call/process_paging')?>"
                        ,type: 'POST'
                        ,data:{query_id: query_id} //,'records_total': records_total
                    }
                });
                $('#loading_modal').modal('toggle');
            });
        //alert('form submitted');
        
        return false;
    });
		

    
	
		
    jQuery(document.body).on('click', '.delete', function (e) {
        var this_holder = this;
		//$(this).attr("disabled",true);
        e.preventDefault();
		var delete_url= $(this).attr('href');
		

        bootbox.confirm("Are you sure to Delete ", function (response) {
            if (response) {
                $.ajax({
                    url: delete_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
							console.log(data);
							$(this_holder).parents("tr").hide(1000);
                            //$(this_holder).closest('td').closest('tr').hide(1000);
							//alert('');
                        } else {
							bootbox.alert("Problem deleting data");
                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });

            }
        });
    });

</script>
