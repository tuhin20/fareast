<?php $this->load->view('common/header.php');
//print_r($params);
?>

<header class="page-header">
	<h2>বিতরণ </h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="index.html">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>বিতরণ </span></li>
			<li><span><?php echo $ui['title']; ?></span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>  
<div class="row">
	<div class=" col-md-12"> 	<!-- this is alert size-->
		<?php 
			$return_value=$this->session->flashdata('return_value');
			if(isset($return_value)) 
			{
				//print_r($return_value);
				$msg=$return_value['msg'];
				$close_button="<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>";
				if($return_value['success'] == true)
				{

					echo "<div class='alert alert-primary' role='alert'>$close_button $msg</div>";

				}
				else if($msg!='') //if not success and msg not empty
				{
					echo "<div class='alert alert-danger' role='alert'>$close_button $msg</div>";
				}
			}
		?>
    
	
			
		<form class="" method="post" action="<?php echo $ui['action']; ?>" accept-charset="utf-8">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>

					<h2 class="panel-title"><?php echo $ui['title']; ?></h2>
					<p class="panel-subtitle">
						<!--Basic validation will display a label with the error after the form control.-->
					</p>
				</header>
				<div class="panel-body">	
					<div class="row">
						<div class="col-md-4">
						
							<input type="hidden" id="id" name="id" value="<?php echo isset ($params['id'])? $params['id']:-1;  ?>">
							
							<label class="radio-inline mz"><input type="radio" name="type" value="bp" <?php echo $params['type']=='bp'? 'checked':'';  ?> >BP</label>
							<label class="radio-inline mz"><input type="radio" name="type" value="unit" <?php echo $params['type']=='unit'? 'checked':'';  ?>>Unit</label>
							<label class="radio-inline mz"><input type="radio" name="type" value="nid" <?php echo $params['type']=='nid'? 'checked':'';  ?>>Retired</label>
							
							
							<div class="form-group " id="div_dropdown">
								<?php echo $dropdown;?>
								
							</div>
						
							<div class="form-group">
								<label for="mobile">নামঃ</label>
								<input type="text" autocomplete="off" class="form-control " id="name" name="name" placeholder="" value="<?php echo isset($params['name'])?$params['name']:'';?>">
							</div>
							
							<div class="form-group">
								<label for="mobile">মোবাইল নম্বরঃ</label>
								<input type="text" autocomplete="off" class="form-control " id="mobile" name="mobile" placeholder="" value="<?php echo isset($params['mobile'])?$params['mobile']:'';?>">
							</div>
							
							<div class="form-group">
								<label for="close_relative">ঠিকানাঃ</label>
								<textarea type="text" autocomplete="off" class="form-control " id="present_address" placeholder="" rows='4' name="present_address"><?php echo isset($params['present_address'])?$params['present_address']:''; ?></textarea>
							</div>
						
						</div>
						
						<div id="member_short_details" class="col-md-8" style="border:1px solid #016FB6; height:390px; overflow-y:scroll;" >
							<?php echo $member_short_details;?>
						</div>
						
						<div class="col-md-12">
							<br><br>
							<!--<input type="checkbox" class="selectAll" /> <span role='button' data-toggle='tooltip' data-placement='top' title='Delete selected' class='glyphicon glyphicon-trash linksDeleteSelected ' controllerMethod="<?php echo site_url('group_manager/deleteSelected') ?>" controllerReloadMethod="<?php echo site_url('group_manager/group_list') ?>" confirmationMsg='are you sure to delete selected group(s)' aria-hidden='true' onclick=''></span>-->

							<table id="distribute" class="table table-bordered table-hover table-striped" id="my_datatable">
								<thead>
								<tr>
									<!--<th style="width:10%;"></th>-->
									
									<th >ঔষধের নাম</th> 
									<th >পরিমাণ</th> 
									<th ><button class="add_row btn btn-sm btn-primary">&nbsp;<span class="ti-plus"></span>&nbsp;</button></th>
									

								</tr>
								</thead>
								<tbody>
									<?php
									//while editing
									foreach($rows as $item){
										echo $item;
									}
									?>
								</tbody>
							</table>

							<br><br>
						</div>
					</div><!-- end of row-->
				</div><!-- end of panel body-->
				<div class="panel-footer">
					<div class="form-group">
						
						<button type="submit" class="btn btn-lg btn-primary">&nbsp;&nbsp;<?php echo $ui['okButton'];?>&nbsp;&nbsp;</button>
						<?php if(isset($params['id'])&&$params['id']!=-1){
							echo "<a target=_blank href='". site_url('stock/print_distribution/'.$params['id'])."' class=\"btn btn-lg btn-success \" title=\"Print\"><i class=\"ti-printer\"></i>&nbsp;Print&nbsp;&nbsp;</a>";
						}?>
					</div>
				</div>
			</section>
		</form>	
			
	</div>    
</div>
		
	
	
      


<?php $this->load->view('common/footer.php'); ?>
<script>

jQuery(document.body).on('click', '.add_row', function (e) {
			
	$.ajax({
		url : "<?php echo site_url('stock/single_row_json'); ?>",
		//data : 'package='+1+'&day='+dayValue,
		data : 'stock_id=-1',
		type : 'post',
		dataType : 'json',
		success : function(response){
			console.log(response);
			//var obj=jQuery.parseJSON(response);
			$("#distribute tbody").append(response.content);
			//$(this).closest('table').attr('id')
		}
	});
	
	
	
	return false;
});
jQuery(document.body).on('click', '.delete_row', function (e) {
	$(this).parents("tr").remove();
	/*var this_holder=this;
	$(this_holder).closest('td').closest('tr').removeClass('bg_red bg_green');*/
});
jQuery(document.body).on('change', '.mz', function (e) {
	radio_value=$("input[name='type']:checked").val();
	$.ajax({
		url : "<?php echo site_url('stock/dropdown_json'); ?>",
		//data : 'package='+1+'&day='+dayValue,
		data : 'type='+radio_value,
		type : 'post',
		dataType : 'json',
		success : function(response){
			console.log(response);
			$('#div_dropdown').html(response.content);
			$("select").select2("destroy").select2();  //reinitialize select 2 othewise select2 will not work 
		}
	});
});
function show_member_short_details(type,value){
	$('#member_short_details').html();
	$.ajax({
		url : "<?php echo site_url('member/short_details_json'); ?>",
		//data : 'package='+1+'&day='+dayValue,
		data : type+'='+value,
		type : 'post',
		dataType : 'json',
		success : function(response){
			console.log(response);
			$('#member_short_details').html(response.content);
		}
	});
}
jQuery(document.body).on('change', '.dropdown_dynamic', function (e) {
	//alert($(this).attr("type"));
	$.ajax({
		url : "<?php echo site_url('member/get_member_json'); ?>",
		//data : 'package='+1+'&day='+dayValue,
		data : $(this).attr("type")+'='+$(this).val(),
		type : 'post',
		dataType : 'json',
		success : function(response){
			console.log(response);
			$('#name').val(response.name);
			$('#mobile').val(response.mobile);
			$('#present_address').val(response.present_address);
		}
	});
	show_member_short_details($(this).attr("type"),$(this).val());
});
$(document).ready(function(){
	//alert('doc loaded');
	
	
	//for new input table should have one empty row , so clicking add button automatically
	var id=$('#id').val();
	if(id==-1){
		$( ".add_row" ).trigger( "click" );
	}
		
});

function print_specific_div_content(){
    var win = window.open('','','left=0,top=0,width=552,height=477,toolbar=0,scrollbars=0,status =0');

    var content = "<html>";
    content += "<body onload=\"window.print();\">";
    content += "new doc";//document.getElementById("divToPrint").innerHTML ;
    content += "</body>";
    content += "</html>";
    win.document.write(content);
    win.document.close();
	//return false;
}
</script>
