<?php $this->load->view('common/header.php'); 



?>
<header class="page-header">
	<h2> বিতরণ </h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="index.html">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span> বিতরণ </span></li>
			<li><span>List</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>

<form role="form" method='post' id="create_user_form" action="<?php echo site_url('click2call/search');?>">
    <?php
    $this->load->view('control/ctrl_loading_modal');
    ?>
	
    <!--<div class="row">
      
		<div class="col-md-1" style="width:230px">
            <input type="text" autocomplete="off" class="form-control input-sm" id="start_date" placeholder="Start Date" name="start_date" >

        </div>

        <div class="col-md-1" style="width:230px">
            <input type="text" autocomplete="off" class="form-control input-sm" id="end_date" placeholder="End Date" name="end_date" >

        </div>
        <div class="col-md-1">
            <button type="submit" id="save_customer_details"  name="save_customer_details" class="btn btn-warning glyphicon glyphicon-search"></button>
        </div>
    </div>-->
	

 
</form>
<div class="row">
    <div class="col-lg-12">
		<section  class="panel">
			<header class="panel-heading">
				<div class="panel-actions">
					<!--<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>-->
				</div>
		
				<h2 class="panel-title">List</h2>
			</header>
			<div class="panel-body">
					<table class="table table-bordered table-hover table-striped" id="my_datatable">
						<thead>
						<tr>
							<!--<th style="width:10%;"></th>-->
							<th>Sl</th>
							<th>তারিখ</th> 
							<th>প্রেসক্রিপশন নম্বর</th> 
							<th>নাম</th>
							<th>আইডি/বিপি নম্বর</th>
							<th>মোবাইল নম্বর</th>
							<th>Action</th>
							

						</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
					
					
			</div>
		</section>
			
	</div>
</div>

<?php $this->load->view('common/footer.php'); ?>

<script type="text/javascript">

   /*$('#start_date').datetimepicker({dateFormat: "yy-mm-dd", timeFormat: "HH:mm"});
    $('#end_date').datetimepicker({dateFormat: "yy-mm-dd", timeFormat: "HH:mm"});*/


		$(document).ready(function() {
			//alert('doc loaded');
			$("#result").hide();
			$('#my_datatable').dataTable({
				destroy: true, //use this to reinitiate the table, other wise problem will occur
				processing: true,
				serverSide: true,
				ajax: {
					url: "<?php echo site_url('stock/paging_distribution');?>"
					,type: 'POST'
					,data:{query_id: '0'}
				}
			});
		} );
		
	$( "form" ).submit(function( e ) {
        e.preventDefault();
        e.stopImmediatePropagation();
		$("#result").show();
		$.ajax({
                data: $(this).serialize(), // get the form data
				type: $(this).attr('method'), // GET or POST
				url: $(this).attr('action'), // the file to call
                beforeSend: function(  ) {
                    $('#loading_modal').modal('toggle');
                }
            })
            .done(function( response ) {
				console.log(response);
				//alert('success');
                var obj=JSON.parse(response);
                var query_id=obj.query_id;
                //var records_total=obj.records_total;
                
                $('#my_datatable').dataTable({
                    destroy: true, //use this to reinitiate the table, other wise problem will occur
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "<?php echo site_url('click2call/process_paging')?>"
                        ,type: 'POST'
                        ,data:{query_id: query_id} //,'records_total': records_total
                    }
                });
                $('#loading_modal').modal('toggle');
            });
        //alert('form submitted');
        
        return false;
    });
		

    jQuery(document.body).on('click', '.redial', function (e) {
        //var this_holder = this;
		$(this).attr("disabled",true);
        e.preventDefault();
		var redial_url= $(this).attr('href');
		

        //bootbox.confirm("Are you sure you want to Redial ", function (response) {
            //if (response) {
                $.ajax({
                    url: redial_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
                            //$(this_holder).closest('td').closest('tr').hide(1000);
							//alert('');
                        } else {

                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });

            //}
        //});
    });
	
	/*$("#send_waiver_code").click(function(){
		console.log('sms clicked');
		console.log($('#agent_id').val());
		if($('#wv_code').val().trim()==''){
			alert('No waiver code');
			return false;
		}
		$.ajax({
			url: 'http://172.20.15.14/mariestopes/index.php/customer/send_sms',  //'http://172.16.252.202/marie_api.php' , //  //
			type: 'POST',
			data: { text: $('#wv_code').val(),to:$('#wv_mobile').val() },
			async:false,
			success: function(data) {
				console.log(data);
				var obj=JSON.parse(data);
				if(obj.hasOwnProperty('result')&&obj.result.length>0){
					var temp=obj.result[0];
					$('#sms_response').html('<div class=" alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+temp.message+'</div>');
					//alert(temp.message);
				}                                
				//console.log(obj);
			}

		});
		return false;
	});*/
	
		
    jQuery(document.body).on('click', '.delete', function (e) {
        var this_holder = this;
		//$(this).attr("disabled",true);
        e.preventDefault();
		var delete_url= $(this).attr('href');
		

        bootbox.confirm("Are you sure to Delete ", function (response) {
            if (response) {
                $.ajax({
                    url: delete_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
							console.log(data);
							$(this_holder).parents("tr").hide(1000);
                            //$(this_holder).closest('td').closest('tr').hide(1000);
							//alert('');
                        } else {
							bootbox.alert("Problem deleting data");
                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });

            }
        });
    });
	jQuery(document.body).on('change', '#member_bp', function (e) {
		//alert('bp changed');
		$.ajax({
			url : "<?php echo site_url('member/get_member_json'); ?>",
			//data : 'package='+1+'&day='+dayValue,
			data : 'bp='+$('#member_bp').val(),
			type : 'post',
			dataType : 'json',
			success : function(response){
				console.log(response);
				$('#name').val(response.name);
				$('#mobile').val(response.mobile);
				$('#present_address').val(response.present_address);
			}
		});
	});

</script>
