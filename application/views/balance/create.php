<?php $this->load->view('common/header.php'); ?>

	<header class="page-header">
		<h2>Recharge</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Recharge</span></li>
				<li><span><?php echo $ui['title']; ?></span></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	<div class="row">
		<div class=" col-md-12"> 	<!-- this is alert size-->
			
		</div>
	</div>
	
	<div class='row'>
		<div class="col-md-7">
			<?php 
				$return_value=$this->session->flashdata('return_value');
				if(isset($return_value)){
	
					$msg=$return_value['msg'];
					$close_button="<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>";
					if($return_value['success'] == true){
						echo "<div class='alert alert-primary' role='alert'>$close_button $msg </div>";
					}
					else if($msg!='') //if not success and msg not empty
					{
						echo "<div class='alert alert-danger' role='alert'>$close_button $msg</div>";
					}
				}
			?>

			<form id="blast_form" method="post" action="<?php echo $ui['action']; ?>" class="form-horizontal" enctype="multipart/form-data">
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
							<a href="#" class="fa fa-times"></a>
						</div>

						<h2 class="panel-title">Recharge</h2>
						<p class="panel-subtitle">
							<!--Basic validation will display a label with the error after the form control.-->
						</p>
					</header>
					<div class="panel-body">
						<!--<div class="form-group">
							<label class="col-sm-3 control-label">Full Name <span class="required">*</span></label>
							<div class="col-sm-9">
								<input type="text" name="fullname" class="form-control" placeholder="eg.: John Doe" required/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email <span class="required">*</span></label>
							<div class="col-sm-9">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-envelope"></i>
									</span>
									<input type="email" name="email" class="form-control" placeholder="eg.: email@email.com" required/>
								</div>
							</div>
							<div class="col-sm-9">

							</div>
						</div>-->
						<div class="form-group">
							
							<div class="col-sm-offset-3 col-sm-9">
								<span class="required"><h4>Please provide balance to prepaid client only</h4></span>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Client<span class="required">*</span></label>
							<div class="col-sm-9">
							
								
								<?php	
									$options=$this->balance_model->get_clients();
											
								?>

									<select data-plugin-selectTwo class="form-control "  name="client_id" id="client_id" required>
										<option value='' >--</option>
										<?php
										foreach($options as $item)
										{
											$selected='';
											/*if(isset($params['callerid']) && $params['callerid']==$item['id']) 
												$selected='selected';*/
											echo "<option value='$item[id]' $selected>$item[name]</option>";
										}

										?>
									</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Amount <span class="required">*</span></label>
							<div class="col-sm-9">
								<input type="number" min="1"  name="amount" id="amount" class="form-control" placeholder="1" value="" required/>
							</div>
						</div>
						
						
						
						<br><br>
						
					</div>
					<footer class="panel-footer">
						<div class="row">
							<div class="col-sm-9 col-sm-offset-3">
								<button type="submit" class="btn btn-primary btn-lg">&nbsp;&nbsp;<?php echo $ui['okButton'];?>&nbsp;&nbsp;</button>
								<!--<button type="reset" class="btn btn-default">Reset</button>-->
							</div>
						</div>
					</footer>
				</section>
			</form>
		</div>
	</div>
			
					

<?php $this->load->view('common/footer.php'); ?>

<script type="text/javascript">
	
	
	$( "form" ).submit(function( e ) {
		$('#loading_modal').modal('toggle'); //show modal to take time during form submit
		
	});


	

</script>
