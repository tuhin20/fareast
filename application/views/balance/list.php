<?php $this->load->view('common/header.php'); ?>

	<header class="page-header">
		<h2>Balance</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Balance</span></li>
				<li><span>List</span></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	
	<div class='row'>
		<div class="col-md-12">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>
			
					<h2 class="panel-title">List</h2>
				</header>
				<div class="panel-body ">
					<!--<form class="form-horizontal" method='post' id="create_user_form" action="<?php echo site_url('balance/search');?>">
			
					
					
					<div class="form-group">
						<div class="col-md-3">
							<input type="text"  autocomplete="off" class="form-control input-sm " id="from_date" placeholder="Start Date" name="from_date" value="<?php echo isset($params['from_date'])?$params['from_date']:'';?>"> 
					
						</div>
						<div class="col-md-3">
							<input type="text" autocomplete="off" class="form-control input-sm" id="to_date" placeholder="End Date" name="to_date" value="<?php echo isset($params['to_date'])?$params['to_date']:'';?>">
						</div>
						
						<div class="col-md-2">
							<button type="submit" id="save_customer_details"  name="save_customer_details" class="btn btn-sm btn-warning glyphicon glyphicon-search"></button>
						</div>
					
					</div>
					
					<br><br>
					
				</form>-->
				
				
					<table class="table table-bordered table-hover table-striped" id="my_datatable">
						<thead>
							<tr>
								<th class='col-md-1'>#</th>
								<th class='col-md-2'>Client Name</th>
								<th class='col-md-1'>Amount</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</section>
						
		
		</div>
	</div>
					
						
						

<?php $this->load->view('common/footer.php'); ?>
<script type="text/javascript">

	jQuery('#from_date').datetimepicker({format:'Y-m-d H:i'});//timepicker:false
	jQuery('#to_date').datetimepicker({format:'Y-m-d H:i'}); //timepicker:false

	$(document).ready(function() {
		//alert('doc loaded');
		$("#result").hide();
		$('#my_datatable').dataTable({
			destroy: true, //use this to reinitiate the table, other wise problem will occur
			processing: true,
			serverSide: true,
			//responsive: true,
			ajax: {
				url: "<?php echo site_url('balance/paging');?>"
				,type: 'POST'
				,data:{query_id: '0'}
			}
		});
	} );
		
	$( "form" ).submit(function( e ) {
        e.preventDefault();
        e.stopImmediatePropagation();
		$("#result").show();
		$.ajax({
                data: $(this).serialize(), // get the form data
				type: $(this).attr('method'), // GET or POST
				url: $(this).attr('action'), // the file to call
                dataType : 'json',
				beforeSend: function(  ) {
                    $('#loading_modal').modal('toggle');
                }
            })
            .done(function( response ) {
				console.log(response);
				//alert('success');
                //var obj=JSON.parse(response);
                var query_id=response.query_id;
                //var records_total=obj.records_total;
                
                $('#my_datatable').dataTable({
                    destroy: true, //use this to reinitiate the table, other wise problem will occur
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "<?php echo site_url('balance/paging')?>"
                        ,type: 'POST'
                        ,data:{query_id: query_id} //,'records_total': records_total
                    }
                });
                $('#loading_modal').modal('toggle');
            });
        //alert('form submitted');
        
        return false;
    });
		

	
	
	
		
    jQuery(document.body).on('click', '.delete', function (e) {
        var this_holder = this;
		//$(this).attr("disabled",true);
        e.preventDefault();
		var delete_url= $(this).attr('href');
		

        bootbox.confirm("Are you sure to Delete ", function (response) {
            if (response) {
                $.ajax({
                    url: delete_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
							console.log(data);
							$(this_holder).parents("tr").hide(1000);
                            //$(this_holder).closest('td').closest('tr').hide(1000);
							//alert('');
                        } else {
							bootbox.alert("Problem deleting data");
                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });
				

            }
        });
    });
	
</script>
