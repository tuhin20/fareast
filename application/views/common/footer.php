	
						
					<!-- end: page -->
				</section>
			
			
			
			</div>

			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close visible-xs">
							Collapse <i class="fa fa-chevron-right"></i>
						</a>
			
						<div class="sidebar-right-wrapper">
			
							<div class="sidebar-widget widget-calendar">
								<h6>Upcoming Tasks</h6>
								<div data-plugin-datepicker data-plugin-skin="dark" ></div>
			
								<ul>
									<li>
										<time datetime="2014-04-19T00:00+00:00">04/19/2014</time>
										<span>Company Meeting</span>
									</li>
								</ul>
							</div>
			
							<div class="sidebar-widget widget-friends">
								<h6>Friends</h6>
								<ul>
									<li class="status-online">
										<figure class="profile-picture">
											<img src="<?php echo base_url();?>assets/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-online">
										<figure class="profile-picture">
											<img src="<?php echo base_url();?>assets/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-offline">
										<figure class="profile-picture">
											<img src="<?php echo base_url();?>assets/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-offline">
										<figure class="profile-picture">
											<img src="<?php echo base_url();?>assets/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
								</ul>
							</div>
			
						</div>
					</div>
				</div>
			</aside>
		</section>

		
		<script src="<?php echo base_url();?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="<?php echo base_url();?>assets/vendor/select2/select2.js"></script>
		<!--<script src="<?php //echo base_url();?>assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?php //echo base_url();?>assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?php //echo base_url();?>assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>-->
		
		
		<script src="<?php echo base_url();?>assets/vendor/jquery-datatables/mz/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-datatables/mz/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-datatables/mz/buttons.flash.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-datatables/mz/jszip.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-datatables/mz/pdfmake.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-datatables/mz/vfs_fonts.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-datatables/mz/buttons.html5.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-datatables/mz/buttons.print.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-validation/jquery.validate.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-validation/additional-methods.js"></script>
		
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url();?>assets/javascripts/theme.js?v2"></script>
		
		<script src="<?php echo base_url();?>assets/javascripts/theme.custom.js"></script>
		
		
		<script src="<?php echo base_url();?>assets/javascripts/theme.init.js"></script> 

		
		
		<!--<script src="<?php echo base_url();?>assets/vendor/jquery-ui-1.11.4/jquery-ui.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-datetime/jquery-ui-timepicker-addon.js"></script>-->

		<!-- Examples -->
		<!--<script src="<?php echo base_url();?>assets/javascripts/tables/examples.datatables.default.js"></script>-->
		<script src="<?php echo base_url();?>assets/vendor/bootbox.js"></script>
		<!--<script src="<?php echo base_url();?>assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?php echo base_url();?>assets/javascripts/tables/examples.datatables.tabletools.js"></script>-->
		
		
		<!--https://xdsoft.net/jqplugins/datetimepicker/-->
		<link href="<?php echo base_url('assets/vendor/jquery-datetimepicker/jquery.datetimepicker.css?v2')?>" rel='stylesheet' type='text/css' />
		<script src="<?php echo base_url('assets/vendor/jquery-datetimepicker/jquery.datetimepicker.full.js')?>"></script>
		
		<script src="<?php echo base_url();?>assets/vendor/TableExport-3.3.13/js-xlsx-master/xlsx.core.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/TableExport-3.3.13/js-xlsx-master/xls.core.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/TableExport-3.3.13/Blob.js-master/Blob.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/TableExport-3.3.13/dist/js/FileSaver.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/TableExport-3.3.13/dist/js/tableexport.js"></script>
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/TableExport-3.3.13/dist/css/tableexport.css" />


	</body>
</html>