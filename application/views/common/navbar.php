<html>
<nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: darkgreen">
    <div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					  <span class="sr-only">Toggle navigation</span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
				</button>
		     <a class="navbar-brand" href="<?php echo site_url('api/policy_number');?> ">HOME</a>
			  

            </div>
		  
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
				<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">POLICY NUMBER<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<?php 
							   //if($this->user_model->has_permission_for_role($this->role_model->Add_product_sale)){
							   echo "<li><a href=".site_url('api/policy_number').">Input Policy Number </a></li>";
							    echo "<li><a href=".site_url('api/PolicyDataInfo').">Info </a></li>";
							  
						  // }
						    ?>
							
						</ul>
						
					  
					</li>
				
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CONTACT <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<?php 
							   //if($this->user_model->has_permission_for_role($this->role_model->Add_product_sale)){
							   echo "<li><a href=".site_url('contact/create').">Create </a></li>";
							   echo "<li><a href=".site_url('contact/index').">View List</a></li>";
							   echo "<li><a href=".site_url('import_contact_manager/import_contact').">Import Contact List</a></li>";
						  // }
						    ?>
							
						</ul>
					  
					</li>
					
					<!--<li><a href="#">Link</a></li>-->
			        <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">TICKET <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<?php 
							   //if($this->user_model->has_permission_for_role($this->role_model->View_ticket_list)){
                            echo "<li><a href=".site_url('contact/create_team').">Create Team</a></li>";
								   echo "<li><a href=".site_url('contact/create_ticket').">Create</a></li>";
								   echo "<li><a href=".site_url('contact/ticket_list').">View List</a></li>";
							  // }
							?>
							
						</ul>
					  
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php $current_user=$this->user_model->get_current_user(); echo $current_user['name'];?> <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo site_url('login/logout');?>">Logout</a></li>
						<!--<li><a href="<?php /*echo site_url('login/change_password'); */?>" >Change credentials</a></li>-->
					</ul>
					</li>
				</ul>
      
      
			</div><!-- /.navbar-collapse -->
          
    </div><!-- /.container-fluid -->
</nav>
<head>
   
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        * {box-sizing: border-box}

        /* Set height of body and the document to 100% */
        body, html {
            height: 100%;
            margin: 0;
            font-family: Arial;
        }

        /* Style tab links */
        .tablink {
            background-color: #c7c7c7;
            color: black;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            font-size: 17px;
            width: 25%;
        }

        .tablink:hover {
            background-color: #777;
        }

        /* Style the tab content (and add height:100% for full page content) */
        .tabcontent {
            color: black;
            display: none;
            padding: 100px 20px;
            height: 100%;
			width:100%;
}

@media (min-width: 1200px)
.container {
    width: 1170px;
}
@media (min-width: 992px)
.container {
    width: 970px;
}
@media (min-width: 768px)
.container {
    width: 750px;
}
        

        #Basic_Info {background-color: #eaffe1;}
        #Policy_Statement {background-color: #eaffe1;}
        #Maturity {background-color: #eaffe1;}
        #BEFTN {background-color: #eaffe1;}
        #Survival_Benefit {background-color: #eaffe1;}
        #Death_Claim {background-color: #eaffe1;}
		
		#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
		
    </style>
	
</head>
	 </html>