<?php $this->load->view('common/header.php'); 



?>
<style>
    body {
        font-size: 12px;
		margin-left:30px;
    }
	.modal-backdrop.fade.in {
    z-index: 0;
	
	</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Product Details /
            <small>List</small>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-sm-3 col-md-3 col-lg-3">
        <a STYLE="max-width:160px; max-height:70px;" id="downloadCSV" class="btn btn-success glyphicon glyphicon-download-alt" href="<?php echo site_url('/product/download_product_chart'); ?>"> Download CSV</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
		<div class="table-responsive">
            <!--<input type="checkbox" class="selectAll" /> <span role='button' data-toggle='tooltip' data-placement='top' title='Delete selected' class='glyphicon glyphicon-trash linksDeleteSelected ' controllerMethod="<?php echo site_url('group_manager/deleteSelected') ?>" controllerReloadMethod="<?php echo site_url('group_manager/group_list') ?>" confirmationMsg='are you sure to delete selected group(s)' aria-hidden='true' onclick=''></span>-->

            <table class="table table-bordered table-hover table-striped" id="my_datatable">
                <thead>
                <tr>
                    <!--<th style="width:10%;"></th>-->
                    <th>Sl</th>
                    <th>Product Name</th>
					<th>Product Category</th>
                    <th>Product ID</th>
                    <th>Unit Price</th>
                    
                    <th class="col-md-2">Action</th>

                </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $this->load->view('common/footer.php'); ?>
<script type="text/javascript">
		$(document).ready(function() {
			$('#my_datatable').dataTable({
				destroy: true, //use this to reinitiate the table, other wise problem will occur
				processing: true,
				serverSide: true,
				ajax: {
					url: "<?php echo site_url('product/process_paging');?>"
					,type: 'POST'
					,data:{query_id: '0'}
				}
			});
		} );
    jQuery(document.body).on('click', '.delete', function (e) {
        var this_holder = this;
        e.preventDefault();
		var delete_url= $(this).attr('href');
		

        bootbox.confirm("Are you sure you want to delete this entry?", function (response) {
            if (response) {
                $.ajax({
                    url: delete_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
                            $(this_holder).closest('td').closest('tr').hide(1000);
                        } else {

                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });

            }
        });
    });
    $(document).on('click', '.linksDeleteSelected', deleteSelected);
    $(document).on('change', '.selectAll', function () {
        if ($(this).is(":checked")) {
            $('.resultRow').prop('checked', true);
        }
        else
            $('.resultRow').prop('checked', false);
    });

    function deleteSelected(e) {
        if ($('.resultRow:checked').size() < 1) {
            alert('please select at least one record');
            return false;
        }
        var r = confirm(e.target.getAttribute("confirmationMsg"));
        if (r == false)
            return false;
        var controllerMethod = e.target.getAttribute("controllerMethod");
        var url = controllerMethod;
        /*var obj = {
            "flammable": "inflammable",
            "duh": "no duh"
        };
        $.each( obj, function( key, value ) {
            alert( key + ": " + value );
        });*/

        var selectedIDs = [];
        $('.resultRow:checked').each(function () {
            selectedIDs.push($(this).val());
        });
        console.log(selectedIDs);
        $.post(
            url,
            {selectedIDs: selectedIDs.join(", ")},
            function (data) {
                /*var obj = $.parseJSON(data);
                if(obj.success){
                    showStatusModal('Success','modal-info',obj.content);
                    reloadResult(e.target.getAttribute("controllerReloadMethod"));
                }
                else showStatusModal('Error','modal-warning',obj.content);*/
                window.location.replace(e.target.getAttribute('controllerReloadMethod'));
            }
        );
        return false;
    }

</script>